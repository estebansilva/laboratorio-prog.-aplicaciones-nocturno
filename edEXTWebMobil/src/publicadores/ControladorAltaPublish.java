/**
 * ControladorAltaPublish.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface ControladorAltaPublish extends java.rmi.Remote {
    public java.lang.String[] setCursos(java.lang.String arg0) throws java.rmi.RemoteException;
    public void ingresarEdicion(java.lang.String[] arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3, int arg4, java.util.Calendar arg5, java.lang.String arg6) throws java.rmi.RemoteException, publicadores.Exception;
    public java.lang.String[] getDocentes(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] setInstitutos() throws java.rmi.RemoteException;
    public void inscripcionEdicionCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException, publicadores.Exception;
    public java.lang.String[] getEstudianteEnEdicion(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] getDocenteEdicion(java.lang.String arg0) throws java.rmi.RemoteException;
    public void ingresarEstudiante(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5) throws java.rmi.RemoteException, publicadores.Exception;
    public java.lang.String[] listaDeCategorias() throws java.rmi.RemoteException;
    public void ingresarCurso(java.lang.String[] arg0, java.lang.String[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, int arg5, int arg6, java.util.Calendar arg7, java.lang.String arg8, java.lang.String arg9) throws java.rmi.RemoteException, publicadores.Exception;
    public void ingresarDocente(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5, java.lang.String arg6) throws java.rmi.RemoteException, publicadores.Exception;
    public java.lang.String[] getEdicionDcocente(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] obtenerCategoriadeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] getEdicionEstudiante(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtEdicion darInfoEdicion(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] setEdiciones(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] getPFromacion(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] getInfoCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] getPreviaCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean esDocnete(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String[] setTodosLosCursos() throws java.rmi.RemoteException;
    public java.lang.String[] getInfoEdicion(java.lang.String arg0) throws java.rmi.RemoteException;
    public publicadores.DtEdicion edicionVigente(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean existeCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean existeCurso(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean existenickemail(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String[] cursosporCategoria(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean esposibleinscribirse(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void changeEstadoInscripcionEdicion(java.lang.String arg0, java.lang.String arg1, boolean arg2) throws java.rmi.RemoteException;
    public java.lang.String[][] getEstudianteEnEdicionAceptado(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String getEstadoInscripcionEdicion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
}
