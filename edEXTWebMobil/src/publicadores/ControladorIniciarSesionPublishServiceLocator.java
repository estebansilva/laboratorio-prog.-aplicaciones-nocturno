/**
 * ControladorIniciarSesionPublishServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public class ControladorIniciarSesionPublishServiceLocator extends org.apache.axis.client.Service implements publicadores.ControladorIniciarSesionPublishService {

    public ControladorIniciarSesionPublishServiceLocator() {
    }


    public ControladorIniciarSesionPublishServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ControladorIniciarSesionPublishServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for controladorIniciarSesionPublishPort
    private java.lang.String controladorIniciarSesionPublishPort_address = "http://127.0.0.1:1942/controladorIniciarSesion";

    public java.lang.String getcontroladorIniciarSesionPublishPortAddress() {
        return controladorIniciarSesionPublishPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String controladorIniciarSesionPublishPortWSDDServiceName = "controladorIniciarSesionPublishPort";

    public java.lang.String getcontroladorIniciarSesionPublishPortWSDDServiceName() {
        return controladorIniciarSesionPublishPortWSDDServiceName;
    }

    public void setcontroladorIniciarSesionPublishPortWSDDServiceName(java.lang.String name) {
        controladorIniciarSesionPublishPortWSDDServiceName = name;
    }

    public publicadores.ControladorIniciarSesionPublish getcontroladorIniciarSesionPublishPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(controladorIniciarSesionPublishPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getcontroladorIniciarSesionPublishPort(endpoint);
    }

    public publicadores.ControladorIniciarSesionPublish getcontroladorIniciarSesionPublishPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            publicadores.ControladorIniciarSesionPublishPortBindingStub _stub = new publicadores.ControladorIniciarSesionPublishPortBindingStub(portAddress, this);
            _stub.setPortName(getcontroladorIniciarSesionPublishPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setcontroladorIniciarSesionPublishPortEndpointAddress(java.lang.String address) {
        controladorIniciarSesionPublishPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (publicadores.ControladorIniciarSesionPublish.class.isAssignableFrom(serviceEndpointInterface)) {
                publicadores.ControladorIniciarSesionPublishPortBindingStub _stub = new publicadores.ControladorIniciarSesionPublishPortBindingStub(new java.net.URL(controladorIniciarSesionPublishPort_address), this);
                _stub.setPortName(getcontroladorIniciarSesionPublishPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("controladorIniciarSesionPublishPort".equals(inputPortName)) {
            return getcontroladorIniciarSesionPublishPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://publicadores/", "controladorIniciarSesionPublishService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://publicadores/", "controladorIniciarSesionPublishPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("controladorIniciarSesionPublishPort".equals(portName)) {
            setcontroladorIniciarSesionPublishPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
