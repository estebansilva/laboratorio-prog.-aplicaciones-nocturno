/**
 * ControladorIniciarSesionPublish.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface ControladorIniciarSesionPublish extends java.rmi.Remote {
    public void iniciarSesion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public boolean existeUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean iniciarSesionbool(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String convertirEmalNick(java.lang.String arg0) throws java.rmi.RemoteException;
    public boolean iniciarSesionboolEstudiante(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
}
