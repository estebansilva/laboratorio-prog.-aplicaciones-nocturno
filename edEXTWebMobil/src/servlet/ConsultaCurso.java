package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ConsultaCurso
 */
@WebServlet("/ConsultaCurso")
public class ConsultaCurso extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultaCurso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		request.setAttribute("elegido", request.getParameter("Instituto"));
		request.setAttribute("Instituto", request.getParameter("Instituto"));
		request.setAttribute("Curso", request.getParameter("Curso"));
		request.setAttribute("cursoelegido", request.getParameter("Curso"));
		
		if(request.getParameter("Curso")!=null){
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			
		}
		if(request.getParameter("ediciones") != null) {
			request.getSession().setAttribute("Instituto2", request.getParameter("Instituto"));
	        request.getSession().setAttribute("Curso2", request.getParameter("Curso"));
	        request.getSession().setAttribute("ediciones2", request.getParameter("ediciones"));
			rd = request.getRequestDispatcher("/consultaEdiciones.jsp");
			rd.forward(request, response);
			
		}else {
			rd = request.getRequestDispatcher("/consultaCurso.jsp");
			rd.forward(request, response);
		}
	}

}
