package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;
import publicadores.ControladorIniciarSesionPublish;
import publicadores.ControladorIniciarSesionPublishService;
import publicadores.ControladorIniciarSesionPublishServiceLocator;

/**
 * Servlet implementation class LoginUsuario
 */
@WebServlet("/LoginUsuario")
public class LoginUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Fabrica fabrica = Fabrica.getInstancia();
		//IControladorIniciarSesion icon = fabrica.getControladorIniciarSesion();
		//IControladorAlta iCAlta = fabrica.getIControladorAlta();
		ControladorIniciarSesionPublishService cIS = new ControladorIniciarSesionPublishServiceLocator();
		ControladorIniciarSesionPublish portIS = null;
		try {
			portIS  = cIS.getcontroladorIniciarSesionPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish portcAP = null;
		try {
			portcAP = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String nick = request.getParameter("nick");
		String pass = request.getParameter("pass");
		
		if(nick != "") {
			if(pass != "") {
				if(portIS.iniciarSesionbool(nick, pass)) {
					try {
						portIS.iniciarSesionboolEstudiante(nick, pass);
						request.setAttribute("mensaje", "El usuario " + nick + " ingreso al sistema.");
						RequestDispatcher rd;
						request.setAttribute("nick", nick);
						if(portcAP.esDocnete(nick)) {		
							request.setAttribute("error", "Solo pueden ingresar los estudiantes");
							request.setAttribute("nick", nick);
							request.setAttribute("pass", pass);
							rd = request.getRequestDispatcher("/login.jsp");
							rd.forward(request, response);
						}else {
							if(nick.contains("@")) {
								request.getSession().setAttribute("usuario", portIS.convertirEmalNick(nick));
							}else {
								request.getSession().setAttribute("usuario", nick);
							}
							rd = request.getRequestDispatcher("/index.jsp");
							rd.forward(request, response);
						}
						
					} catch (ServletException e) {
						// TODO Auto-generated catch block
						RequestDispatcher rd;
						request.setAttribute("error", "Solo pueden ingresar los estudiantes");
						request.setAttribute("nick", nick);
						request.setAttribute("pass", pass);
						rd = request.getRequestDispatcher("/login.jsp");
						rd.forward(request, response);
					}
				}else {
					if(!portcAP.existenickemail(nick, nick)) {
						RequestDispatcher rd;
						request.setAttribute("error", "El nickname es Incorrecto");
						request.setAttribute("nick", nick);
						request.setAttribute("pass", pass);
						rd = request.getRequestDispatcher("/login.jsp");
						rd.forward(request, response);
					}else {
						RequestDispatcher rd;
						request.setAttribute("error", "La Pass es Incorrecta");
						request.setAttribute("nick", nick);
						request.setAttribute("pass", pass);
						rd = request.getRequestDispatcher("/login.jsp");
						rd.forward(request, response);
					}
				}
				}else {
					RequestDispatcher rd;
					request.setAttribute("error", "El campo Password esta vacio");
					request.setAttribute("nick", nick);
					request.setAttribute("pass", pass);
					rd = request.getRequestDispatcher("/login.jsp");
					rd.forward(request, response);
				}
			}else {
				RequestDispatcher rd;
				request.setAttribute("error", "El campo Nick esta vacio");
				request.setAttribute("nick", nick);
				request.setAttribute("pass", pass);
				rd = request.getRequestDispatcher("/login.jsp");
				rd.forward(request, response);
			}
		}
		
		
	}


