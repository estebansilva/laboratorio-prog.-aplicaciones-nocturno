package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import com.google.gson.Gson;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;



/**
 * Servlet implementation class ListarAceptadosEdicionCurso
 */
@WebServlet("/ListarAceptadosEdicionCurso")
public class ListarAceptadosEdicionCurso extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListarAceptadosEdicionCurso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String institutoGet = request.getParameter("institutoGet");
		String cursoGet = request.getParameter("cursoGet");
		
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<String> cursosList = new ArrayList<>();
		String[] cursosStr = null;
				

		if (institutoGet != null) {
				cursosStr = port.setCursos(institutoGet);
		}
			
		Gson gson = new Gson();
		if  (!(institutoGet == null) ) {	
				for(String c:cursosStr) {           	
	       			cursosList.add(c.toString());
	        	}		
		}
		
		
		String responseJsonCurso = gson.toJson(cursosList);
 	    response.setContentType("application/json, charset=UTF-8");
		String ambosJson = "["+responseJsonCurso+"]"; 
		response.getWriter().write(ambosJson); 
}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<String> edicionesList = new ArrayList<>();
		List<String> ediciones = new ArrayList<>();
		String curso = request.getParameter("curso");
		String docenteSession = (String)request.getSession().getAttribute("usuario");

		if( curso!=null ){
			if (docenteSession != null) {
				request.setAttribute("docente", docenteSession); 
				RequestDispatcher rd = request.getRequestDispatcher("/ListarEdicCursosAceptados.jsp");
				rd.forward(request, response);
			
			}else {     								
				RequestDispatcher rd = request.getRequestDispatcher("/ListarEdicCursosAceptados.jsp");
			}	

		}else { 	    								
			RequestDispatcher rd = request.getRequestDispatcher("/ListarEdicCursosAceptados.jsp");
		}
	
	}

}
