package servlet;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import com.google.gson.Gson;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
/**
 * Servlet implementation class AltaEdicionCurso2
 */
@WebServlet("/AltaEdicionCurso2")
public class AltaEdicionCurso2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaEdicionCurso2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	String institutoGet = request.getParameter("institutoGet");
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
    	String[] cursos = port.setCursos(institutoGet);
    	String[] docentes = port.getDocentes(institutoGet);
		List<String> cursosList = new ArrayList<>();
		List<String> docentesList = new ArrayList<>();
		
		Gson gson = new Gson();
		if  (!(institutoGet.isEmpty()) ) {	
				for(String c:cursos) {           	
	       			cursosList.add(c.toString());
	        	}
				
				for(String d:docentes) {
					docentesList.add(d.toString());
				}
	    	}
		
		String responseJsonCurso = gson.toJson(cursosList);
		String responseJsonDocente = gson.toJson(docentesList);
 	    response.setContentType("application/json, charset=UTF-8");
		String ambosJson = "["+responseJsonCurso+","+responseJsonDocente+"]"; 
	    response.getWriter().write(ambosJson); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String fecha=request.getParameter("fecha");
		String fechaIni=request.getParameter("fechaIni");
		String fechaFin=request.getParameter("fechaFin");
		String nomEdiCurso=request.getParameter("nomEdiCurso");
		String cupo=request.getParameter("cupo");
		String instituto=request.getParameter("instituto");
		String curso=request.getParameter("curso");
		String[] docente=request.getParameterValues("docente[]");
		List<String> listaDocente = new ArrayList<>();
		String[] listaDocenteReturn = null;
		
		if(docente != null) {
			listaDocente = Arrays.asList(docente);
			listaDocenteReturn = new String[listaDocente.size()];
			listaDocenteReturn = listaDocente.toArray(listaDocenteReturn);
		}
			
		Calendar fecha1C = Calendar.getInstance();
		Calendar fecha2C = Calendar.getInstance();
		Calendar fecha3C = Calendar.getInstance();
		Date fechaDate =null;
		Date fechaIniDate =null;
		Date fechaFinDate =null;
		int cupoInt = 0;
	    
		if ((fecha!="") && (nomEdiCurso!="") && (instituto!="") && (curso!="") && (listaDocente!=null) && (cupo!="") && (fechaFin!="") && (fechaIni!="")) {
			 System.out.println("datos dentro del if" + fecha + fechaIni + fechaFin + nomEdiCurso + cupo + instituto + curso + docente + "/n");
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				fechaDate = format.parse(fecha);
				fechaIniDate = format.parse(fechaIni);
				fechaFinDate = format.parse(fechaFin);
				SimpleDateFormat format2 = new SimpleDateFormat("dd/MM/yyyy");
				String fp = format2.format(fechaDate);
				String fi = format2.format(fechaIniDate);
				String ff = format2.format(fechaFinDate);
				cupoInt = Integer.parseInt(cupo);
				
				fecha1C.setTime(fechaDate);
				fecha2C.setTime(fechaIniDate);
				fecha3C.setTime(fechaFinDate);
				
				try {
					port.ingresarEdicion(listaDocenteReturn,nomEdiCurso, fecha1C, fecha2C, cupoInt, fecha3C, curso);
					RequestDispatcher rd;
					request.setAttribute("mensaje", "Se ha ingresado la edici�n" + nomEdiCurso + "correctamente el usuario  en el sistema.");
					rd = request.getRequestDispatcher("/index.jsp");
					rd.forward(request, response);
				} catch ( Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			    						
		}else {
			System.out.println("entro al else");
			RequestDispatcher rd;
			request.setAttribute("error", "No pueden haber campos vacios");
			request.setAttribute("fecha", fecha);
			request.setAttribute("fechaIni", fechaIni);
			request.setAttribute("fechaFin", fechaFin );
			request.setAttribute("nomEdiCurso", nomEdiCurso );
			request.setAttribute("cupo", cupo );
			request.setAttribute("instituto", instituto );
			request.setAttribute("curso", curso );
			request.setAttribute("docente", docente );		
			rd = request.getRequestDispatcher("/AltaEdicionCurso.jsp");
			rd.forward(request, response);
		}    					
	}

	
	
}
