package servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;

/**
 * Servlet implementation class AltaUsuario
 */
@WebServlet("/AltaUsuario")
public class AltaUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());

	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("Catch de ControladorAltaPublish: " + e1.toString());
		}	
		Calendar fechaRC = Calendar.getInstance();
		String nick=request.getParameter("nick");
	    String nombre=request.getParameter("nombre");
	    String apellido=request.getParameter("apellido");
	    String correo=request.getParameter("correo");
	    String password=request.getParameter("password");
	    String cpassword=request.getParameter("cpassword");
	    String fecha=request.getParameter("fecha");
	    String instituto=request.getParameter("Instituto");
	    SimpleDateFormat farmatmalo = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	    Date fechaN =null;
	    Date D =null;
	    if(nick!="") {
	    	if(nombre!="") {
	    		if(apellido!="") {
	    			if(correo!="") {
	    				if(password!="") {
	    					if(cpassword!="") {
	    						if(password.equals(cpassword)) {
			    					if(fecha!="") {
			    						try {
			    							fechaN = farmatmalo.parse(fecha);
			    							String fn = format.format(fechaN);
			    							D = format.parse(fn);
			    							fechaRC.setTime(D);
			    						} catch (ParseException e) {
			    							// TODO Auto-generated catch block
			    							e.printStackTrace();
			    						} 
			    					   // publicadores.DtUsuario dtu = new publicadores.DtUsuario(nick,nombre,apellido,correo,D,password);
			    					    if(!port.existenickemail(nick, correo)) {
				    						if(instituto.equals("Soy Estudiante")) {
				    							try {
				    								port.ingresarEstudiante(nick,nombre,apellido,correo,fechaRC,password);
				    								request.setAttribute("mensaje", "Se ha ingresado correctamente el usuario " + nick + " en el sistema.");
				    								RequestDispatcher rd;
				    								rd = request.getRequestDispatcher("/index.jsp");
				    								rd.forward(request, response);
				    							}catch(Exception e) {
				    								//throw new Exception(e.getMessage());
				    							}
				    						}
				    						else {
				    							if(instituto!=null) {
					    							try {
					    								//publicadores.DtInstituto dti = new publicadores.DtInstituto(instituto);
					    								port.ingresarDocente(nick,nombre,apellido,correo,fechaRC,password,instituto);
					    								request.setAttribute("mensaje", "Se ha ingresado correctamente el usuario " + nick + " en el sistema.");
					    								RequestDispatcher rd;
					    								rd = request.getRequestDispatcher("/index.jsp");
					    								rd.forward(request, response);
					    							}catch(Exception e) {
					    								//throw new Exception(e.getMessage());
					    							}
				    							}
				    						}//else
			    					    }else {
			    					    	RequestDispatcher rd;
											request.setAttribute("error", "El nickname o el correo eleigo ya existen");
											request.setAttribute("nick", nick);
											request.setAttribute("nombre", nombre);
											request.setAttribute("apellido", apellido);
											request.setAttribute("correo", correo);
											request.setAttribute("password", password);
											request.setAttribute("fecha", fecha);
											request.setAttribute("instituto", instituto);
											rd = request.getRequestDispatcher("/AltaUsuario.jsp");
											rd.forward(request, response);
			    					    }
			    						
			    					}else {
			    						RequestDispatcher rd;
										request.setAttribute("error", "El campo Fecha de Nacimiento esta vacio");
										request.setAttribute("nick", nick);
										request.setAttribute("nombre", nombre);
										request.setAttribute("apellido", apellido);
										request.setAttribute("correo", correo);
										request.setAttribute("password", password);
										request.setAttribute("fecha", fecha);
										request.setAttribute("instituto", instituto);
										rd = request.getRequestDispatcher("/AltaUsuario.jsp");
										rd.forward(request, response);
			    					}
	    						}else {
	    							RequestDispatcher rd;
									request.setAttribute("error", "Las Password seleccionadas no coinciden");
									request.setAttribute("nick", nick);
									request.setAttribute("nombre", nombre);
									request.setAttribute("apellido", apellido);
									request.setAttribute("correo", correo);
									request.setAttribute("password", password);
									request.setAttribute("fecha", fecha);
									request.setAttribute("instituto", instituto);
									rd = request.getRequestDispatcher("/AltaUsuario.jsp");
									rd.forward(request, response);
	    						}	
	    					}else {
	    						RequestDispatcher rd;
								request.setAttribute("error", "El campo Confirmar Password esta vacio");
								request.setAttribute("nick", nick);
								request.setAttribute("nombre", nombre);
								request.setAttribute("apellido", apellido);
								request.setAttribute("correo", correo);
								request.setAttribute("password", password);
								request.setAttribute("fecha", fecha);
								request.setAttribute("instituto", instituto);
								rd = request.getRequestDispatcher("/AltaUsuario.jsp");
								rd.forward(request, response);
	    					}	
	    				}else {
	    					RequestDispatcher rd;
							request.setAttribute("error", "El campo Password esta vacio");
							request.setAttribute("nick", nick);
							request.setAttribute("nombre", nombre);
							request.setAttribute("apellido", apellido);
							request.setAttribute("correo", correo);
							request.setAttribute("password", password);
							request.setAttribute("fecha", fecha);
							request.setAttribute("instituto", instituto);
							rd = request.getRequestDispatcher("/AltaUsuario.jsp");
							rd.forward(request, response);
	    				}
	    			}else {
	    				RequestDispatcher rd;
						request.setAttribute("error", "El campo Correo esta vacio");
						request.setAttribute("nick", nick);
						request.setAttribute("nombre", nombre);
						request.setAttribute("apellido", apellido);
						request.setAttribute("correo", correo);
						request.setAttribute("password", password);
						request.setAttribute("fecha", fecha);
						request.setAttribute("instituto", instituto);
						rd = request.getRequestDispatcher("/AltaUsuario.jsp");
						rd.forward(request, response);
	    			}
	    		}else {
	    			RequestDispatcher rd;
					request.setAttribute("error", "El campo Apellido esta vacio");
					request.setAttribute("nick", nick);
					request.setAttribute("nombre", nombre);
					request.setAttribute("apellido", apellido);
					request.setAttribute("correo", correo);
					request.setAttribute("password", password);
					request.setAttribute("fecha", fecha);
					request.setAttribute("instituto", instituto);
					rd = request.getRequestDispatcher("/AltaUsuario.jsp");
					rd.forward(request, response);
	    		}
	    	}else {
	    		RequestDispatcher rd;
	    		request.setAttribute("error", "El campo Nombre esta vacio");
	    		request.setAttribute("nick", nick);
				request.setAttribute("nombre", nombre);
				request.setAttribute("apellido", apellido);
				request.setAttribute("correo", correo);
				request.setAttribute("password", password);
				request.setAttribute("fecha", fecha);
				request.setAttribute("instituto", instituto);
				rd = request.getRequestDispatcher("/AltaUsuario.jsp");
				rd.forward(request, response);
	    	}
	    }else {
	    	RequestDispatcher rd;
	    	request.setAttribute("error", "El campo Nickname esta vacio");
			request.setAttribute("nick", nick);
			request.setAttribute("nombre", nombre);
			request.setAttribute("apellido", apellido);
			request.setAttribute("correo", correo);
			request.setAttribute("password", password);
			request.setAttribute("fecha", fecha);
			request.setAttribute("instituto", instituto);
			rd = request.getRequestDispatcher("/AltaUsuario.jsp");
			rd.forward(request, response);
	    }
	    
	}

}
