package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datatypes.DtInstituto;
import excepciones.InstitutoRepetidaException;
import interfaces.Fabrica;
import interfaces.IControladorAlta;

/**
 * Servlet implementation class AltaInstituto
 */
@WebServlet("/AltaInstituto")
public class AltaInstituto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaInstituto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Fabrica fabrica = Fabrica.getInstancia();
		IControladorAlta icon = fabrica.getIControladorAlta();
		String nombreInst = request.getParameter("nomInst");
		DtInstituto dtins = new DtInstituto(nombreInst);
		System.out.println("UN POST CON " + nombreInst);
		try {
			icon.ingresarInstituto(dtins);
			request.setAttribute("mensaje", "Se ha ingresado correctamente el instituto " + nombreInst + " en el sistema.");
			System.out.println("Se agrego el curso: " + nombreInst );
		} catch (InstitutoRepetidaException e) {
			// TODO Auto-generated catch block
			throw new ServletException(e.getMessage());
		}
		RequestDispatcher rd;
	//	request.setAttribute("mensaje", "Se ha ingresado correctamente el instituto " + nombreInst + " en el sistema.");
		rd = request.getRequestDispatcher("/index.jsp");
		rd.forward(request, response);
	}

}
