<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.Arrays"%> 
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<%@page import="java.util.List"%> 

<html>
<head>
<meta charset = "utf-8">
<meta name= "viewport" content= "width= device-width, initial-scale=1, shrink-to-fit=no">
<style>


	#consultaEdicion{
	display: blocks;
	margin: auto;
	width: 85%;
	border: 1px solid black;
	}
	
</style>

<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Consulta Edicion Curso</title>
</head>
<body>
<form action="ConsultaEdicion" method="post">
	<%
	
	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
	String[] ins = port.setInstitutos();
	String[] cat = port.listaDeCategorias();
	
	String insti =(String)request.getParameter("Instituto");
	String cosocon =(String)request.getParameter("Curso");
	String edicon =(String)request.getParameter("ediciones");
	%>
	<div class="form-group">
  	<label for="exampleFormControlInput1">Seleccione instituto o categoria:</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ 
				if(insti!= null){%>
					<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%for(String auxins:ins){ %>
					<%if(insti!= null){%>
						<%if(!insti.equals(auxins)){%>
						<option value="<%=auxins%>"><%=auxins%></option>
						<%}%>
					<%}else{%>
						<option value="<%=auxins%>"><%=auxins%></option>
					<%}%>
				<%}%>
			<%}else{%>
					<option value=""></option>
				<%} %>
			<%
			if(cat !=null){
				if(insti!= null){%>
				<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%%>
					<%if(insti!= null){
						for(String auxcat:cat){%>
						<%if(insti.equals(auxcat)){%>
						<%}else{%>
							<option value="<%=auxcat%>"><%=auxcat%></option>
						<%}%>
						<%}%>
					<%}else{%>
						<%for(String auxcat:cat){ %>
						<option value="<%=auxcat%>"><%=auxcat%></option>
						<%} %>
					<%}%>
				<%}%>
			</select>
    </div>
    <%if(request.getAttribute("elegido") != null){
       	List<String> listaCursos;
    	request.setAttribute("Instituto",request.getParameter("Instituto"));
    	String elegido = (String)request.getAttribute("elegido");
    	if(port.existeCategoria(elegido)){
    		listaCursos = Arrays.asList(port.cursosporCategoria(elegido));
    	}else{
    		listaCursos = Arrays.asList(port.setCursos(elegido));
    	}%>
    	<div class="form-group">
  			<label for="exampleFormControlInput1">Seleccione curso:</label>
  				<select class="custom-select" id="Curso" name="Curso">
				<%if(ins != null){ 
				if(cosocon!= null){%>
					<option value="<%=cosocon%>"><%=cosocon%></option>
				<%}%>
				
				<%for(String auxcur:listaCursos){ %>
					<option value="<%=auxcur%>"><%=auxcur%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    </div>
	    <% if(request.getAttribute("cursoelegido") != null){
        	String cursoelegido = (String)request.getAttribute("cursoelegido");%>
	    	<% List<String> ediciones = Arrays.asList(port.setEdiciones(cursoelegido));
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Ediciones:</label>
  				<select class="custom-select" id="ediciones" name="ediciones">
				<%if(ediciones != null){
				if(edicon!= null){%>
					<option value="<%=edicon%>"><%=edicon%></option>
				<%}%>
				<%for(String auxedi:ediciones){ %>
					<option value="<%=auxedi%>"><%=auxedi%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	
	    	<%if(request.getAttribute("edicionlegida") != null){
	    		String edicionlegida = (String)request.getAttribute("edicionlegida");
	    		publicadores.DtEdicion dte = port.darInfoEdicion(edicionlegida);
	    		List<String> est = Arrays.asList(port.getEstudianteEnEdicion(edicionlegida));
	    		List<String> doc = Arrays.asList(port.getDocenteEdicion(edicionlegida));
	    		%>
	    	<%if(dte != null){
	        	%>
	        	<input type="hidden" id="thisField" name="edicionFinal" value="<%=dte.getNombre()%>">
	        	<label for="exampleFormControlInput1">Nombre de la Edicion: <%=dte.getNombre()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Cupo: <%=dte.getCupo()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Publicacion: <%=dte.getFechaPub()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Inicio: <%=dte.getFechaI()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Fin: <%=dte.getFechaF()%></label>
	        	
	        	<br>
	        	<label for="exampleFormControlInput1">Docentes:</label>
	        	<select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
	    	<%if(est != null){
	    		for(String auxdoc:doc){%>
	    			<option value="<%=auxdoc%>"><%=auxdoc%></option>
	    		<%}
	    	}else{ %>
    		<option value=""></option>
    	<%} %>
			
	    </select>
	    <br>
	        	<label for="exampleFormControlInput1">Estudiantess:</label>
	    <select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
	    	<%if(est != null){
	    		for(String auxest:est){%>
	    			<option value="<%=auxest%>"><%=auxest%></option>
	    		<%}
	    	}else{ %>
	    		<option value=""></option>
	    	<%} %>
	    </select>
	        	
	        	<%}else{%>
	        			<label for="exampleFormControlInput1">NO HAY EDICIONES VIGENTES PARA ESTE CURSO</label>
	        	<%}%>
	    	
	    	
	    	<%} %>
       <% }%>
		<%}%>
		<br>
	<button type="submit" class="btn btn-primary" name="1">Confirmar</button>	
	</form>
	
		<form action="consultaCursos.jsp" method="post">
			
	    	<%if(request.getAttribute("Curso")!= null){
	        request.getSession().setAttribute("Instituto2", request.getParameter("Instituto"));
	        request.getSession().setAttribute("Curso2", request.getParameter("Curso"));
	    	%>
	    
	    	<a class="small" href="consultaCursos.jsp">Consultar Curso</a>
	    	<%} %>
	</form>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		
</body>
</html>