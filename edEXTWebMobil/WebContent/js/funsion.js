function buscador_en_select(){
    wp_enqueue_style('estilos-select', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(),'1.0');
    wp_enqueue_script('jquery'); 
    wp_enqueue_script('script-select', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array('jquery'), '1.0');
    wp_enqueue_script('mi-script', get_template_directory_uri().'/js/mi-script.js', array('script-select'),'1.0');
}
add_action('wp_enqueue_scripts', 'buscador_en_select');