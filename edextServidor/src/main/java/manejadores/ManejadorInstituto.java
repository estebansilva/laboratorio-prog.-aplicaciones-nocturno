package manejadores;

import java.util.List;

import javax.persistence.EntityManager;

import logica.Curso;
import logica.Instituto;
import utils.Conexion;

public class ManejadorInstituto {
private static ManejadorInstituto instancia = null;
	
	private ManejadorInstituto(){}
	
	public static ManejadorInstituto getInstancia() {
		if(instancia == null)
			instancia = new ManejadorInstituto();
		return instancia;
	}
	
	public void agregarInstituto(Instituto instituto) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(instituto);
        em.getTransaction().commit();
	}
	
	public boolean existeInstituto(String nombre) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Instituto.class, nombre) == null);
	}
	
	public Instituto buscarInstituto(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Instituto.class, nombre);  
	}
	
	public List<String> obtenerInstitutos(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaInstitutos = em.createQuery("SELECT nombre FROM Instituto").getResultList();
	    return listaInstitutos;
	}
	
	/*public List<String> getCursos(String idInstituto) {
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaCursos = em.createQuery("SELECT cursos_nombre FROM instituto_curso WHERE instituto_nombre = '" + idInstituto + "'").getResultList();
	    return listaCursos;
	}*/
	
	
}
