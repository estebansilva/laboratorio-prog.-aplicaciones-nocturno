package manejadores;

import java.util.List;

import javax.persistence.EntityManager;

import logica.InscripcionEd;

import utils.Conexion;

public class ManejadorInscripcionEd {
private static ManejadorInscripcionEd instancia = null;
	
	private ManejadorInscripcionEd(){}
	
	public static ManejadorInscripcionEd getInstancia() {
		if(instancia == null)
			instancia = new ManejadorInscripcionEd();
		return instancia;
	}
	
	public void agregarInscripcionEd(InscripcionEd inscripcionEd) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(inscripcionEd);
        em.getTransaction().commit();
	}
	
	public boolean existeInscripcionEd(String edicion, String estudiante) {
		  Conexion conexion=Conexion.getInstancia();
		  EntityManager em =conexion.getEntityManager();
		  boolean existe = !em.createQuery("SELECT fecha FROM InscripcionEd WHERE (edicion_nombre = :edicion) AND (estudiante_nick = :estudiante)").setParameter("edicion", edicion).setParameter("estudiante", estudiante).getResultList().isEmpty();
		  return existe;
	}
}
