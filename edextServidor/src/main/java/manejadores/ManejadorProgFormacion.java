package manejadores;

import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;

import datatypes.DtProgFormacion;
import logica.Curso;
import logica.ProgFormacion;
import utils.Conexion;

public class ManejadorProgFormacion {
private static ManejadorProgFormacion instancia = null;

	private ManejadorProgFormacion(){}
	
	public static ManejadorProgFormacion getInstancia() {
		if(instancia == null)
			instancia = new ManejadorProgFormacion();
		return instancia;
	}
	
	public void agregarProgFormacion(ProgFormacion progFormacion) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(progFormacion);
        em.getTransaction().commit();
	}
	
	public boolean existeProgFormacion(String nombre) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html#find%28java.lang.Class,%20java.lang.Object%29
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(ProgFormacion.class, nombre) == null);
	}
	
	
	public ProgFormacion buscarProgFormacion(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(ProgFormacion.class, nombre);  
	}
	
	public List<String> obtenerProgFormacion(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaProgForm = em.createQuery("SELECT nombre FROM ProgFormacion").getResultList();
	    return listaProgForm;
	}
	
	public void agregarCurso(ProgFormacion pf, Curso curso) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(pf);
        pf.agregarCurso(curso);
        em.getTransaction().commit();
	
	}
	
	
	
	public DtProgFormacion obtenerDt(String pf) {
		ProgFormacion prog = buscarProgFormacion(pf);
		DtProgFormacion dtprog = new DtProgFormacion(prog.getNombre(),prog.getDescrip(),prog.getFechaI(),prog.getFechaF());	
		return dtprog;
	}
}
