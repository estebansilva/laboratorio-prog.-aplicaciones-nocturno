package manejadores;

import java.util.List;

import javax.persistence.EntityManager;

import logica.Estudiante;
import logica.Usuario;
import utils.Conexion;

public class ManejadorEstudiante {
private static ManejadorEstudiante instancia = null;
	
	private ManejadorEstudiante(){}
	
	public static ManejadorEstudiante getInstancia() {
		if(instancia == null)
			instancia = new ManejadorEstudiante();
		return instancia;
	}
	
	public void agregarEstudiante(Estudiante estudiante) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(estudiante);
        em.getTransaction().commit();
	}
	
	public boolean existeEstudiante(String nickname) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html#find%28java.lang.Class,%20java.lang.Object%29
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Estudiante.class, nickname) != null);
	}
	
	public Estudiante buscarEstudiante(String string) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Estudiante.class, string);  
	}
	
	public Estudiante buscarEstudiante2(String est) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Estudiante.class, est);  
	}
	
	public boolean existeCorreo(String email) {
		  Conexion conexion=Conexion.getInstancia();
		  EntityManager em =conexion.getEntityManager();
		  boolean existe = !em.createQuery("SELECT correo FROM Estudiante WHERE correo = :email").setParameter("email", email).getResultList().isEmpty();
		  return existe;
	}
	public List<String> obtenerEstudiantes(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaEstudiantes = em.createQuery("SELECT nick FROM Estudiante").getResultList();
	    return listaEstudiantes;
	}
	public String obtenerEstudianteEmail(String e){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    String est = (String) em.createQuery("SELECT nick FROM Estudiante WHERE correo = :email").setParameter("email", e).getSingleResult();
	    return est;
	}
}
