package manejadores;

import java.util.List;

import javax.persistence.EntityManager;
import logica.Curso;
import utils.Conexion;

public class ManejadorCurso {
	private static ManejadorCurso instancia = null;
	
	private ManejadorCurso(){}
	
	public static ManejadorCurso getInstancia() {
		if(instancia == null)
			instancia = new ManejadorCurso();
		return instancia;
	}
	
	public void agregarCurso(Curso curso) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(curso);
        em.getTransaction().commit();
	}
	
	public boolean existeCurso(String nombre) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html#find%28java.lang.Class,%20java.lang.Object%29
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Curso.class, nombre) == null);
	}
	
	public Curso buscarCurso(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Curso.class, nombre);  
	}
	public List<String> obtenerCurso(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaInstitutos = em.createQuery("SELECT nombre FROM Curso").getResultList();
	    return listaInstitutos;
	}
	
	public List<String> obtenerPrevias(String s){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaInstitutos = em.createQuery("SELECT previas_nombre FROM curso_curso WHERE curso_nombre = :s").setParameter("s", s).getResultList();
	    return listaInstitutos;
	}
	
	public List<String> obtenerCursoInstituto(String s){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaInstitutos = em.createQuery("SELECT nombre FROM Curso WHERE instituto_nombre = :s").setParameter("s", s).getResultList();
	    return listaInstitutos;
	}
		
	public String nombreCurso(Curso curso) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return curso.getNombre();
	}
	
	public String[] infoCurso(String curso) {
		Curso cursoObject = buscarCurso(curso);
		String horas = Integer.toString(cursoObject.getCantHoras());
		String creditos = Integer.toString(cursoObject.getCreditos());
		String[] cursoRet = {
				cursoObject.getDescrip(), 
				cursoObject.getDuracion(),
				horas,
				creditos,
				cursoObject.getFechaR().toString(),
				cursoObject.getUrl(),
				cursoObject.getInstituto().getNombre()
				};
		return cursoRet;
	}
	
	public List<String> obtenerCursoPorPF(String pf){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaCurso = em.createQuery("SELECT curso_nombre FROM progformacion_curso WHERE progformacion_nombre = :pf").setParameter("pf", pf).getResultList();
	    return listaCurso;
	}
	
	public List<String> obtenerCursoPorBusqueda(String pf){
        Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        List<String> listaCurso = em.createQuery("SELECT nombre FROM curso WHERE nombre LIKE ':pf%' OR descrip LIKE ':pf%' ORDER BY nombre").setParameter("pf", pf).getResultList();
        return listaCurso;
    }
	
	public List<String> obtenerCursoPorFecha(String pf){
        Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        List<String> listaCurso = em.createQuery("SELECT nombre FROM curso WHERE nombre LIKE ':pf%' OR descrip LIKE ':pf%' ORDER BY fechar DESC").setParameter("pf", pf).getResultList();
        return listaCurso;
    }
}