package manejadores;

import java.util.List;

import javax.persistence.EntityManager;

import logica.Categoria;
import utils.Conexion;

public class ManejadorCategoria {
private static ManejadorCategoria instancia = null;
	
	private ManejadorCategoria(){}
	
	public static ManejadorCategoria getInstancia() {
		if(instancia == null)
			instancia = new ManejadorCategoria();
		return instancia;
	}
	
	public void agregarCategoria(Categoria categoria) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(categoria);
        em.getTransaction().commit();
	}
	
	public boolean existeCategoria(String nombre) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Categoria.class, nombre) == null);
	}
	
	public Categoria buscarCategorias(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Categoria.class, nombre);  
	}
	
	public List<String> obtenerCategorias(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaCategorias = em.createQuery("SELECT nombre FROM Categoria").getResultList();
	    return listaCategorias;
	}
	
	
}
