package manejadores;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import logica.Curso;
import logica.Edicion;
import utils.Conexion;

public class ManejadorEdicion {
private static ManejadorEdicion instancia = null;
	
	private ManejadorEdicion(){}
	
	public static ManejadorEdicion getInstancia() {
		if(instancia == null)
			instancia = new ManejadorEdicion();
		return instancia;
	}
	
	public void agregarEdicion(Edicion edicion) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(edicion);
        em.getTransaction().commit();
	}
	
	public boolean existeEdicion(String nombre) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html#find%28java.lang.Class,%20java.lang.Object%29
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Edicion.class, nombre) == null);
	}
	
	public Edicion buscarEdicion(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Edicion.class, nombre);  
	}
	public void restarCupo(String nombre) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        Edicion e = em.find(Edicion.class, nombre);
        em.getTransaction().begin();
        em.persist(e);
		e.setCupo(e.getCupo()-1);
		em.getTransaction().commit();
	}
	
	public String[] infoEdicion(String edicion) {
		Edicion edicionObject = buscarEdicion(edicion);			
		String[] edicionRet = {
				edicionObject.getNombre(),
				edicionObject.getFechaI().toString(),
				edicionObject.getFechaF().toString(),
				Integer.toString(edicionObject.getCupo()),
				edicionObject.getFechaPub().toString()
				};
		return edicionRet;
	}
}
