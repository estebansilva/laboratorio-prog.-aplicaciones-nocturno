package manejadores;

import java.util.List;

import javax.persistence.EntityManager;

import logica.Docente;
import utils.Conexion;

public class ManejadorDocente {
	
	private static ManejadorDocente instancia = null;
	
	private ManejadorDocente(){}
	
	public static ManejadorDocente getInstancia() {
		if(instancia == null)
			instancia = new ManejadorDocente();
		return instancia;
	}
	
	public void agregarDocente(Docente docente) {
		Conexion conexion= Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(docente);
        em.getTransaction().commit();
	}
	
	public boolean existeDocente(String nickname) {
		//https://docs.oracle.com/javaee/7/api/javax/persistence/EntityManager.html#find%28java.lang.Class,%20java.lang.Object%29
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return (em.find(Docente.class, nickname) != null);
	}
	
	public Docente buscarDocente(String nickname) {
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        return em.find(Docente.class, nickname);  
	}
	
	public boolean existeCorreo(String email) {
	  Conexion conexion=Conexion.getInstancia();
	  EntityManager em =conexion.getEntityManager();
	  boolean existe = !em.createQuery("SELECT correo FROM Docente WHERE correo = :email").setParameter("email", email).getResultList().isEmpty();
	  return existe;
	}
	
	public List<String> obtenerDocentes(){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaDocentes = em.createQuery("SELECT nick FROM Docente").getResultList();
	    return listaDocentes;
	}
	
	public List<String> obtenerDocentesInstituto(String instituto){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaDocentes = em.createQuery("SELECT nick FROM Docente WHERE instituto_nombre = :instituto").setParameter("instituto", instituto).getResultList();
	    return listaDocentes;
	}
	
	public List<String> obtenerDocentesEdicion(String edicion){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    List<String> listaDocentes = em.createQuery("SELECT docente_nick FROM docente_edicion WHERE edicion_nombre = :edicion").setParameter("edicion", edicion).getResultList();
	    System.out.print("    i    ");
	    return listaDocentes;
	}
	public String obtenerDocenteEmail(String e){
		Conexion conexion=Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    String doc = (String) em.createQuery("SELECT nick FROM Docente WHERE correo = :email").setParameter("email", e).getSingleResult();
	    return doc;
	}
}
