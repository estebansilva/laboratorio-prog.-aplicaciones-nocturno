package presentacion;

import java.awt.EventQueue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import datatypes.DtInstituto;
import datatypes.DtUsuario;
import excepciones.InstitutoRepetidaException;
import excepciones.UsuarioRepetidoException;
import interfaces.Fabrica;
import interfaces.IControladorAlta;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JPasswordField;

public class AltaUsuario extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JTextField campoNickname;
	private JTextField campoNombre;
	private JTextField campoApellido;
	private JTextField campoCorreo;
	private JComboBox comboBox;
	private JSpinner campoFechaN;
	private JToggleButton tglbtnDocente;
	private IControladorAlta iCAlta;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	public void refresh() {
        List<String> institutos = iCAlta.setInstitutos();
        this.comboBox.removeAllItems();
        for(String ins:institutos) {
            this.comboBox.addItem(ins);
        }
	}
	

    
	public AltaUsuario(IControladorAlta iCAlta) {
		setTitle("Alta Usuario");
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		this.iCAlta= iCAlta;
		setBounds(100, 100, 428, 334);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nickname");
		lblNewLabel.setBounds(26, 52, 61, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(26, 77, 98, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Apellido");
		lblNewLabel_2.setBounds(25, 130, 47, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Correo");
		lblNewLabel_3.setBounds(26, 102, 98, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Fec. de nacimiento");
		lblNewLabel_4.setBounds(26, 155, 98, 14);
		getContentPane().add(lblNewLabel_4);
		
		campoNickname = new JTextField();
		campoNickname.setBounds(97, 49, 131, 20);
		getContentPane().add(campoNickname);
		campoNickname.setColumns(10);
		
		campoNombre = new JTextField();
		campoNombre.setBounds(97, 74, 131, 20);
		getContentPane().add(campoNombre);
		campoNombre.setColumns(10);
		
		campoApellido = new JTextField();
		campoApellido.setBounds(97, 127, 129, 20);
		getContentPane().add(campoApellido);
		campoApellido.setColumns(10);
		
		campoCorreo = new JTextField();
		campoCorreo.setBounds(97, 102, 131, 20);
		getContentPane().add(campoCorreo);
		campoCorreo.setColumns(10);
		
		JButton Aceptar = new JButton("Aceptar");
		Aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		Aceptar.setBounds(51, 246, 89, 23);
		getContentPane().add(Aceptar);
		
		JButton Cancelar = new JButton("Cancelar");
		Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		Cancelar.setBounds(210, 246, 89, 23);
		getContentPane().add(Cancelar);
		
		
		campoFechaN = new JSpinner();
	    SimpleDateFormat model = new SimpleDateFormat("dd/MM/yyyy");
	    campoFechaN.setModel(new SpinnerDateModel(new Date(-2208973392000L), null, null, Calendar.MILLISECOND));
	    campoFechaN.setEditor(new JSpinner.DateEditor(campoFechaN, model.toPattern()));
	    campoFechaN.setBounds(134, 152, 79, 20);
	    
	    getContentPane().add(campoFechaN);
	    
	    comboBox = new JComboBox();
	    comboBox.setBounds(256, 149, 131, 20);
	    getContentPane().add(comboBox);
	    
	    tglbtnDocente = new JToggleButton("Docente");
	    tglbtnDocente.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		if(tglbtnDocente.getText()== "Docente") {
	    			tglbtnDocente.setText("Estudiante");
	    			comboBox.setEnabled(false);
	    		}
	    		else {
	    			tglbtnDocente.setText("Docente");
	    			comboBox.setEnabled(true);
	    		}
	    	}
	    });
	    
	    tglbtnDocente.setBounds(256, 98, 131, 23);
	    getContentPane().add(tglbtnDocente);
	    
	    JLabel lblNewLabel_5 = new JLabel("Alta Usuario");
	    lblNewLabel_5.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 22));
	    lblNewLabel_5.setBounds(131, 11, 168, 36);
	    getContentPane().add(lblNewLabel_5);
	    
	    passwordField = new JPasswordField();
	    passwordField.setBounds(97, 180, 129, 20);
	    getContentPane().add(passwordField);
	    
	    JLabel lblContrasea = new JLabel("Contrase\u00F1a");
	    lblContrasea.setBounds(14, 183, 73, 14);
	    getContentPane().add(lblContrasea);
	    
	    JLabel lblNewLabel_6 = new JLabel("Re:Contrase\u00F1a");
	    lblNewLabel_6.setBounds(14, 211, 73, 14);
	    getContentPane().add(lblNewLabel_6);
	    
	    passwordField_1 = new JPasswordField();
	    passwordField_1.setBounds(97, 211, 129, 20);
	    getContentPane().add(passwordField_1);
	}
	private void Cancelar(ActionEvent ae) {
		limpiarFormulario();
		setVisible(false);
	}
	
	private void Aceptar(ActionEvent ae) {
		if (checkFormulario()) {
			String nick = this.campoNickname.getText();
	        String nombre = this.campoNombre.getText();
	        String apellido = this.campoApellido.getText();
	        String correo = this.campoCorreo.getText();
	        char pass[]= this.passwordField.getPassword();
	        String contrasenia= new String(pass); 
	        Date fechaN = (Date)this.campoFechaN.getValue();
	        DtUsuario dtusuario = new DtUsuario(nick,nombre, apellido, correo, fechaN,contrasenia);
	        if (this.tglbtnDocente.getText()=="Docente") {
	        	DtInstituto dtinstituto = new DtInstituto((String)this.comboBox.getSelectedItem());
	        	try {
		            this.iCAlta.ingresarDocente(dtusuario, dtinstituto);
		            JOptionPane.showMessageDialog(this, "Docente creado con exito", "Agregar Usuario", JOptionPane.INFORMATION_MESSAGE);
		            limpiarFormulario();
		    		setVisible(false);
		            } catch (UsuarioRepetidoException e) {
		                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Usuario", JOptionPane.ERROR_MESSAGE);
		            }
	        }
	        else {
	        	try {
			        this.iCAlta.ingresarEstudiante(dtusuario);
			        JOptionPane.showMessageDialog(this, "Usuario creado con exito", "Agregar Instituto", JOptionPane.INFORMATION_MESSAGE);
			        limpiarFormulario();
			        setVisible(false);
			        } catch (UsuarioRepetidoException e) {
			            JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Instituto", JOptionPane.ERROR_MESSAGE);
			        }
	        }
		}
	}
	private boolean checkFormulario() {
        String nick = this.campoNickname.getText();
        String nombre = this.campoNombre.getText();
        String apellido = this.campoApellido.getText();
        String correo = this.campoCorreo.getText();
        char pass[]= this.passwordField.getPassword();
        String contrasenia= new String(pass); 
        char pass2[]= this.passwordField_1.getPassword();
        String contrasenia2= new String(pass2); 
        
        Date fechaN = (Date)this.campoFechaN.getValue();
        Date fechaActual = new Date();
        if (nombre.isEmpty() || nick.isEmpty() || apellido.isEmpty() || correo.isEmpty() || contrasenia.isEmpty() || contrasenia2.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Ingresar Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (fechaN.after(fechaActual)) {
            JOptionPane.showMessageDialog(this, "La fecha de nacimiento no puede ser mayor a la fecha actual", "Ingresar Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        Date fechaprueba = new Date(01/01/1900);
        if (fechaprueba.after(fechaN)) {
            JOptionPane.showMessageDialog(this, "La fecha de nacimiento no puede ser anterior al a�o 1900", "Ingresar Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (this.tglbtnDocente.getText()=="Docente") {
        	if(this.comboBox.getSelectedItem() == null) {
	        	JOptionPane.showMessageDialog(this, "No se eligio instituto para el Docente", "Ingresar Usuario",
	                    JOptionPane.ERROR_MESSAGE);
	            return false;
        	}
        }
        if (!contrasenia.equals(contrasenia2)) {
            JOptionPane.showMessageDialog(this, "Las contrase�as no coinciden", "Ingresar Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
	private void limpiarFormulario() {
		 campoNickname.setText("");
		 campoNombre.setText("");
		 campoCorreo.setText("");
		 campoApellido.setText("");
		 passwordField.setText("");
		 passwordField_1.setText("");
 }
}
