package presentacion;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

import datatypes.DtEdicion;
import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;

public class InfoEdicion extends JInternalFrame {
	private JLabel lblNombre;
	private JLabel lblFechaI;
	private JLabel lblFechaF;
	private JLabel lblFechaP;
	private JLabel lblCupo;
	private JList list;
	private JList listDocente;
	
	private IControladorAlta iCAlta;
	public InfoEdicion(IControladorAlta iCAlta, IControladorModificarUsuario iMod, String ed) {
		setBounds(100, 100, 457, 436);
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Edicion de Curso");
        
getContentPane().setLayout(null);
		
		
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(144, 362, 144, 33);
		getContentPane().add(btnCancelar);
		btnCancelar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
		
		JLabel lblConsultaDeEdicin = new JLabel("Consulta de Edici\u00F3n de Curso");
		lblConsultaDeEdicin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblConsultaDeEdicin.setBounds(76, 11, 276, 32);
		getContentPane().add(lblConsultaDeEdicin);
	
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(28, 54, 58, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha Inicio:");
		lblNewLabel_2.setBounds(29, 79, 74, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha Fin:");
		lblNewLabel_3.setBounds(29, 104, 74, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Cupo");
		lblNewLabel_4.setBounds(29, 129, 46, 14);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha Publicacion:");
		lblNewLabel_5.setBounds(29, 154, 115, 14);
		getContentPane().add(lblNewLabel_5);
		
		lblNombre = new JLabel("");
		lblNombre.setBounds(144, 54, 144, 14);
		getContentPane().add(lblNombre);
		
		lblFechaI = new JLabel("");
		lblFechaI.setBounds(144, 79, 130, 14);
		getContentPane().add(lblFechaI);
		
		lblFechaF = new JLabel("");
		lblFechaF.setBounds(144, 104, 130, 14);
		getContentPane().add(lblFechaF);
		
		lblCupo = new JLabel("");
		lblCupo.setBounds(144, 129, 114, 14);
		getContentPane().add(lblCupo);
		
		lblFechaP = new JLabel("");
		lblFechaP.setBounds(144, 154, 130, 14);
		getContentPane().add(lblFechaP);
		
		JLabel lblNewLabel_6 = new JLabel("Inscriptos:");
		lblNewLabel_6.setBounds(29, 179, 74, 14);
		getContentPane().add(lblNewLabel_6);
		
		list = new JList();
		list.setBounds(144, 179, 170, 65);
		getContentPane().add(list);
		btnCancelar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		
		
		DtEdicion dte= this.iCAlta.darInfoEdicion(ed);
		
		String fecha;
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		lblNombre.setText(dte.getNombre());
		fecha = formato.format(dte.getFechaI());
		lblFechaI.setText(fecha);
		fecha = formato.format(dte.getFechaF());
		lblFechaF.setText(fecha);
		fecha =formato.format(dte.getFechaPub());
		lblFechaP.setText(fecha);
		lblCupo.setText(Integer.toString(dte.getCupo()));
		
		JLabel lblNewLabel_6_1 = new JLabel("Docentes");
		lblNewLabel_6_1.setBounds(28, 268, 74, 14);
		getContentPane().add(lblNewLabel_6_1);
		
		listDocente = new JList();
		listDocente.setBounds(144, 267, 170, 65);
		getContentPane().add(listDocente);
		try {
			refreshList(this.iCAlta.getEstudianteEnEdicion(ed));
		}catch (Exception e2) {
			
		}
		try {
			refreshListDoc(this.iCAlta.getDocenteEdicion(ed));
		}catch (Exception e2) {
			
		}
	}
	
	public void refreshList(List<String> s) {
		this.list.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(s != null) {
	        for(String c:s) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Ediciones actualmente");
		}
		list.setModel(modelo);
	}
	
	private void refreshListDoc(List<String> d) {
		this.listDocente.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(d != null) {
	        for(String c:d) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Docentes actualmente");
		}
		listDocente.setModel(modelo);
		
	}
	

}
