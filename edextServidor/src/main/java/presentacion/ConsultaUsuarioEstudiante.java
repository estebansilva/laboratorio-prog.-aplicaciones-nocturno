package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import datatypes.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.*;
import javax.swing.JScrollPane;

public class ConsultaUsuarioEstudiante extends JInternalFrame {
	private JTextField textFieldnick;
	private JTextField textField_nom;
	private JTextField textField_ape;
	private JTextField textField_corr;
	private JTextField textField_fecha;
	private JList list;

	public ConsultaUsuarioEstudiante(DtEstudiante e) {
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Estudiante");
		setBounds(100, 100, 475, 394);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Nick");
		lblNewLabel_1.setBounds(60, 13, 95, 13);
		getContentPane().add(lblNewLabel_1);
		
		textFieldnick = new JTextField();
		textFieldnick.setEditable(false);
		textFieldnick.setColumns(10);
		textFieldnick.setBounds(180, 10, 189, 19);
		getContentPane().add(textFieldnick);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre");
		lblNewLabel_2.setBounds(60, 53, 95, 13);
		getContentPane().add(lblNewLabel_2);
		
		textField_nom = new JTextField();
		textField_nom.setEditable(false);
		textField_nom.setColumns(10);
		textField_nom.setBounds(180, 50, 189, 19);
		getContentPane().add(textField_nom);
		
		JLabel lblNewLabel_3 = new JLabel("Apellido");
		lblNewLabel_3.setBounds(60, 94, 95, 13);
		getContentPane().add(lblNewLabel_3);
		
		textField_ape = new JTextField();
		textField_ape.setEditable(false);
		textField_ape.setColumns(10);
		textField_ape.setBounds(180, 91, 189, 19);
		getContentPane().add(textField_ape);
		
		JLabel lblNewLabel_4 = new JLabel("Correo");
		lblNewLabel_4.setBounds(60, 137, 95, 13);
		getContentPane().add(lblNewLabel_4);
		
		textField_corr = new JTextField();
		textField_corr.setEditable(false);
		textField_corr.setColumns(10);
		textField_corr.setBounds(180, 134, 189, 19);
		getContentPane().add(textField_corr);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha");
		lblNewLabel_5.setBounds(60, 182, 95, 13);
		getContentPane().add(lblNewLabel_5);
		
		textField_fecha = new JTextField();
		textField_fecha.setEditable(false);
		textField_fecha.setColumns(10);
		textField_fecha.setBounds(180, 179, 189, 19);
		getContentPane().add(textField_fecha);
		
		JLabel lblNewLabel_6 = new JLabel("Inscripto a: ");
		lblNewLabel_6.setBounds(60, 220, 95, 13);
		getContentPane().add(lblNewLabel_6);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(180, 220, 189, 78);
		getContentPane().add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		
		
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnNewButton.setBounds(222, 314, 83, 21);
		getContentPane().add(btnNewButton);
		
		textFieldnick.setText(e.getNick());
		textField_nom.setText(e.getNombre());
		textField_ape.setText(e.getApellido());
		textField_corr.setText(e.getCorreo());
		textField_fecha.setText(e.getFechaNac().toString());

	}
	
	public void refreshList(List<String> s) {
		this.list.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(s != null) {
	        for(String c:s) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Ediciones actualmente");
		}
		list.setModel(modelo);
	}
}
