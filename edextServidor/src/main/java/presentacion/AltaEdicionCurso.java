package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;


import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import datatypes.DtEdicion;
import datatypes.DtCurso;
import interfaces.IControladorAlta;
import excepciones.EdicionRepetidaException;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JScrollPane;

public class AltaEdicionCurso extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private IControladorAlta iCAlta;
	private JTextField textField_Inicio;
	private JTextField textField_Fin;
	private JTextField textField_Cupos;
	private JComboBox comboBox;
	private JComboBox comboBox_1;	
	private JTextField textField_fechapub;
	private JList<String> list;
	private Date d;
	private JTextField textField_nombreEdicion;
	private JTextField textField_nombreEdicionCurso;

	public AltaEdicionCurso(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Alta Edicion Curso");
		
		
		setBounds(100, 100, 434, 369);
		getContentPane().setLayout(null);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		btnAceptar.setBounds(92, 303, 97, 21);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		btnCancelar.setBounds(229, 303, 97, 21);
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("Instituto");
		lblNewLabel.setBounds(43, 75, 54, 16);
		getContentPane().add(lblNewLabel);
		
		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setBounds(43, 102, 46, 14);
		getContentPane().add(lblCurso);
		
		comboBox = new JComboBox();
		comboBox.setBounds(92, 73, 139, 20);
		getContentPane().add(comboBox);
		comboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(comboBox.getSelectedItem() != null) {
					refreshCurso((String)comboBox.getSelectedItem());
					try {
					refreshList((String)comboBox.getSelectedItem());
					} catch (Exception es) {
						System.out.print("No dos");
					}
				}	
			}
		});
		
		JLabel lblAltaDeEdicin = new JLabel("Alta de Edici\u00F3n de Curso");
		lblAltaDeEdicin.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 19));
		lblAltaDeEdicin.setBounds(92, 0, 267, 40);
		getContentPane().add(lblAltaDeEdicin);
		
		JLabel lblFechaInicio = new JLabel("Fecha Inicio");
		lblFechaInicio.setBounds(254, 76, 72, 14);
		getContentPane().add(lblFechaInicio);
		
		JLabel lblFechaFin = new JLabel("Fecha Fin");
		lblFechaFin.setBounds(254, 102, 46, 14);
		getContentPane().add(lblFechaFin);
		
		textField_Inicio = new JTextField();
		textField_Inicio.setBounds(319, 73, 86, 20);
		getContentPane().add(textField_Inicio);
		textField_Inicio.setColumns(10);
		
		textField_Fin = new JTextField();
		textField_Fin.setBounds(319, 99, 86, 20);
		getContentPane().add(textField_Fin);
		textField_Fin.setColumns(10);
				
		JLabel lblNewLabel_1 = new JLabel("Docentes");
		lblNewLabel_1.setBounds(12, 191, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblCupos = new JLabel("Cupos");
		lblCupos.setBounds(254, 129, 46, 14);
		getContentPane().add(lblCupos);
		
		textField_Cupos = new JTextField();
		textField_Cupos.setBounds(319, 126, 86, 20);
		getContentPane().add(textField_Cupos);
		textField_Cupos.setColumns(10);
		
		textField_fechapub = new JTextField();
		textField_fechapub.setBounds(319, 37, 86, 20);
		getContentPane().add(textField_fechapub);
		textField_fechapub.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(72, 191, 333, 101);
		getContentPane().add(scrollPane);
		
		list = new JList<String>();
		scrollPane.setViewportView(list);
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL_WRAP);
			
		JLabel lblNewLabel_1_1 = new JLabel("Nombre de la Edici\u00F3n del Curso");
		lblNewLabel_1_1.setBounds(12, 156, 164, 14);
		getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha ");
		lblNewLabel_2.setBounds(280, 40, 46, 14);
		getContentPane().add(lblNewLabel_2);
		
		//cursos
		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(92, 99, 139, 20);
		getContentPane().add(comboBox_1);
		
		textField_nombreEdicionCurso = new JTextField();
		textField_nombreEdicionCurso.setBounds(199, 154, 206, 20);
		getContentPane().add(textField_nombreEdicionCurso);
		textField_nombreEdicionCurso.setColumns(10);

	}
	private void Cancelar(ActionEvent ae) {
		limpiarFormulario();
		setVisible(false);
	}
	


	public void refreshInstituto() {
        List<String> institutos = iCAlta.setInstitutos();
        this.comboBox.removeAllItems();
        for(String ins:institutos) {
            this.comboBox.addItem(ins);
        }
        
	}
	
	public void refreshCurso(String s) {
		List<String> curso = iCAlta.setCursos(s);
        this.comboBox_1.removeAllItems();
        for(String c:curso) {
            this.comboBox_1.addItem(c);
        }

	}
	
	@SuppressWarnings("unchecked")
	public void refreshList(String s) {
		this.list.removeAll();
		List<String> docentes = iCAlta.getDocentes(s);
		ListModel<String> modelo = new DefaultListModel<String>();
		if(docentes != null) {
	        for(String d:docentes) {
	        	((DefaultListModel<String>) modelo).addElement(d.toString());
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay docentes actualmente");
		}
		list.setModel(modelo);
	}
	

	
	private void Aceptar(ActionEvent ae) {
		if (checkFormulario()) {
			String fechaI = this.textField_Inicio.getText();
	        String fechaF = this.textField_Fin.getText();
	        String nombre = this.textField_nombreEdicionCurso.getText();
	        int cupo = Integer.parseInt(this.textField_Cupos.getText());     
	        String fechapub = this.textField_fechapub.getText();
	        List<String> docentes = this.list.getSelectedValuesList();
	        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	        String curso= comboBox_1.getSelectedItem().toString();
	        Date fechaActual = new Date();
	        Date date = null;
	        Date date1= null;
	        Date date2= null;
	        
			try {
				date = format.parse(fechapub);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				date1 = format.parse(fechaI);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				date2 = format.parse(fechaF);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				 
				DtEdicion dt= new DtEdicion(nombre,date1,date2,cupo,date);
				try {
		            this.iCAlta.ingresarEdicion(dt,docentes, curso);
		            JOptionPane.showMessageDialog(this, "La edicion se creo correctamente", "Agregar Edicion", JOptionPane.INFORMATION_MESSAGE);
		            limpiarFormulario();
		    		setVisible(false);
		            } catch (EdicionRepetidaException e) {
		                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Edicion", JOptionPane.ERROR_MESSAGE);
		            }
		}
		limpiarFormulario();
	}
	
	private boolean checkFormulario() {
		String fechaI = this.textField_Inicio.getText();
        String fechaF = this.textField_Fin.getText();
        String nombre = this.textField_nombreEdicionCurso.getText();
        String cupos = this.textField_Cupos.getText();
        String fechapub = this.textField_fechapub.getText();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        Date fechaActual = new Date();
        Date date = null;
        Date date1= null;
        Date date2= null;
        
        if (fechaI.isEmpty() || fechaF.isEmpty() || nombre.isEmpty() || cupos.isEmpty() || fechapub.isEmpty() ||(comboBox.getSelectedItem() == null) || (comboBox_1.getSelectedItem() == null) ) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        String curso= (String)comboBox_1.getSelectedItem();
		try {
			date = format.parse(fechapub);
		} catch (ParseException e1) {
			JOptionPane.showMessageDialog(this, "La fecha debe ser de forma dd/mm/yyyy", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
			 return false;
		}
		try {
			date1 = format.parse(fechaI);
		} catch (ParseException e1) {
			JOptionPane.showMessageDialog(this, "La fecha debe ser de forma dd/mm/yyyy", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
			 return false;
		}
		try {
			date2 = format.parse(fechaF);
		} catch (ParseException e1) {
			JOptionPane.showMessageDialog(this, "La fecha debe ser de forma dd/mm/yyyy", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
			 return false;
		}
		
		if (date.after(date1)) {
            JOptionPane.showMessageDialog(this, "Fecha publicacion no puede ser posterior a la de Fecha de Inicio", "Agregar edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
		}
		 if (date1.after(date2)) {
	            JOptionPane.showMessageDialog(this, "Fecha Inicio no puede ser posterior a la Fecha Fin", "Agregar edicion",
	                    JOptionPane.ERROR_MESSAGE);
	            return false;
	     }
		 if (fechaActual.after(date)) {
	            JOptionPane.showMessageDialog(this, "Fecha de publicacion no puede ser anterior a la fecha actual", "Agregar edicion",
	                    JOptionPane.ERROR_MESSAGE);
	            return false;
	     }
        try {
            Integer.parseInt(cupos);
        } catch (NumberFormatException e1) {
            JOptionPane.showMessageDialog(this, "El cupo debe ser un numero", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if( Integer.parseInt(cupos) <= 0) {
        	JOptionPane.showMessageDialog(this, "El cupo debe ser mayor a 0", "Agregar Edicion",
                    JOptionPane.ERROR_MESSAGE);
        	return false;
        }
        return true;
    }
	private void limpiarFormulario() {
		textField_Inicio.setText("");
		textField_Fin.setText("");
		textField_nombreEdicionCurso.setText("");
		textField_Cupos.setText("");
		textField_fechapub.setText("");

 }
}
