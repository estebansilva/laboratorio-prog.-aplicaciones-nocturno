package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.ListModel;
import javax.swing.JScrollBar;


public class ConsultaCurso extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IControladorAlta iCAlta;
	private IControladorModificarUsuario iMod;
	private JComboBox comboBoxInstitutos;
	private JComboBox comboBoxCursos;
	private JComboBox comboBoxEdiciones;
	private JComboBox comboBoxpformacion;
	private JLabel lblNewLabel_valueNombre;
	private JLabel lblNewLabel_value_cantH;
	private JLabel lblNewLabel_valueFechaR;
	private JLabel lblNewLabel_valueInstituto;
	private JLabel lblNewLabel_valueDuracion;
	private JLabel lblNewLabel_valueCreditos;
	private JLabel lblNewLabel_valueURL;
	private JLabel lblNewLabel_valueDesc;
	private JLabel lblNewLabel;
	private JButton btnNewButtonPFormacion;
	private JButton btnNewButtonEdiciones;
	private JButton btnInfoCurso;
	private JButton btnBuscarCursos;
	private JLabel lblNombre;
	private JLabel lblDuracion;
	private JLabel lblcreditos;
	private JLabel lblurl;
	private JLabel lblDescripcion;
	private JLabel lblcantHoras;
	private JLabel lblfechaR;
	private JLabel lblinstituto;
	private JLabel lbledicionCurso;
	private JLabel lblProgFormacion;
	private JList<String> list;
	private JScrollPane scrollPane;
	private JList<String> list_1;
	private JScrollPane scrollPane_1;
	
	
	

	/**
	 * Create the frame.
	 */
	public ConsultaCurso(IControladorAlta iCAlta,IControladorModificarUsuario iMod) {
		setIconifiable(true);
		setMaximizable(true);
		setTitle("Consulta de Curso");
		this.iCAlta= iCAlta;
		this.iMod= iMod;
		setResizable(true);
		setClosable(true);
		setBounds(100, 100, 450, 532);
		getContentPane().setLayout(null);
		
		JLabel lblSeleccioneInstituto = new JLabel("Seleccione Instituto:");
		lblSeleccioneInstituto.setToolTipText("");
		lblSeleccioneInstituto.setBounds(10, 15, 126, 14);
		getContentPane().add(lblSeleccioneInstituto);
		
		comboBoxInstitutos = new JComboBox();
		comboBoxInstitutos.setToolTipText("");
		comboBoxInstitutos.setMaximumRowCount(20);
		comboBoxInstitutos.setBounds(137, 12, 198, 20);
		getContentPane().add(comboBoxInstitutos);
		
		btnBuscarCursos = new JButton("Buscar");
		btnBuscarCursos.setEnabled(false);
		btnBuscarCursos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBoxInstitutos.getSelectedItem() !=null)
					buscarCursosActionPerformed(e);
			}
		});
		btnBuscarCursos.setBounds(345, 12, 79, 20);
		getContentPane().add(btnBuscarCursos);
		
		JLabel lblSeleccioneCurso = new JLabel("Seleccione Curso:");
		lblSeleccioneCurso.setToolTipText("");
		lblSeleccioneCurso.setBounds(10, 44, 126, 14);
		getContentPane().add(lblSeleccioneCurso);
		
		comboBoxCursos = new JComboBox();
		comboBoxCursos.setEnabled(false);
		comboBoxCursos.setBounds(137, 41, 198, 20);
		getContentPane().add(comboBoxCursos);
		
		btnInfoCurso = new JButton("Aceptar");
		btnInfoCurso.setEnabled(false);
		btnInfoCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBoxCursos.getSelectedItem() !=null)
					infoCursoActionPerformed(e);
			}
			
		});
		btnInfoCurso.setBounds(345, 41, 79, 20);
		getContentPane().add(btnInfoCurso);
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(35, 97, 101, 14);
		lblNombre.setVisible(false);		
		getContentPane().add(lblNombre);
		
		lblDuracion = new JLabel("Duracion:");
		lblDuracion.setBounds(35, 147, 101, 14);
		lblDuracion.setVisible(false);
		getContentPane().add(lblDuracion);
		
		lblcreditos = new JLabel("Creditos:");
		lblcreditos.setBounds(35, 197, 101, 14);
		lblcreditos.setVisible(false);
		getContentPane().add(lblcreditos);
		
		lblurl = new JLabel("URL:");
		lblurl.setBounds(35, 249, 101, 14);
		lblurl.setVisible(false);
		getContentPane().add(lblurl);
		
		lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(35, 122, 101, 14);
		lblDescripcion.setVisible(false);
		getContentPane().add(lblDescripcion);
		
		lblcantHoras = new JLabel("Cant Horas:");
		lblcantHoras.setBounds(35, 172, 101, 14);
		lblcantHoras.setVisible(false);
		getContentPane().add(lblcantHoras);
		
		lblfechaR = new JLabel("Fecha Reg:");
		lblfechaR.setBounds(35, 221, 101, 14);
		lblfechaR.setVisible(false);
		getContentPane().add(lblfechaR);
		
		lblinstituto = new JLabel("Instituto:");
		lblinstituto.setBounds(35, 274, 101, 14);
		lblinstituto.setVisible(false);
		getContentPane().add(lblinstituto);
		
		lbledicionCurso = new JLabel("Ediciones:");
		lbledicionCurso.setBounds(21, 450, 103, 14);
		lbledicionCurso.setVisible(false);
		getContentPane().add(lbledicionCurso);
		
		lblProgFormacion = new JLabel("Prog Formacion:");
		lblProgFormacion.setBounds(21, 474, 103, 14);
		lblProgFormacion.setVisible(false);
		getContentPane().add(lblProgFormacion);
		
		comboBoxEdiciones = new JComboBox();
		comboBoxEdiciones.setEnabled(false);
		comboBoxEdiciones.setVisible(false);
		comboBoxEdiciones.setBounds(137, 447, 198, 20);
		getContentPane().add(comboBoxEdiciones);	
		
		comboBoxpformacion = new JComboBox();
		comboBoxpformacion.setEnabled(false);
		comboBoxpformacion.setVisible(false);
		comboBoxpformacion.setBounds(137, 471, 198, 20);
		getContentPane().add(comboBoxpformacion);
		
		btnNewButtonEdiciones = new JButton("Ver");
		btnNewButtonEdiciones.setEnabled(false);
		btnNewButtonEdiciones.setVisible(false);
		btnNewButtonEdiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verEdicionActionPerformed(e);
			}
		});
		btnNewButtonEdiciones.setBounds(345, 447, 79, 20);
		getContentPane().add(btnNewButtonEdiciones);
		
		btnNewButtonPFormacion = new JButton("Ver");
		btnNewButtonPFormacion.setEnabled(false);
		btnNewButtonPFormacion.setVisible(false);
		btnNewButtonPFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verPFormacionActionPerformed(e);
			}
		});
		btnNewButtonPFormacion.setBounds(345, 471, 79, 20);
		getContentPane().add(btnNewButtonPFormacion);
		
		lblNewLabel_valueNombre = new JLabel("");
		lblNewLabel_valueNombre.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueNombre.setBounds(136, 97, 263, 14);
		getContentPane().add(lblNewLabel_valueNombre);
		
		lblNewLabel_value_cantH = new JLabel("");
		lblNewLabel_value_cantH.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_value_cantH.setBounds(136, 172, 263, 14);
		getContentPane().add(lblNewLabel_value_cantH);
		
		lblNewLabel_valueFechaR = new JLabel("");
		lblNewLabel_valueFechaR.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueFechaR.setBounds(136, 221, 263, 14);
		getContentPane().add(lblNewLabel_valueFechaR);
		
		lblNewLabel_valueInstituto = new JLabel("");
		lblNewLabel_valueInstituto.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueInstituto.setBounds(136, 274, 263, 14);
		getContentPane().add(lblNewLabel_valueInstituto);
		
		lblNewLabel_valueDuracion = new JLabel("");
		lblNewLabel_valueDuracion.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueDuracion.setBounds(136, 147, 263, 14);
		getContentPane().add(lblNewLabel_valueDuracion);
		
		lblNewLabel_valueCreditos = new JLabel("");
		lblNewLabel_valueCreditos.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueCreditos.setBounds(136, 197, 263, 14);
		getContentPane().add(lblNewLabel_valueCreditos);
		
		lblNewLabel_valueURL = new JLabel("");
		lblNewLabel_valueURL.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueURL.setBounds(136, 249, 263, 14);
		getContentPane().add(lblNewLabel_valueURL);
		
		lblNewLabel_valueDesc = new JLabel("");
		lblNewLabel_valueDesc.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueDesc.setBounds(136, 122, 263, 14);
		getContentPane().add(lblNewLabel_valueDesc);
		
		lblNewLabel = new JLabel("Previas");
		lblNewLabel.setBounds(35, 298, 89, 13);
		lblNewLabel.setVisible(false);
		getContentPane().add(lblNewLabel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(137, 299, 273, 73);
		scrollPane.setVisible(false);
		getContentPane().add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		list.setVisibleRowCount(20);
		list.setValueIsAdjusting(true);
		
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(139, 377, 260, 62);
		scrollPane_1.setVisible(false);
		getContentPane().add(scrollPane_1);
		
		list_1 = new JList();
		scrollPane_1.setRowHeaderView(list_1);
		list_1.setVisibleRowCount(20);
		list_1.setValueIsAdjusting(true);
		
		
		
		JLabel lblNewLabel_1 = new JLabel("Categorias");
		lblNewLabel_1.setBounds(35, 378, 72, 14);
		getContentPane().add(lblNewLabel_1);
		
		
		

	}
	
	//////////////////////////////////////////////
	//////////////////FUNCIONES///////////////////
	//////////////////////////////////////////////
	
	
	
	
	public void clearAllData() {
		comboBoxCursos.removeAllItems();
		comboBoxCursos.setEnabled(false);
		btnInfoCurso.setEnabled(false);
		
		lblNombre.setVisible(false);
		lblDuracion.setVisible(false);
		lblcreditos.setVisible(false);
		lblurl.setVisible(false);
		lblDescripcion.setVisible(false);
		lblcantHoras.setVisible(false);
		lblfechaR.setVisible(false);
		lblinstituto.setVisible(false);
		lbledicionCurso.setVisible(false);
		lblProgFormacion.setVisible(false);
		lblNewLabel.setVisible(false);
		comboBoxEdiciones.setVisible(false);
		comboBoxpformacion.setVisible(false);
		btnNewButtonEdiciones.setVisible(false);
		btnNewButtonPFormacion.setVisible(false);
		scrollPane.setVisible(false);
		
		lblNewLabel_valueNombre.setText("");
		lblNewLabel_value_cantH.setText("");
		lblNewLabel_valueFechaR.setText("");
		lblNewLabel_valueInstituto.setText("");
		lblNewLabel_valueDuracion.setText("");
		lblNewLabel_valueCreditos.setText("");
		lblNewLabel_valueURL.setText("");
		lblNewLabel_valueDesc.setText("");
		
		lblNewLabel_valueNombre.setVisible(false);
		lblNewLabel_value_cantH.setVisible(false);
		lblNewLabel_valueFechaR.setVisible(false);
		lblNewLabel_valueInstituto.setVisible(false);
		lblNewLabel_valueDuracion.setVisible(false);
		lblNewLabel_valueCreditos.setVisible(false);
		lblNewLabel_valueURL.setVisible(false);
		lblNewLabel_valueDesc.setVisible(false);
		
		comboBoxEdiciones.removeAllItems();
		comboBoxEdiciones.setVisible(false);
		comboBoxEdiciones.setEnabled(false);
		comboBoxpformacion.removeAllItems();
		comboBoxpformacion.setVisible(false);
		comboBoxpformacion.setEnabled(false);
		btnNewButtonEdiciones.setEnabled(false);
		btnNewButtonEdiciones.setVisible(false);
		btnNewButtonPFormacion.setEnabled(false);
		btnNewButtonPFormacion.setVisible(false);		
	}
	
	public void inicializarComboBox() {
        List<String> institutos = iCAlta.setInstitutos();
        if (institutos != null) {
        	btnBuscarCursos.setEnabled(true);
        	this.comboBoxInstitutos.removeAllItems();
	        for(String ins:institutos) {
	        	this.comboBoxInstitutos.addItem(ins);
	        }
        }
	}
	public void refreshList(String s) {
		this.list.removeAll();
		List<String> cursos = iCAlta.getPreviaCurso(s);
		ListModel<String> modelo = new DefaultListModel<String>();
		if(cursos != null) {
	        for(String c:cursos) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay cursos actualmente");
		}
		list.setModel(modelo);
	}
	
	public void refreshCat(String a) {
		this.list_1.removeAll();
		List<String> cat = iCAlta.obtenerCategoriadeCurso(a);
		ListModel<String> modelo2 = new DefaultListModel<String>();
		if(cat != null){
			for(String c:cat) {
				((DefaultListModel<String>) modelo2).addElement(c);
			}
		}else {
		((DefaultListModel<String>) modelo2).addElement("No hay categorias actualmente");
	}
		list_1.setModel(modelo2);
	}
	
	public void buscarCursosActionPerformed(ActionEvent e) {
		//obtengo instituto seleccionado del combobox
		String institutoElegido = this.comboBoxInstitutos.getSelectedItem().toString();
		if(institutoElegido.length() != 0) {			
			//creo lista de cursos asociados
			List<String> cursos = this.iCAlta.setCursos(institutoElegido);
			//agrego values al combobox de cursos
			if (cursos != null) {
				this.comboBoxCursos.removeAllItems();
		        for(String c: cursos) {
		        	this.comboBoxCursos.addItem(c);
		        }
		        comboBoxCursos.setEnabled(true);
		        btnInfoCurso.setEnabled(true);
			}
		}
	}
	
	private void infoCursoActionPerformed(ActionEvent e) {
		//obtengo curso seleccionado del combobox
		String cursoElegido = this.comboBoxCursos.getSelectedItem().toString();		
		if (cursoElegido.length()!=0) {
			//Hago visibles las labels
			lblNombre.setVisible(true);
			lblDuracion.setVisible(true);
			lblcreditos.setVisible(true);
			lblurl.setVisible(true);
			lblDescripcion.setVisible(true);
			lblcantHoras.setVisible(true);
			lblfechaR.setVisible(true);
			lblinstituto.setVisible(true);
			lbledicionCurso.setVisible(true);
			lblProgFormacion.setVisible(true);
			lblNewLabel.setVisible(true);
			comboBoxEdiciones.setVisible(true);
			comboBoxpformacion.setVisible(true);
			btnNewButtonEdiciones.setVisible(true);
			btnNewButtonPFormacion.setVisible(true);
			lblNewLabel_valueNombre.setVisible(true);
			lblNewLabel_value_cantH.setVisible(true);
			lblNewLabel_valueFechaR.setVisible(true);
			lblNewLabel_valueInstituto.setVisible(true);
			lblNewLabel_valueDuracion.setVisible(true);
			lblNewLabel_valueCreditos.setVisible(true);
			lblNewLabel_valueURL.setVisible(true);
			lblNewLabel_valueDesc.setVisible(true);
			scrollPane.setVisible(true);
			scrollPane_1.setVisible(true);
			
			//obtengo los datos del curso
			String[] atributosCurso = this.iCAlta.getInfoCurso(cursoElegido);//ESTA FALLANDO*********************************
									
			//Muestro los datos del curso
			lblNewLabel_valueNombre.setText(cursoElegido);
			lblNewLabel_value_cantH.setText(atributosCurso[2]);
			lblNewLabel_valueFechaR.setText(atributosCurso[4]);
			lblNewLabel_valueInstituto.setText(atributosCurso[6]);
			lblNewLabel_valueDuracion.setText(atributosCurso[1]);
			lblNewLabel_valueCreditos.setText(atributosCurso[3]);
			lblNewLabel_valueURL.setText(atributosCurso[5]);
			lblNewLabel_valueDesc.setText(atributosCurso[0]);
			
			try {
				refreshList((String)this.comboBoxCursos.getSelectedItem());
				refreshCat((String)this.comboBoxCursos.getSelectedItem());
			}catch (Exception e2) {
				
			}
				
			
			
			//obtengo las ediciones
			this.comboBoxEdiciones.removeAllItems();
			try {	
			List<String> edicionesCurso = this.iCAlta.setEdiciones(cursoElegido);			
			if (!(edicionesCurso.isEmpty())) {
				comboBoxEdiciones.setEnabled(true);
				btnNewButtonEdiciones.setEnabled(true);
				
		        for(String ed:edicionesCurso) {
		        	this.comboBoxEdiciones.addItem(ed);
		        }
			}	
			} catch (Exception e3) {
				this.comboBoxEdiciones.removeAllItems();
			}
			
			//obtengo planes de formacion
			this.comboBoxpformacion.removeAllItems();
			List<String> pFormCurso = this.iCAlta.getPFromacion(cursoElegido);
			try {
			if (!(pFormCurso.isEmpty())) {
				comboBoxpformacion.setEnabled(true);
				btnNewButtonPFormacion.setEnabled(true);
				
		        for(String pf:pFormCurso) {
		        	this.comboBoxpformacion.addItem(pf);
		        }
			}
			} catch (Exception e4) {
				this.comboBoxpformacion.removeAllItems();
			}	
					
		}
	}
	
	private void verEdicionActionPerformed(ActionEvent e) {
		if (this.comboBoxEdiciones.getSelectedItem() != null) {
			//get values from combobox
			String institutoSelected = this.comboBoxInstitutos.getSelectedItem().toString();
			String cursoSelected = this.comboBoxCursos.getSelectedItem().toString();
			String edicionSelected = this.comboBoxEdiciones.getSelectedItem().toString();		
			//Ocultar este panel
			this.hide();
			
			//muestro consultaEdicionCurso
			ConsultaEdicionCurso consultaEdicionCursoInternalFrame = new ConsultaEdicionCurso(iCAlta);
			Principal.frame.getContentPane().add(consultaEdicionCursoInternalFrame);
			consultaEdicionCursoInternalFrame.toFront();
			consultaEdicionCursoInternalFrame.refreshInstituto();
			consultaEdicionCursoInternalFrame.setVisible(true);
			
			//seteo value en combobox instituto			
			int num = consultaEdicionCursoInternalFrame.comboBoxInstituto.getItemCount();
			boolean found = false;
			// Get items
			int i = 0;
			while ((found==false)&&(i<num)) {
				if(consultaEdicionCursoInternalFrame.comboBoxInstituto.getItemAt(i).toString().equals(institutoSelected))
					found = true;
				else
					i++;
			}
			consultaEdicionCursoInternalFrame.comboBoxInstituto.setSelectedIndex(i);	
			//seteo value en combobox curso			
			num = consultaEdicionCursoInternalFrame.comboBoxCurso.getItemCount();
			found = false;
			// Get items
			i = 0;
			while ((found==false)&&(i<num)) {
				if(consultaEdicionCursoInternalFrame.comboBoxCurso.getItemAt(i).toString().equals(cursoSelected))					
					found = true;
				else
					i++;
			}
			consultaEdicionCursoInternalFrame.comboBoxCurso.setSelectedIndex(i);
			//seteo value en combobox edicion			
			num = consultaEdicionCursoInternalFrame.comboBoxEdicion.getItemCount();
			found = false;
			// Get items
			i = 0;
			while ((found==false)&&(i<num)) {
				if(consultaEdicionCursoInternalFrame.comboBoxEdicion.getItemAt(i).toString().equals(edicionSelected))
					found = true;
				else
					i++;
			}		
			consultaEdicionCursoInternalFrame.comboBoxEdicion.setSelectedIndex(i);
			
			//necesito hacer click en el boton de ver
			//infoEdicionCursoAceptarActionPerformed
			consultaEdicionCursoInternalFrame.infoEdicionCursoAceptarActionPerformed(e);

		}
		
	}
	
	private void verPFormacionActionPerformed(ActionEvent e) {
		//this.comboBoxpformacion.getSelectedItem().toString();
		if(this.comboBoxpformacion.getSelectedItem() != null) {
			this.hide();
			InfoProgFormacion infoProgFormacionInternalFrame = new InfoProgFormacion(iCAlta,iMod, (String)this.comboBoxpformacion.getSelectedItem());
	        Principal.frame.getContentPane().add(infoProgFormacionInternalFrame);
	        infoProgFormacionInternalFrame.toFront();
	        infoProgFormacionInternalFrame.setVisible(true);
	        setVisible(false);
		}
	}
}