package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import datatypes.DtCategoria;
import excepciones.categoriaRepetidaException;
import interfaces.IControladorAlta;
import java.awt.Font;
public class AltaCategoria extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	private IControladorAlta iCAlta;
	private JTextField textField;
	
	
	public AltaCategoria(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setBounds(100, 100, 424, 234);
        getContentPane().setLayout(null);
        
        JLabel lblNewLabel = new JLabel("Nombre:");
        lblNewLabel.setBounds(62, 101, 46, 14);
        getContentPane().add(lblNewLabel);
        setTitle("Alta Categoria");
        
        JLabel lblNewLabel_1 = new JLabel("Alta Categoria");
        lblNewLabel_1.setBounds(139, 28, 197, 35);
		lblNewLabel_1.setFont(new Font("Tw Cen MT Condensed", Font.BOLD | Font.ITALIC, 24));
		getContentPane().add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(118, 98, 157, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(41, 151, 89, 23);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		getContentPane().add(btnCancelar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(204, 151, 89, 23);
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		getContentPane().add(btnAceptar);
        
	}
	private void Cancelar(ActionEvent ae) {
		limpiarFormulario();
		setVisible(false);
	}
	
	private void Aceptar(ActionEvent ae) {
		if (checkFormulario()) {
			String nom = this.textField.getText();
			try {
	            this.iCAlta.ingresarCategoria(nom);
	            JOptionPane.showMessageDialog(this, "La categoria creado con exito", "Agregar Categoria", JOptionPane.INFORMATION_MESSAGE);
	            limpiarFormulario();
	    		setVisible(false);
	            } catch (categoriaRepetidaException e) {
	                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Categoria", JOptionPane.ERROR_MESSAGE);
	            }
		}
	}
	
	private boolean checkFormulario() {
        String nombre = this.textField.getText();
        if (nombre.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Categoria",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
	private void limpiarFormulario() {
		textField.setText("");
	}
	
}

 


