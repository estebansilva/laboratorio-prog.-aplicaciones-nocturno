package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import datatypes.DtInstituto;
import datatypes.DtPF;
import excepciones.InstitutoRepetidaException;
import excepciones.ProgFormacionRepetidoException;
import interfaces.IControladorAlta;

import java.util.Date;
import java.util.Calendar;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class CrearPF extends JInternalFrame {
	private JTextField campoNombre;
	private JTextField campoDescripcion;
	private JSpinner campoFechaI;
	private JSpinner campoFechaF;
	private IControladorAlta iCAlta;

	/**
	 * Create the frame.
	 */
	public CrearPF(IControladorAlta iCAlta) {
		setTitle("Crear Programa de Formaci\u00F3n");
		setResizable(true);
		setIconifiable(true);
		setMaximizable(true);
		setClosable(true);
		this.iCAlta = iCAlta;
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(74, 67, 82, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Descripcion");
		lblNewLabel_1.setBounds(74, 94, 82, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha Inicio");
		lblNewLabel_2.setBounds(74, 119, 82, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha Fin");
		lblNewLabel_3.setBounds(74, 144, 82, 14);
		getContentPane().add(lblNewLabel_3);
		
		campoNombre = new JTextField();
		campoNombre.setBounds(224, 64, 129, 20);
		getContentPane().add(campoNombre);
		campoNombre.setColumns(10);
		
		campoDescripcion = new JTextField();
		campoDescripcion.setBounds(224, 91, 129, 20);
		getContentPane().add(campoDescripcion);
		campoDescripcion.setColumns(10);
		
		campoFechaI = new JSpinner();
	    SimpleDateFormat model = new SimpleDateFormat("dd/MM/yyyy");
		campoFechaI.setModel(new SpinnerDateModel(new Date(1598842800000L), null, null, Calendar.MILLISECOND));
		campoFechaI.setBounds(224, 116, 129, 20);
		getContentPane().add(campoFechaI);
		
		campoFechaF = new JSpinner();
		SimpleDateFormat model1 = new SimpleDateFormat("dd/MM/yyyy");
		campoFechaF.setModel(new SpinnerDateModel(new Date(1598842800000L), null, null, Calendar.MILLISECOND));
		campoFechaF.setBounds(224, 141, 129, 20);
		getContentPane().add(campoFechaF);
		SimpleDateFormat model11 = new SimpleDateFormat("dd/MM/yyyy");
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		btnAceptar.setBounds(93, 186, 89, 23);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar();
			}
		});
		btnCancelar.setBounds(221, 186, 89, 23);
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel_4 = new JLabel("Crear Programa de Formaci\u00F3n");
		lblNewLabel_4.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 22));
		lblNewLabel_4.setBounds(42, 11, 355, 56);
		getContentPane().add(lblNewLabel_4);

	}

	private void Cancelar() {
		limpiarFormulario();
		setVisible(false);
	}
	
	
	private void limpiarFormulario() {
		campoNombre.setText("");
		campoDescripcion.setText("");
	}
	
	
	private void Aceptar(ActionEvent ae)  {
		if (checkFormulario()) {
			String nombre = this.campoNombre.getText();
			String descripcion = this.campoDescripcion.getText();
			Date fechaI = (Date)this.campoFechaI.getValue();
			Date fechaF = (Date)this.campoFechaF.getValue();
			DtPF dtpf = new DtPF(nombre, descripcion, fechaI, fechaF);
			try {
	            this.iCAlta.IngresarProgramaF(dtpf);
	            JOptionPane.showMessageDialog(this, "El programa de Formación se creo correctamente", "Crear Programa de Formación", JOptionPane.INFORMATION_MESSAGE);
	            limpiarFormulario();
	    		setVisible(false);
	            } catch (ProgFormacionRepetidoException e) {
	                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Curso a Programa de Formación", JOptionPane.ERROR_MESSAGE);
	            }
		}
		
	}	

	private boolean checkFormulario() {
        String nombre = this.campoNombre.getText();
        String descripcion = this.campoDescripcion.getText();
        Date fechaI = (Date)this.campoFechaI.getValue();
        Date fechaF = (Date)this.campoFechaF.getValue();
        Date fechaActual = new Date();
        
        if (fechaActual.after(fechaI)) {
            JOptionPane.showMessageDialog(this, "La fecha de inicio no puede ser anterior a la fecha actual", "Agregar edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (fechaActual.after(fechaF)) {
            JOptionPane.showMessageDialog(this, "La fecha final no puede ser anterior a la fecha actual", "Agregar edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (nombre.isEmpty() || descripcion.isEmpty() ) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Crear Programa de Formación",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
       
        if (fechaI.after(fechaF)) {
            JOptionPane.showMessageDialog(this, "Fecha Inicio no puede ser posterior a Fecha Fin", "Crear Programa de Formación",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
}



