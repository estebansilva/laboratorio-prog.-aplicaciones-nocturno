package presentacion;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import java.awt.HeadlessException;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import datatypes.DtCurso;
import datatypes.DtInstituto;
import excepciones.CursoRepetidaException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;

public class AltaCurso extends JInternalFrame {
	private IControladorAlta iCAlta;
	private static final long serialVersionUID = 1L;
	private JTextField textField_Nombre;
	private JTextField textField_Desc;
	private JTextField textField_Dur;
	private JTextField textField_Canthora;
	private JTextField textField_Creditos;
	private JTextField textField_Date;
	private JTextField textField_Link;
	private JComboBox<String> comboBox;
	private JList<String> list;
	private JList<String> listaCat;
	private Date d;

	public AltaCurso(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Alta de un Curso");
		setBounds(100, 100, 475, 477);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nombre del Curso");
		lblNewLabel.setBounds(32, 84, 120, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Descripcion");
		lblNewLabel_1.setBounds(32, 113, 73, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Duracion");
		lblNewLabel_2.setBounds(32, 141, 73, 13);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Cant. de Horas");
		lblNewLabel_3.setBounds(276, 111, 73, 13);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Creditos");
		lblNewLabel_4.setBounds(276, 137, 73, 13);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha(dd/MM/aaaa)");
		lblNewLabel_5.setBounds(249, 84, 113, 13);
		getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Link(URL)");
		lblNewLabel_6.setBounds(32, 169, 73, 13);
		getContentPane().add(lblNewLabel_6);
		
		textField_Nombre = new JTextField();
		textField_Nombre.setBounds(121, 80, 86, 19);
		getContentPane().add(textField_Nombre);
		textField_Nombre.setColumns(10);
		
		textField_Desc = new JTextField();
		textField_Desc.setBounds(94, 108, 113, 19);
		getContentPane().add(textField_Desc);
		textField_Desc.setColumns(10);
		
		textField_Dur = new JTextField();
		textField_Dur.setBounds(94, 137, 113, 19);
		getContentPane().add(textField_Dur);
		textField_Dur.setColumns(10);
		
		textField_Canthora = new JTextField();
		textField_Canthora.setBounds(359, 107, 73, 19);
		getContentPane().add(textField_Canthora);
		textField_Canthora.setColumns(10);
		
		textField_Creditos = new JTextField();
		textField_Creditos.setBounds(359, 133, 73, 19);
		getContentPane().add(textField_Creditos);
		textField_Creditos.setColumns(10);
		
		textField_Date = new JTextField();
		textField_Date.setBounds(359, 80, 73, 19);
		getContentPane().add(textField_Date);
		textField_Date.setColumns(10);
		
		textField_Link = new JTextField();
		textField_Link.setBounds(94, 165, 307, 19);
		getContentPane().add(textField_Link);
		textField_Link.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Aceptar(e);
				} catch (NumberFormatException | HeadlessException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAceptar.setBounds(90, 415, 102, 21);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		btnCancelar.setBounds(202, 415, 103, 21);
		getContentPane().add(btnCancelar);
		
		
		JLabel lblAltaCurso = new JLabel("Alta Curso");
		lblAltaCurso.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblAltaCurso.setBounds(175, 11, 126, 31);
		getContentPane().add(lblAltaCurso);
		
		
		JLabel lblNewLabel_7 = new JLabel("Instituto");
		lblNewLabel_7.setBounds(113, 51, 54, 13);
		getContentPane().add(lblNewLabel_7);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(175, 51, 168, 21);
		getContentPane().add(comboBox);
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					refreshList((String)comboBox.getSelectedItem());
					
				}catch(Exception e2) {
					System.out.print("No tengo cursos");
				}
				
				try {
					refreshCat();
		
				}catch(Exception e3) {
					System.out.print("No tengo categorias");
				}
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(94, 214, 285, 84);
		getContentPane().add(scrollPane);
		
		
		list = new JList<String>();
		scrollPane.setViewportView(list);
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL_WRAP);
		
		JLabel lblNewLabel_8 = new JLabel("Previas");
		lblNewLabel_8.setBounds(32, 215, 54, 13);
		getContentPane().add(lblNewLabel_8);
		
		JLabel Categorias = new JLabel("Categorias");
		Categorias.setBounds(32, 317, 54, 14);
		getContentPane().add(Categorias);
		
		JScrollPane scrollCat = new JScrollPane();
		scrollCat.setBounds(94, 316, 285, 74);
		getContentPane().add(scrollCat);
		
		listaCat = new JList<String>();
		scrollCat.setViewportView(listaCat);
		listaCat.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		listaCat.setLayoutOrientation(JList.VERTICAL_WRAP);
		
		
		
	}
	
	public void refresh() {
        List<String> institutos = iCAlta.setInstitutos();
        this.comboBox.removeAllItems();
        for(String ins:institutos) {
            this.comboBox.addItem(ins);
        }
	}
	public void refreshCat() {
		this.listaCat.removeAll();
		List<String> cat = iCAlta.listaDeCategorias();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(cat != null){
			for(String c:cat) {
				((DefaultListModel<String>) modelo).addElement(c);
			}
		}else {
		((DefaultListModel<String>) modelo).addElement("No hay categorias actualmente");
	}
	listaCat.setModel(modelo);
	}
	
	@SuppressWarnings("unchecked")
	public void refreshList(String s) {
		this.list.removeAll();
		List<String> cursos = iCAlta.setCursos(s);
		ListModel<String> modelo = new DefaultListModel<String>();
		if(cursos != null) {
	        for(String c:cursos) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay cursos actualmente");
		}
		list.setModel(modelo);
	}

	protected void Cancelar(ActionEvent e) {
		// TODO Auto-generated method stub
		limpiarFormulario();
		setVisible(false);
		
	}

	private void Aceptar(ActionEvent e) throws NumberFormatException, HeadlessException, ParseException {
		// TODO Auto-generated method stub
		if(checkFormulario()) {
        	String nombre = this.textField_Nombre.getText();
            String desc = this.textField_Desc.getText();
            String dur = this.textField_Dur.getText();
            int creditos = Integer.parseInt(this.textField_Creditos.getText());
            int canthora = Integer.parseInt(this.textField_Canthora.getText());
            String datestring = this.textField_Date.getText();
            String link = this.textField_Link.getText();
            List<String> ls = this.list.getSelectedValuesList();
            List<String> lsCat = this.listaCat.getSelectedValuesList();
            for(String ca:lsCat) {
            	System.out.print(ca);
            }
            	
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
			try {
				date = format.parse(datestring);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	DtCurso dtc = new DtCurso(nombre, desc, dur, canthora, creditos, date, link);
        	DtInstituto dtinstituto = new DtInstituto((String)this.comboBox.getSelectedItem());
        	try {
        		this.iCAlta.ingresarCurso(dtc, dtinstituto, ls,lsCat);
        		JOptionPane.showMessageDialog(this, "El Curso se creo correctamente", "Agregar curso", JOptionPane.INFORMATION_MESSAGE);
	            limpiarFormulario();
	    		setVisible(false);
        	} catch (CursoRepetidaException e2) {
        		JOptionPane.showMessageDialog(this, e2.getMessage(), "Agregar Instituto", JOptionPane.ERROR_MESSAGE);
        	}
        	
        	
        }
		
	}
	
	
	private boolean checkFormulario() throws ParseException {
        String nombre = this.textField_Nombre.getText();
        String desc = this.textField_Desc.getText();
        String dur = this.textField_Dur.getText();
        String creditos = this.textField_Creditos.getText();
        String canthora = this.textField_Canthora.getText();
        String date = this.textField_Date.getText();
        String link = this.textField_Link.getText();
        Date fechaActual = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha = null;
        if (nombre.isEmpty()|| desc.isEmpty() || dur.isEmpty() || creditos.isEmpty() || canthora.isEmpty()|| date.isEmpty() || link.isEmpty() || this.comboBox.getSelectedItem() == null) {
        	JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Curso", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        try {
            fecha = format.parse(date);
        } catch (ParseException e2) {
            JOptionPane.showMessageDialog(this, "Fecha debe ser de formato dd/mm/yyyy", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (fecha.after(fechaActual)) {
            JOptionPane.showMessageDialog(this, "Fecha no puede ser mayor a la fecha actual", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        try {
            Integer.parseInt(creditos);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Los creditos debe ser un numero", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        try {
            Integer.parseInt(canthora);
        } catch (NumberFormatException e1) {
            JOptionPane.showMessageDialog(this, "La cantidad de Horas debe ser un numero", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(Integer.parseInt(creditos) <= 0) {
        	JOptionPane.showMessageDialog(this, "La cantidad de Creditos debe ser postivo", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if(Integer.parseInt(canthora) <= 0) {
        	JOptionPane.showMessageDialog(this, "La cantidad de Horas debe ser postivo", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
	
	
	
	private void limpiarFormulario() {
		 textField_Nombre.setText("");
		 textField_Desc.setText("");
		 textField_Dur.setText("");
		 textField_Creditos.setText("");
		 textField_Canthora.setText("");
		 textField_Date.setText("");
		 textField_Link.setText("");

}
}
