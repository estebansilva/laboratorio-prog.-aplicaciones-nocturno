package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListModel;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import datatypes.*;
import javax.swing.JScrollPane;

public class ConsultaUsuarioDocente extends JInternalFrame {

	private JTextField textFieldinst;
	private JTextField textField_nick;
	private JTextField textField_nom;
	private JTextField textField_ape;
	private JTextField textField_cor;
	private JTextField textField_fecha;
	private JList list;

	public ConsultaUsuarioDocente(DtDocente d, String s, List<String> ls) {
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Docente");
		setBounds(100, 100, 475, 405);
		getContentPane().setLayout(null);
		
		textFieldinst = new JTextField();
		textFieldinst.setEditable(false);
		textFieldinst.setBounds(148, 7, 189, 19);
		getContentPane().add(textFieldinst);
		textFieldinst.setColumns(10);
		
		
		
		JLabel lblNewLabel = new JLabel("Instituto");
		lblNewLabel.setBounds(28, 10, 95, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nick");
		lblNewLabel_1.setBounds(28, 43, 95, 13);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre");
		lblNewLabel_2.setBounds(28, 83, 95, 13);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Apellido");
		lblNewLabel_3.setBounds(28, 124, 95, 13);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Correo");
		lblNewLabel_4.setBounds(28, 167, 95, 13);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha");
		lblNewLabel_5.setBounds(28, 212, 95, 13);
		getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Dicta: ");
		lblNewLabel_6.setBounds(28, 250, 95, 13);
		getContentPane().add(lblNewLabel_6);
		
		textField_nick = new JTextField();
		textField_nick.setEditable(false);
		textField_nick.setColumns(10);
		textField_nick.setBounds(148, 40, 189, 19);
		getContentPane().add(textField_nick);
		
		
		textField_nom = new JTextField();
		textField_nom.setEditable(false);
		textField_nom.setColumns(10);
		textField_nom.setBounds(148, 80, 189, 19);
		getContentPane().add(textField_nom);
		
		
		textField_ape = new JTextField();
		textField_ape.setEditable(false);
		textField_ape.setColumns(10);
		textField_ape.setBounds(148, 121, 189, 19);
		getContentPane().add(textField_ape);
		
		
		textField_cor = new JTextField();
		textField_cor.setEditable(false);
		textField_cor.setColumns(10);
		textField_cor.setBounds(148, 164, 189, 19);
		getContentPane().add(textField_cor);
		
		
		textField_fecha = new JTextField();
		textField_fecha.setEditable(false);
		textField_fecha.setColumns(10);
		textField_fecha.setBounds(148, 209, 189, 19);
		getContentPane().add(textField_fecha);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(147, 250, 189, 78);
		getContentPane().add(scrollPane);
		
		
		
		list = new JList();
		scrollPane.setViewportView(list);
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnNewButton.setBounds(190, 344, 83, 21);
		getContentPane().add(btnNewButton);
		
		this.textFieldinst.setText(s);
		this.textField_nick.setText(d.getNick());
		this.textField_nom.setText(d.getNombre());
		this.textField_ape.setText(d.getApellido());
		this.textField_cor.setText(d.getCorreo());
		this.textField_fecha.setText(d.getFechaNac().toString());
		try {
			refreshList(ls);
		}catch (Exception e) {
			
		}

	}
	
	public void refreshList(List<String> s) {
		this.list.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(s != null) {
	        for(String c:s) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Ediciones actualmente");
		}
		list.setModel(modelo);
	}
	
}
