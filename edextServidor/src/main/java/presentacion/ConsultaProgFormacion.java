package presentacion;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextPane;

import datatypes.DtProgFormacion;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class ConsultaProgFormacion extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JComboBox comboBoxPF;
	private JComboBox comboBoxCurso;
	private IControladorModificarUsuario iMod;
	private IControladorAlta iCAlta;
	private JButton btnVerInfoCurso;
	/**
	 * Launch the application.
	 */
	public void refreshPF() {
        List<String> progformacion = iMod.getPf();
        this.comboBoxPF.removeAllItems();
        for(String pf:progformacion) {
            this.comboBoxPF.addItem(pf);
        }
	}
	public void refreshCurso(String nombrepf) {
        List<String> cursos = iMod.getCursoPorPF(nombrepf);
        this.comboBoxCurso.removeAllItems();
        for(String c:cursos) {
            this.comboBoxCurso.addItem(c);
        }
	}

	public ConsultaProgFormacion(IControladorModificarUsuario iMod, IControladorAlta iCAlta) {
		this.iMod = iMod;
		this.iCAlta = iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Programa de Formacion");
        setBounds(100, 100, 475, 394);
        getContentPane().setLayout(null);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		comboBoxPF = new JComboBox();
		comboBoxPF.setBounds(106, 11, 163, 25);
		getContentPane().add(comboBoxPF);
		
		
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(27, 54, 63, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Descripcion:");
		lblNewLabel_1.setBounds(27, 79, 79, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha Inicio:");
		lblNewLabel_2.setBounds(27, 104, 79, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha Fin:");
		lblNewLabel_3.setBounds(27, 129, 79, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel labelNombre = new JLabel("");
		labelNombre.setBounds(116, 54, 169, 14);
		getContentPane().add(labelNombre);
		
		JLabel labelDesc = new JLabel("");
		labelDesc.setBounds(116, 79, 169, 14);
		getContentPane().add(labelDesc);
		
		JLabel labelFI = new JLabel("");
		labelFI.setBounds(116, 104, 169, 14);
		getContentPane().add(labelFI);
		
		JLabel labelFF = new JLabel("");
		labelFF.setBounds(115, 129, 170, 14);
		getContentPane().add(labelFF);
		
		comboBoxCurso = new JComboBox();
		comboBoxCurso.setBounds(116, 173, 169, 20);
		getContentPane().add(comboBoxCurso);
		
		JLabel lblNewLabel_4 = new JLabel("Cursos:");
		lblNewLabel_4.setBounds(27, 176, 63, 14);
		getContentPane().add(lblNewLabel_4);
		
		btnVerInfoCurso = new JButton("Ver info");
		btnVerInfoCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxCurso.getSelectedItem() != null){
					InfoCurso InfoCursoInternalFrame = new InfoCurso(iCAlta, iMod,(String)comboBoxCurso.getSelectedItem());
	                Principal.frame.getContentPane().add(InfoCursoInternalFrame);
	                InfoCursoInternalFrame.toFront();
	                InfoCursoInternalFrame.setVisible(true);
	                setVisible(false);
				}
			}
		});
		btnVerInfoCurso.setBounds(295, 172, 89, 23);
		getContentPane().add(btnVerInfoCurso);
		
		JButton cargarDatos = new JButton("Cargar Datos");
		cargarDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxPF.getSelectedItem()!= null) {
					DtProgFormacion DtPF = iMod.obtenerDtPF((String)comboBoxPF.getSelectedItem());
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					labelNombre.setText(DtPF.getNombre());
					labelDesc.setText(DtPF.getDescrip());
					labelFI.setText(df.format(DtPF.getFechaI()));
					labelFF.setText(df.format(DtPF.getFechaF()));
					refreshCurso(DtPF.getNombre());
				}
			}
		});
		cargarDatos.setBounds(277, 12, 97, 23);
		getContentPane().add(cargarDatos);

	}
	
}
