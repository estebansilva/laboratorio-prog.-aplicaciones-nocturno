package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import excepciones.inscripcionEdicionRepetidaException;

import javax.swing.JComboBox;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextArea;

import datatypes.DtEdicion;
import datatypes.DtCurso;
import datatypes.DtInstituto;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.JList;

public class ConsultaEdicionCurso extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IControladorAlta iCAlta;
	protected JComboBox<String> comboBoxInstituto;
	protected JComboBox<String> comboBoxCurso;
	protected JComboBox<String> comboBoxEdicion;
	private JLabel lblNombre;
	private JLabel lblFechaI;
	private JLabel lblFechaF;
	private JLabel lblFechaP;
	private JLabel lblCupo;
	private JList list;
	private JList listDocente;
	
	public ConsultaEdicionCurso(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Edicion de Curso");
		setBounds(100, 100, 403, 524);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Instituto");
		lblNewLabel.setBounds(30, 54, 74, 20);
		getContentPane().add(lblNewLabel);
		
		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setBounds(201, 54, 74, 20);
		getContentPane().add(lblCurso);
		
		JLabel lblEdicion = new JLabel("Edicion");
		lblEdicion.setBounds(30, 85, 74, 20);
		getContentPane().add(lblEdicion);
		
		comboBoxInstituto = new JComboBox<String>();
		comboBoxInstituto.setBounds(76, 54, 115, 21);
		getContentPane().add(comboBoxInstituto);
		comboBoxInstituto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					refreshCurso(comboBoxInstituto.getSelectedItem().toString());
				}catch(Exception e2) {
					
				}
				
			}
		});
		
		comboBoxCurso = new JComboBox<String>();
		comboBoxCurso.setBounds(253, 54, 110, 21);
		getContentPane().add(comboBoxCurso);
		comboBoxCurso.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					refreshEdicion(comboBoxCurso.getSelectedItem().toString());
				}catch(Exception e2) {
					
				}
				
			}
		});
		
		comboBoxEdicion = new JComboBox<String>();
		comboBoxEdicion.setBounds(71, 85, 132, 21);
		getContentPane().add(comboBoxEdicion);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(115, 450, 144, 33);
		getContentPane().add(btnCancelar);
		btnCancelar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
		
		JLabel lblConsultaDeEdicin = new JLabel("Consulta de Edici\u00F3n de Curso");
		lblConsultaDeEdicin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblConsultaDeEdicin.setBounds(76, 11, 276, 32);
		getContentPane().add(lblConsultaDeEdicin);
		
		JButton btnNewButton_VerInfo = new JButton("Ver Informaci\u00F3n");
		btnNewButton_VerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				infoEdicionCursoAceptarActionPerformed(e);
			}
		});
		btnNewButton_VerInfo.setBounds(232, 84, 110, 23);
		getContentPane().add(btnNewButton_VerInfo);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		lblNewLabel_1.setBounds(29, 123, 58, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha Inicio:");
		lblNewLabel_2.setBounds(30, 148, 74, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha Fin:");
		lblNewLabel_3.setBounds(30, 173, 74, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Cupo");
		lblNewLabel_4.setBounds(30, 198, 46, 14);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Fecha Publicacion:");
		lblNewLabel_5.setBounds(30, 223, 115, 14);
		getContentPane().add(lblNewLabel_5);
		
		lblNombre = new JLabel("");
		lblNombre.setBounds(145, 123, 144, 14);
		getContentPane().add(lblNombre);
		
		lblFechaI = new JLabel("");
		lblFechaI.setBounds(145, 148, 130, 14);
		getContentPane().add(lblFechaI);
		
		lblFechaF = new JLabel("");
		lblFechaF.setBounds(145, 173, 130, 14);
		getContentPane().add(lblFechaF);
		
		lblCupo = new JLabel("");
		lblCupo.setBounds(145, 198, 114, 14);
		getContentPane().add(lblCupo);
		
		lblFechaP = new JLabel("");
		lblFechaP.setBounds(145, 223, 130, 14);
		getContentPane().add(lblFechaP);
		
		JLabel lblNewLabel_6 = new JLabel("Inscriptos:");
		lblNewLabel_6.setBounds(30, 248, 74, 14);
		getContentPane().add(lblNewLabel_6);
		
		list = new JList();
		list.setBounds(145, 248, 170, 65);
		getContentPane().add(list);
		
		JLabel lblNewLabel_7 = new JLabel("Docentes:");
		lblNewLabel_7.setBounds(30, 341, 74, 14);
		getContentPane().add(lblNewLabel_7);
		
		listDocente = new JList();
		listDocente.setBounds(145, 340, 170, 65);
		getContentPane().add(listDocente);
		
		
		btnCancelar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});

	}
	
	
	protected void infoEdicionCursoAceptarActionPerformed(ActionEvent arg0) {
		if (checkFormulario()){
			String instituto = (String)this.comboBoxInstituto.getSelectedItem();
			String curso = (String)this.comboBoxCurso.getSelectedItem();
			String edicion = (String)this.comboBoxEdicion.getSelectedItem();
			List<String> ediciones;
			DtEdicion dte;
			List<String> cursos;
			cursos=this.iCAlta.setCursos(instituto);
			comboBoxCurso.setSelectedItem(cursos);
			ediciones=this.iCAlta.setEdiciones(curso);
			comboBoxEdicion.setSelectedItem(ediciones);
			dte= this.iCAlta.darInfoEdicion(edicion);
			
			String fecha;
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			
			lblNombre.setText(dte.getNombre());
			fecha = formato.format(dte.getFechaI());
			lblFechaI.setText(fecha);
			fecha = formato.format(dte.getFechaF());
			lblFechaF.setText(fecha);
			fecha =formato.format(dte.getFechaPub());
			lblFechaP.setText(fecha);
			lblCupo.setText(Integer.toString(dte.getCupo()));
			try {
				refreshList(this.iCAlta.getEstudianteEnEdicion(dte.getNombre()));
			}catch (Exception e2) {
				
			}
			try {
				refreshListDoc(this.iCAlta.getDocenteEdicion(dte.getNombre()));
			}catch (Exception e2) {
				System.out.print("ConsultaEdicionCurso");
				
			}
		}
		
		
	} 
	
	private void refreshListDoc(List<String> d) {
		this.listDocente.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(d != null) {
	        for(String c:d) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Docentes actualmente");
		}
		listDocente.setModel(modelo);
		
	}


	public void refreshInstituto() {
        List<String> institutos = iCAlta.setInstitutos();
        this.comboBoxInstituto.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxInstituto.addItem(ins);
        }
	}
	public void refreshCurso(String s) {
        List<String> institutos = iCAlta.setCursos(s);
        this.comboBoxCurso.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxCurso.addItem(ins);
        }
	}
	public void refreshEdicion(String s) {
        List<String> institutos = iCAlta.setEdiciones(s);
        this.comboBoxEdicion.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxEdicion.addItem(ins);
        }
	}
	
	private boolean checkFormulario() {
        if (comboBoxInstituto.getSelectedItem() ==null || comboBoxCurso.getSelectedItem() ==null || comboBoxEdicion.getSelectedItem() ==null) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar edicion",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        return true;
    }
	public void refreshList(List<String> s) {
		this.list.removeAll();
		ListModel<String> modelo = new DefaultListModel<String>();
		if(s != null) {
	        for(String c:s) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay Ediciones actualmente");
		}
		list.setModel(modelo);
	}
	
	public void limpiar() {
		lblCupo.setText("");
		lblFechaF.setText("");
		lblFechaI.setText("");
		lblFechaP.setText("");
		lblNombre.setText("");
		this.listDocente.removeAll();
		this.list.removeAll();
	}
}
