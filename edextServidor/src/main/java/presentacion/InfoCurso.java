package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingConstants;

import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;

public class InfoCurso extends JInternalFrame {
	private JComboBox comboBoxEdiciones;
	private JComboBox comboBoxpformacion;
	private JLabel lblNewLabel_valueNombre;
	private JLabel lblNewLabel_value_cantH;
	private JLabel lblNewLabel_valueFechaR;
	private JLabel lblNewLabel_valueInstituto;
	private JLabel lblNewLabel_valueDuracion;
	private JLabel lblNewLabel_valueCreditos;
	private JLabel lblNewLabel_valueURL;
	private JLabel lblNewLabel_valueDesc;
	private JLabel lblNewLabel;
	private JButton btnNewButtonPFormacion;
	private JButton btnNewButtonEdiciones;
	private JButton btnInfoCurso;
	private JButton btnBuscarCursos;
	private JLabel lblNombre;
	private JLabel lblDuracion;
	private JLabel lblcreditos;
	private JLabel lblurl;
	private JLabel lblDescripcion;
	private JLabel lblcantHoras;
	private JLabel lblfechaR;
	private JLabel lblinstituto;
	private JLabel lbledicionCurso;
	private JLabel lblProgFormacion;
	private JList<String> list;
	private JList<String> list_1;

	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private IControladorAlta iCAlta;
	private IControladorModificarUsuario iMod;
	
	
	public InfoCurso(IControladorAlta iCAlta, IControladorModificarUsuario iMod, String c) {
		setIconifiable(true);
		setMaximizable(true);
		setTitle("Consulta de Curso");
		this.iCAlta= iCAlta;
		this.iMod= iMod;
		setResizable(true);
		setClosable(true);
		
		setBounds(100, 100, 461, 512);
		getContentPane().setLayout(null);
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(35, 42, 101, 14);
		lblNombre.setVisible(false);		
		getContentPane().add(lblNombre);
		
		lblDuracion = new JLabel("Duracion:");
		lblDuracion.setBounds(35, 90, 101, 14);
		lblDuracion.setVisible(false);
		getContentPane().add(lblDuracion);
		
		lblcreditos = new JLabel("Creditos:");
		lblcreditos.setBounds(35, 138, 101, 14);
		lblcreditos.setVisible(false);
		getContentPane().add(lblcreditos);
		
		lblurl = new JLabel("URL:");
		lblurl.setBounds(35, 186, 101, 14);
		lblurl.setVisible(false);
		getContentPane().add(lblurl);
		
		lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(35, 66, 101, 14);
		lblDescripcion.setVisible(false);
		getContentPane().add(lblDescripcion);
		
		lblcantHoras = new JLabel("Cant Horas:");
		lblcantHoras.setBounds(35, 114, 101, 14);
		lblcantHoras.setVisible(false);
		getContentPane().add(lblcantHoras);
		
		lblfechaR = new JLabel("Fecha Reg:");
		lblfechaR.setBounds(35, 162, 101, 14);
		lblfechaR.setVisible(false);
		getContentPane().add(lblfechaR);
		
		lblinstituto = new JLabel("Instituto:");
		lblinstituto.setBounds(35, 210, 101, 14);
		lblinstituto.setVisible(false);
		getContentPane().add(lblinstituto);
		
		lbledicionCurso = new JLabel("Ediciones:");
		lbledicionCurso.setBounds(28, 416, 103, 14);
		lbledicionCurso.setVisible(false);
		getContentPane().add(lbledicionCurso);
		
		lblProgFormacion = new JLabel("Prog Formacion:");
		lblProgFormacion.setBounds(28, 440, 103, 14);
		lblProgFormacion.setVisible(false);
		getContentPane().add(lblProgFormacion);
		
		comboBoxEdiciones = new JComboBox();
		comboBoxEdiciones.setEnabled(false);
		comboBoxEdiciones.setVisible(false);
		comboBoxEdiciones.setBounds(139, 413, 198, 20);
		getContentPane().add(comboBoxEdiciones);	
		
		comboBoxpformacion = new JComboBox();
		comboBoxpformacion.setEnabled(false);
		comboBoxpformacion.setVisible(false);
		comboBoxpformacion.setBounds(139, 437, 198, 20);
		getContentPane().add(comboBoxpformacion);
		
		btnNewButtonEdiciones = new JButton("Ver");
		btnNewButtonEdiciones.setEnabled(false);
		btnNewButtonEdiciones.setVisible(false);
		btnNewButtonEdiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verEdicionActionPerformed(e);
			}
		});
		btnNewButtonEdiciones.setBounds(345, 413, 79, 20);
		getContentPane().add(btnNewButtonEdiciones);
		
		btnNewButtonPFormacion = new JButton("Ver");
		btnNewButtonPFormacion.setEnabled(false);
		btnNewButtonPFormacion.setVisible(false);
		btnNewButtonPFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verPFormacionActionPerformed(e);
			}
		});
		btnNewButtonPFormacion.setBounds(345, 437, 79, 20);
		getContentPane().add(btnNewButtonPFormacion);
		
		lblNewLabel_valueNombre = new JLabel("");
		lblNewLabel_valueNombre.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueNombre.setBounds(136, 42, 263, 14);
		getContentPane().add(lblNewLabel_valueNombre);
		
		lblNewLabel_value_cantH = new JLabel("");
		lblNewLabel_value_cantH.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_value_cantH.setBounds(136, 114, 263, 14);
		getContentPane().add(lblNewLabel_value_cantH);
		
		lblNewLabel_valueFechaR = new JLabel("");
		lblNewLabel_valueFechaR.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueFechaR.setBounds(136, 162, 263, 14);
		getContentPane().add(lblNewLabel_valueFechaR);
		
		lblNewLabel_valueInstituto = new JLabel("");
		lblNewLabel_valueInstituto.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueInstituto.setBounds(136, 210, 263, 14);
		getContentPane().add(lblNewLabel_valueInstituto);
		
		lblNewLabel_valueDuracion = new JLabel("");
		lblNewLabel_valueDuracion.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueDuracion.setBounds(136, 90, 263, 14);
		getContentPane().add(lblNewLabel_valueDuracion);
		
		lblNewLabel_valueCreditos = new JLabel("");
		lblNewLabel_valueCreditos.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueCreditos.setBounds(136, 138, 263, 14);
		getContentPane().add(lblNewLabel_valueCreditos);
		
		lblNewLabel_valueDesc = new JLabel("");
		lblNewLabel_valueDesc.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_valueDesc.setBounds(146, 66, 263, 14);
		getContentPane().add(lblNewLabel_valueDesc);
		
		lblNewLabel = new JLabel("Previas");
		lblNewLabel.setBounds(35, 234, 89, 13);
		lblNewLabel.setVisible(false);
		getContentPane().add(lblNewLabel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(137, 240, 287, 84);
		scrollPane.setVisible(false);
		getContentPane().add(scrollPane);
		
		list = new JList();
		scrollPane.setRowHeaderView(list);
		list.setVisibleRowCount(20);
		list.setValueIsAdjusting(true);
		
		lblNewLabel_valueURL = new JLabel("");
		lblNewLabel_valueURL.setBounds(158, 186, 247, 14);
		getContentPane().add(lblNewLabel_valueURL);
		lblNewLabel_valueURL.setVerticalAlignment(SwingConstants.TOP);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(139, 340, 260, 62);
		scrollPane_1.setVisible(false);
		getContentPane().add(scrollPane_1);
		
		list_1 = new JList();
		scrollPane_1.setRowHeaderView(list_1);
		list_1.setVisibleRowCount(20);
		list_1.setValueIsAdjusting(true);
		
		
		
		JLabel lblNewLabel_1 = new JLabel("Categorias");
		lblNewLabel_1.setBounds(35, 341, 72, 14);
		getContentPane().add(lblNewLabel_1);
		

		CargaDatos(c);
	}
	
	private void CargaDatos(String c) {
		lblNombre.setVisible(true);
		lblDuracion.setVisible(true);
		lblcreditos.setVisible(true);
		lblurl.setVisible(true);
		lblDescripcion.setVisible(true);
		lblcantHoras.setVisible(true);
		lblfechaR.setVisible(true);
		lblinstituto.setVisible(true);
		lbledicionCurso.setVisible(true);
		lblProgFormacion.setVisible(true);
		lblNewLabel.setVisible(true);
		comboBoxEdiciones.setVisible(true);
		comboBoxpformacion.setVisible(true);
		btnNewButtonEdiciones.setVisible(true);
		btnNewButtonPFormacion.setVisible(true);
		lblNewLabel_valueNombre.setVisible(true);
		lblNewLabel_value_cantH.setVisible(true);
		lblNewLabel_valueFechaR.setVisible(true);
		lblNewLabel_valueInstituto.setVisible(true);
		lblNewLabel_valueDuracion.setVisible(true);
		lblNewLabel_valueCreditos.setVisible(true);
		lblNewLabel_valueURL.setVisible(true);
		lblNewLabel_valueDesc.setVisible(true);
		scrollPane.setVisible(true);
		scrollPane_1.setVisible(true);
		//obtengo los datos del curso
		String[] atributosCurso = this.iCAlta.getInfoCurso(c);//ESTA FALLANDO*********************************
								
		//Muestro los datos del curso
		lblNewLabel_valueNombre.setText(c);
		lblNewLabel_value_cantH.setText(atributosCurso[2]);
		lblNewLabel_valueFechaR.setText(atributosCurso[4]);
		lblNewLabel_valueInstituto.setText(atributosCurso[6]);
		lblNewLabel_valueDuracion.setText(atributosCurso[1]);
		lblNewLabel_valueCreditos.setText(atributosCurso[3]);
		lblNewLabel_valueURL.setText(atributosCurso[5]);
		lblNewLabel_valueDesc.setText(atributosCurso[0]);
		
		try {
			refreshList(c);
			refreshCat(c);
		}catch (Exception e2) {
			
		}
			
		
		
		//obtengo las ediciones
		this.comboBoxEdiciones.removeAllItems();
		try {	
		List<String> edicionesCurso = this.iCAlta.setEdiciones(c);			
		if (!(edicionesCurso.isEmpty())) {
			comboBoxEdiciones.setEnabled(true);
			btnNewButtonEdiciones.setEnabled(true);
			
	        for(String ed:edicionesCurso) {
	        	this.comboBoxEdiciones.addItem(ed);
	        }
		}	
		} catch (Exception e3) {
			this.comboBoxEdiciones.removeAllItems();
		}
		
		//obtengo planes de formacion
		this.comboBoxpformacion.removeAllItems();
		List<String> pFormCurso = this.iCAlta.getPFromacion(c);
		try {
		if (!(pFormCurso.isEmpty())) {
			comboBoxpformacion.setEnabled(true);
			btnNewButtonPFormacion.setEnabled(true);
			
	        for(String pf:pFormCurso) {
	        	this.comboBoxpformacion.addItem(pf);
	        }
		}
		} catch (Exception e4) {
			this.comboBoxpformacion.removeAllItems();
		}
		
	}

	public void refreshCat(String a) {
		this.list_1.removeAll();
		List<String> cat = iCAlta.obtenerCategoriadeCurso(a);
		ListModel<String> modelo2 = new DefaultListModel<String>();
		if(cat != null){
			for(String c:cat) {
				((DefaultListModel<String>) modelo2).addElement(c);
			}
		}else {
		((DefaultListModel<String>) modelo2).addElement("No hay categorias actualmente");
	}
		list_1.setModel(modelo2);
	}
	public void refreshList(String s) {
		this.list.removeAll();
		List<String> cursos = iCAlta.getPreviaCurso(s);
		ListModel<String> modelo = new DefaultListModel<String>();
		if(cursos != null) {
	        for(String c:cursos) {
	        	((DefaultListModel<String>) modelo).addElement(c);
	        	
	        }
		}else {
			((DefaultListModel<String>) modelo).addElement("No hay cursos actualmente");
		}
		list.setModel(modelo);
	}
	
	protected void verPFormacionActionPerformed(ActionEvent e) {
		if(this.comboBoxpformacion.getSelectedItem() != null) {
			this.hide();
			InfoProgFormacion infoProgFormacionInternalFrame = new InfoProgFormacion(iCAlta,iMod, (String)this.comboBoxpformacion.getSelectedItem());
	        Principal.frame.getContentPane().add(infoProgFormacionInternalFrame);
	        infoProgFormacionInternalFrame.toFront();
	        infoProgFormacionInternalFrame.setVisible(true);
	        setVisible(false);
		}
		
	}
	protected void verEdicionActionPerformed(ActionEvent e) {
		if(this.comboBoxEdiciones.getSelectedItem() != null) {
			this.hide();
			InfoEdicion infoEdicionInternalFrame = new InfoEdicion(iCAlta,iMod, (String)this.comboBoxEdiciones.getSelectedItem());
	        Principal.frame.getContentPane().add(infoEdicionInternalFrame);
	        infoEdicionInternalFrame.toFront();
	        infoEdicionInternalFrame.setVisible(true);
	        setVisible(false);
		}
		
	}
}
