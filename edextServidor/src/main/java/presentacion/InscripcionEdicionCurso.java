package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import excepciones.inscripcionEdicionRepetidaException;

import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;

public class InscripcionEdicionCurso extends JInternalFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IControladorAlta iCAlta;
	private JComboBox<String> comboBoxInstituto;
	private JComboBox<String> comboBoxCurso;
	private JComboBox<String> comboBoxEdicion;
	private JComboBox<String> comboBoxEstudiante;
	
	public InscripcionEdicionCurso(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Inscripcion a Edicion de Curso");
		setBounds(100, 100, 404, 354);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Instituto");
		lblNewLabel.setBounds(45, 87, 74, 20);
		getContentPane().add(lblNewLabel);
		
		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setBounds(45, 127, 74, 20);
		getContentPane().add(lblCurso);
		
		JLabel lblEdicion = new JLabel("Edicion");
		lblEdicion.setBounds(45, 168, 74, 20);
		getContentPane().add(lblEdicion);
		
		JLabel lblEstudiante = new JLabel("Estudiante");
		lblEstudiante.setBounds(45, 210, 74, 20);
		getContentPane().add(lblEstudiante);
		
		comboBoxInstituto = new JComboBox<String>();
		comboBoxInstituto.setBounds(185, 87, 172, 21);
		getContentPane().add(comboBoxInstituto);
		comboBoxInstituto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					refreshCurso((String)comboBoxInstituto.getSelectedItem());
				}catch(Exception e2) {
					
				}
				
			}
		});
		
		comboBoxCurso = new JComboBox<String>();
		comboBoxCurso.setBounds(185, 127, 172, 21);
		getContentPane().add(comboBoxCurso);
		comboBoxCurso.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					refreshEdicion((String)comboBoxCurso.getSelectedItem());
				}catch(Exception e2) {
					
				}
				
			}
		});
		
		comboBoxEdicion = new JComboBox<String>();
		comboBoxEdicion.setBounds(185, 168, 172, 21);
		getContentPane().add(comboBoxEdicion);
		comboBoxEdicion.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					refreshEstudiante();
				}catch(Exception e2) {
					
				}
				
			}
		});
		
		comboBoxEstudiante = new JComboBox<String>();
		comboBoxEstudiante.setBounds(185, 210, 172, 21);
		getContentPane().add(comboBoxEstudiante);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(89, 265, 104, 33);
		getContentPane().add(btnAceptar);
		btnAceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Aceptar();
			}

			
		});
		
		JButton btnCanselar = new JButton("Cancelar");
		btnCanselar.setBounds(216, 265, 104, 33);
		getContentPane().add(btnCanselar);
		
		JLabel lblNewLabel_1 = new JLabel("Inscripci\u00F3n a Edicion de Curso");
		lblNewLabel_1.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 22));
		lblNewLabel_1.setBounds(45, 33, 358, 38);
		getContentPane().add(lblNewLabel_1);
		btnCanselar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});

	}
	private void Aceptar() {
		// TODO Auto-generated method stub
		if(checkFormulario()) {
			String ins = (String)this.comboBoxInstituto.getSelectedItem();
			String cur = (String)this.comboBoxCurso.getSelectedItem();
			String edi = (String)this.comboBoxEdicion.getSelectedItem();
			String est = (String)this.comboBoxEstudiante.getSelectedItem();
			try {
				iCAlta.inscripcionEdicionCurso(ins, cur,edi, est);
				JOptionPane.showMessageDialog(this, "La Inscripcion se hizo correctamente", "Inscripcion a Edicion", JOptionPane.INFORMATION_MESSAGE);
	    		setVisible(false);
			}catch(inscripcionEdicionRepetidaException e2) {
				JOptionPane.showMessageDialog(this, e2.getMessage(), "Agregar Instituto", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	public void refreshInstituto() {
        List<String> institutos = iCAlta.setInstitutos();
        this.comboBoxInstituto.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxInstituto.addItem(ins);
        }
	}
	public void refreshCurso(String s) {
        List<String> institutos = iCAlta.setCursos(s);
        this.comboBoxCurso.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxCurso.addItem(ins);
        }
	}
	public void refreshEdicion(String s) {
        List<String> edicion = iCAlta.setEdiciones(s);
        this.comboBoxEdicion.removeAllItems();
        for(String e:edicion) {
            this.comboBoxEdicion.addItem(e);
        }
	}
	public void refreshEstudiante() {
        List<String> institutos = iCAlta.setEstudiantes();
        this.comboBoxEstudiante.removeAllItems();
        for(String ins:institutos) {
            this.comboBoxEstudiante.addItem(ins);
        }
	}
	
		private boolean checkFormulario(){
        if (comboBoxInstituto.getSelectedItem() ==null || comboBoxCurso.getSelectedItem() ==null || comboBoxEdicion.getSelectedItem() ==null || comboBoxEstudiante.getSelectedItem() ==null) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Curso",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
}
