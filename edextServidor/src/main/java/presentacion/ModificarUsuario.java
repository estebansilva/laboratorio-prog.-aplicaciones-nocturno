package presentacion;

import java.awt.EventQueue;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;

import interfaces.IControladorModificarUsuario;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import datatypes.DtUsuario;
import javax.swing.SwingConstants;
import java.awt.Font;

public class ModificarUsuario extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JComboBox<String> comboBox;
	private IControladorModificarUsuario iCModUser;
	private JTextField campoNombre;
	private JTextField campoApellido;
	private JTextField campoCorreo;
	private JSpinner campoFechaN;
	private JTextField campoPass;
	
	
	public void refresh() {
        List<String> listaDocente = iCModUser.setDocente();
        this.comboBox.removeAllItems();
        for(String usuarios:listaDocente) {
            this.comboBox.addItem(usuarios);
        }
        List<String>listaEstudiante = iCModUser.setEstudiantes();
        for(String usuarios:listaEstudiante) {
            this.comboBox.addItem(usuarios);
        }
	}

	/**
	 * Create the frame.
	 */
	public ModificarUsuario(IControladorModificarUsuario iCModUser) {
		setTitle("Modificar Usuario");
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		this.iCModUser = iCModUser;
		setBounds(100, 100, 452, 348);
		getContentPane().setLayout(null);
		
		comboBox = new JComboBox();
		comboBox.setBounds(154, 80, 141, 20);
		getContentPane().add(comboBox);
		
		
		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(47, 83, 61, 14);
		getContentPane().add(lblNewLabel);
		
		JButton Aceptar = new JButton("Aceptar");
		Aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		Aceptar.setBounds(76, 268, 89, 23);
		getContentPane().add(Aceptar);
		
		JButton Cancelar = new JButton("Cancelar");
		Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		Cancelar.setBounds(273, 268, 89, 23);
		getContentPane().add(Cancelar);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(47, 108, 46, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Apellido");
		lblNewLabel_2.setBounds(47, 133, 46, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Correo");
		lblNewLabel_3.setBounds(47, 158, 46, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Fecha de nacimiento");
		lblNewLabel_4.setBounds(47, 183, 103, 14);
		getContentPane().add(lblNewLabel_4);
		
		campoNombre = new JTextField();
		campoNombre.setBounds(154, 105, 141, 20);
		getContentPane().add(campoNombre);
		campoNombre.setColumns(10);
		
		campoApellido = new JTextField();
		campoApellido.setBounds(154, 130, 141, 20);
		getContentPane().add(campoApellido);
		campoApellido.setColumns(10);
		
		campoCorreo = new JTextField();
		campoCorreo.setEditable(false);
		campoCorreo.setBounds(154, 155, 141, 20);
		getContentPane().add(campoCorreo);
		campoCorreo.setColumns(10);
		
		campoFechaN = new JSpinner();
	    SimpleDateFormat model = new SimpleDateFormat("dd/MM/yyyy");
	    campoFechaN.setModel(new SpinnerDateModel(new Date(-2208973392000L), null, null, Calendar.MILLISECOND));
	    campoFechaN.setEditor(new JSpinner.DateEditor(campoFechaN, model.toPattern()));
	    campoFechaN.setBounds(154, 180, 141, 20);
	    getContentPane().add(campoFechaN);
	    
	    campoPass = new JTextField();
	    campoPass.setBounds(154, 205, 141, 20);
	    getContentPane().add(campoPass);
	    campoPass.setColumns(10);
	    
	    JButton CargarDatos = new JButton("Cargar Datos");
	    CargarDatos.setHorizontalAlignment(SwingConstants.RIGHT);
	    CargarDatos.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		if(comboBox.getSelectedItem() !=null) {
	    			DtUsuario dtuser = iCModUser.obtenerDt((String)comboBox.getSelectedItem());
	    			campoNombre.setText(dtuser.getNombre());
	    			campoApellido.setText(dtuser.getApellido());
	    			campoCorreo.setText(dtuser.getCorreo());
	    			campoFechaN.setValue(dtuser.getFechaNac());
	    			campoPass.setText(dtuser.getPass());
	    		}
	    	}
	    });
	    CargarDatos.setBounds(307, 79, 103, 23);
	    getContentPane().add(CargarDatos);
	    
	    JLabel lblNewLabel_5 = new JLabel("Modificar Usuario");
	    lblNewLabel_5.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 22));
	    lblNewLabel_5.setBounds(98, 23, 230, 46);
	    getContentPane().add(lblNewLabel_5);
	    
	    JLabel lblNewLabel_6 = new JLabel("Contrase\u00F1a:");
	    lblNewLabel_6.setBounds(47, 208, 71, 14);
	    getContentPane().add(lblNewLabel_6);
	    
	   
	   
	    

	}
	private void Aceptar(ActionEvent ae) {
		if(checkFormulario()) {
			String nick = (String)this.comboBox.getSelectedItem();
	        String nombre = this.campoNombre.getText();
	        String apellido = this.campoApellido.getText();
	        String correo = this.campoCorreo.getText();
	        Date fechaN = (Date)this.campoFechaN.getValue();
	        String pass = this.campoPass.getText();
	        DtUsuario dtusuario = new DtUsuario(nick,nombre, apellido, correo, fechaN,pass);
		    this.iCModUser.modificarUsuario(dtusuario);
		    JOptionPane.showMessageDialog(this, "El usuario se modifico correctamente", "Modificar Usuario", JOptionPane.INFORMATION_MESSAGE);
	        limpiarFormulario();
			setVisible(false);
		}
	}
	
	private void Cancelar(ActionEvent ae) {
		limpiarFormulario();
		setVisible(false);
	}
	private boolean checkFormulario() {
        String nombre = this.campoNombre.getText();
        String apellido = this.campoApellido.getText();
        String correo = this.campoCorreo.getText();
        String pass = this.campoPass.getText();
        if (this.comboBox.getSelectedItem()==null || nombre.isEmpty() || apellido.isEmpty() || correo.isEmpty() || pass.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Modificar Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
	}
	private void limpiarFormulario() {
		 campoNombre.setText("");
		 campoCorreo.setText("");
		 campoApellido.setText("");
		 campoPass.setText("");
	}
}
