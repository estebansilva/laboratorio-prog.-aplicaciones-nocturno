package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import interfaces.IControladorAlta;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class ConsultaUsuario extends JInternalFrame {	
	private JComboBox comboBox;
	private IControladorAlta iCAlta;
	
	
	public ConsultaUsuario(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Consulta Usuario");
		setBounds(100, 100, 365, 243);
		getContentPane().setLayout(null);
		
		comboBox = new JComboBox();
		comboBox.setBounds(33, 104, 136, 21);
		getContentPane().add(comboBox);
		
		JButton btnNewButton = new JButton("Seleccionar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Seleccionar();
			}

		});
		btnNewButton.setBounds(193, 104, 117, 21);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Salir");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnNewButton_1.setBounds(142, 174, 83, 21);
		getContentPane().add(btnNewButton_1);
		
		JLabel lblConsultaUsuario = new JLabel("Consulta Usuario");
		lblConsultaUsuario.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 20));
		lblConsultaUsuario.setBounds(83, 29, 255, 64);
		getContentPane().add(lblConsultaUsuario);

	}
	private boolean checkFormulario() {
        if (this.comboBox.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "Ingresa Algun Usuario", "Consulta Usuario",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
	public void refresh() {
        List<String> usuario = iCAlta.getUsuarios();
        this.comboBox.removeAllItems();
        for(String u:usuario) {
            this.comboBox.addItem(u);
        }
	}
	
	public String comboboxvalue() {
		return (String)this.comboBox.getSelectedItem();
	}
	
	private void Seleccionar() {
		// TODO Auto-generated method stub
		if(checkFormulario()) {
			if(iCAlta.esDocnete((String)this.comboBox.getSelectedItem())){
				ConsultaUsuarioDocente consultaUsuarioDocenteInternalFrame = new ConsultaUsuarioDocente(iCAlta.getInfoDocente((String)this.comboBox.getSelectedItem()), iCAlta.getDocneteInstituto((String)this.comboBox.getSelectedItem()), iCAlta.getEdicionDcocente((String)this.comboBox.getSelectedItem()));
				Principal.frame.getContentPane().add(consultaUsuarioDocenteInternalFrame);
				consultaUsuarioDocenteInternalFrame.toFront();
				consultaUsuarioDocenteInternalFrame.setVisible(true);
				setVisible(false);
				
			}else {
				ConsultaUsuarioEstudiante consultaUsuarioEstudianteInternalFrame = new ConsultaUsuarioEstudiante(iCAlta.getInfoEstudiante((String)this.comboBox.getSelectedItem()));
				Principal.frame.getContentPane().add(consultaUsuarioEstudianteInternalFrame);
				try {
					consultaUsuarioEstudianteInternalFrame.refreshList(iCAlta.getEdicionEstudiante((String)this.comboBox.getSelectedItem()));
				}catch(Exception e) {
					
				}
				//consultaUsuarioEstudianteInternalFrame.toFront();
				consultaUsuarioEstudianteInternalFrame.setVisible(true);
				setVisible(false);
				
			}
		}
	}
}
