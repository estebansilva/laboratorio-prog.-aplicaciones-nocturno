package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import interfaces.IControladorAlta;
import excepciones.InstitutoRepetidaException;
import datatypes.DtInstituto;
import java.awt.Font;

public class AltaInstituto extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JTextField txtngresarNombre;
	private IControladorAlta iCAlta;

	public AltaInstituto(IControladorAlta iCAlta) {
		this.iCAlta= iCAlta;
		setResizable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setClosable(true);
        setTitle("Alta Instituto");
		
		
		setBounds(100, 100, 424, 234);
		getContentPane().setLayout(null);
		
		txtngresarNombre = new JTextField();
		txtngresarNombre.setBounds(139, 88, 187, 19);
		getContentPane().add(txtngresarNombre);
		txtngresarNombre.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Aceptar(e);
			}
		});
		btnAceptar.setBounds(88, 163, 97, 21);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar(e);
			}
		});
		btnCancelar.setBounds(195, 163, 97, 21);
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(60, 90, 69, 16);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Alta Instituto");
		lblNewLabel_1.setFont(new Font("Tw Cen MT Condensed", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel_1.setBounds(139, 28, 197, 35);
		getContentPane().add(lblNewLabel_1);

	}
	private void Cancelar(ActionEvent ae) {
		limpiarFormulario();
		setVisible(false);
	}
	
	private void Aceptar(ActionEvent ae) {
		if (checkFormulario()) {
			String nom = this.txtngresarNombre.getText();
			DtInstituto dt= new DtInstituto(nom);
			try {
	            this.iCAlta.ingresarInstituto(dt);
	            JOptionPane.showMessageDialog(this, "Instituto creado con exito", "Agregar Instituto", JOptionPane.INFORMATION_MESSAGE);
	            limpiarFormulario();
	    		setVisible(false);
	            } catch (InstitutoRepetidaException e) {
	                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Instituto", JOptionPane.ERROR_MESSAGE);
	            }
		}
	}
	
	private boolean checkFormulario() {
        String nombre = this.txtngresarNombre.getText();
        if (nombre.isEmpty()) {
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Instituto",
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
	private void limpiarFormulario() {
		 txtngresarNombre.setText("");

 }
}
