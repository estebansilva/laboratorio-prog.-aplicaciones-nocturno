package presentacion;

import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.util.Date;
import java.util.List;

import javax.swing.JInternalFrame;

import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;
import manejadores.ManejadorCurso;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorProgFormacion;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JPopupMenu;

import datatypes.DtPF;
import excepciones.CursoEnProgFormacionRepetidoException;
import excepciones.ProgFormacionRepetidoException;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class AgregarCursoProgForm extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private IControladorModificarUsuario iMod;
	private JComboBox comboBoxPF;
	private JComboBox comboBoxCurso;

	public void refresh() {
		//NO USAR MANEJADORES EN LAS INTERFACES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
		ManejadorCurso mC = ManejadorCurso.getInstancia();
	    List<String> listaProgForm = mPF.obtenerProgFormacion();
	    List<String> listaCurso = mC.obtenerCurso();
	    //NO USAR MANEJADORES EN LAS INTERFACES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	    this.comboBoxPF.removeAllItems();
	    this.comboBoxCurso.removeAllItems();
	    
	    for(String progform:listaProgForm) {
	        this.comboBoxPF.addItem(progform);
	    }
	    
	    for(String curso:listaCurso) {
	        this.comboBoxCurso.addItem(curso);
	    }
	}
	
	
	public AgregarCursoProgForm(IControladorModificarUsuario iMod) {
		setClosable(true);
		setTitle("Agregar Curso a Programa de Formaci\u00F3n");
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		this.iMod= iMod;
		setBounds(100, 100, 421, 300);
		getContentPane().setLayout(null);
		
		comboBoxPF = new JComboBox();
		comboBoxPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					comboBoxPF.getSelectedItem().toString();
				}catch(Exception e2) {
					System.out.print("No hay Programas de Formacion");
				}
			}
		});
		comboBoxPF.setBounds(221, 87, 106, 20);
		getContentPane().add(comboBoxPF);
		
		JLabel lblNewLabel = new JLabel("Programa de Formaci\u00F3n");
		lblNewLabel.setBounds(79, 90, 148, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Curso");
		lblNewLabel_1.setBounds(79, 130, 73, 14);
		getContentPane().add(lblNewLabel_1);
		
		comboBoxCurso = new JComboBox();
		comboBoxCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
						comboBoxCurso.getSelectedItem().toString();
					}catch(Exception e2) {
						System.out.print("No hay Cursos");
					}
				}
		});
		comboBoxCurso.setBounds(221, 127, 106, 20);
		getContentPane().add(comboBoxCurso);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Aceptar(e);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAgregar.setBounds(93, 188, 89, 23);
		getContentPane().add(btnAgregar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cancelar();
			}
		});
		btnCancelar.setBounds(233, 188, 89, 23);
		getContentPane().add(btnCancelar);
		
		JLabel lblAgregarCursoProg = new JLabel("Agregar Curso a Prog. de Formaci\u00F3n");
		lblAgregarCursoProg.setFont(new Font("Trebuchet MS", Font.BOLD | Font.ITALIC, 20));
		lblAgregarCursoProg.setBounds(24, 30, 383, 43);
		getContentPane().add(lblAgregarCursoProg);

	}
	
	
	private void Cancelar() {
		setVisible(false);
	}
	
	private void Aceptar(ActionEvent ae) throws ParseException  {
			if ( checkFormulario() ) {
			try {
				String nombrePF = (String)this.comboBoxPF.getSelectedItem();
				String nombreCurso = (String)this.comboBoxCurso.getSelectedItem();
	            this.iMod.setCursoProgFormacion(nombrePF, nombreCurso);
	           JOptionPane.showMessageDialog(this, "El Curso fue agregado correctamente al Programa de Formación", "Agregar Curso a Programa de Formación", JOptionPane.INFORMATION_MESSAGE);
	    		setVisible(false);
	            } catch (CursoEnProgFormacionRepetidoException e) {
	                JOptionPane.showMessageDialog(this, e.getMessage(), "Agregar Curso a Programa de Formación", JOptionPane.ERROR_MESSAGE);
	
	            }
		}
	}
		
	
	
	private boolean checkFormulario() throws ParseException {
       if ( (this.comboBoxPF.getSelectedItem() == null) || (this.comboBoxCurso.getSelectedItem() == null ) ){
            JOptionPane.showMessageDialog(this, "No puede haber campos vacios", "Agregar Curso", JOptionPane.ERROR_MESSAGE);
            return false;
        }
       else
    	   return true;
	}
	
	
	
	}	
