package presentacion;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import datatypes.DtProgFormacion;
import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class InfoProgFormacion extends JInternalFrame {
	private JComboBox comboBoxCurso;
	private IControladorAlta iCAlta;
	private IControladorModificarUsuario iMod;
	
	public void refreshCurso(String nombrepf) {
        List<String> cursos = iMod.getCursoPorPF(nombrepf);
        this.comboBoxCurso.removeAllItems();
        for(String c:cursos) {
            this.comboBoxCurso.addItem(c);
        }
	}
	
	public InfoProgFormacion(IControladorAlta iCAlta, IControladorModificarUsuario iMod, String p) {
		this.iCAlta = iCAlta;
		this.iMod = iMod;
		setIconifiable(true);
		setMaximizable(true);
		setTitle("Consulta de Programa  de Formacion");
		this.iCAlta= iCAlta;
		this.iMod= iMod;
		setResizable(true);
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		comboBoxCurso = new JComboBox();
		comboBoxCurso.setBounds(114, 160, 169, 20);
		getContentPane().add(comboBoxCurso);
		
		JButton btnVerInfoCurso = new JButton("Ver info");
		btnVerInfoCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxCurso.getSelectedItem() != null){
					InfoCurso infoCursoInternalFrame = new InfoCurso(iCAlta, iMod,(String)comboBoxCurso.getSelectedItem());
	                Principal.frame.getContentPane().add(infoCursoInternalFrame);
	                infoCursoInternalFrame.toFront();
	                infoCursoInternalFrame.setVisible(true);
	                setVisible(false);
				}
			}
		});
		btnVerInfoCurso.setBounds(293, 159, 89, 23);
		getContentPane().add(btnVerInfoCurso);
		
		
		
		JLabel lblNewLabel_4 = new JLabel("Cursos:");
		lblNewLabel_4.setBounds(25, 163, 63, 14);
		getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha Inicio:");
		lblNewLabel_2.setBounds(25, 91, 79, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha Fin:");
		lblNewLabel_3.setBounds(25, 116, 79, 14);
		getContentPane().add(lblNewLabel_3);
		
		JLabel labelFF = new JLabel("");
		labelFF.setBounds(113, 116, 170, 14);
		getContentPane().add(labelFF);
		
		JLabel labelFI = new JLabel("");
		labelFI.setBounds(114, 91, 169, 14);
		getContentPane().add(labelFI);
		
		JLabel lblNewLabel_1 = new JLabel("Descripcion:");
		lblNewLabel_1.setBounds(25, 66, 79, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(25, 41, 63, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel labelNombre = new JLabel("");
		labelNombre.setBounds(114, 41, 169, 14);
		getContentPane().add(labelNombre);
		
		JLabel labelDesc = new JLabel("");
		labelDesc.setBounds(114, 66, 169, 14);
		getContentPane().add(labelDesc);
		
		DtProgFormacion DtPF = iMod.obtenerDtPF(p);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		labelNombre.setText(DtPF.getNombre());
		labelDesc.setText(DtPF.getDescrip());
		labelFI.setText(df.format(DtPF.getFechaI()));
		labelFF.setText(df.format(DtPF.getFechaF()));
		refreshCurso(DtPF.getNombre());
	}


	
	
}
