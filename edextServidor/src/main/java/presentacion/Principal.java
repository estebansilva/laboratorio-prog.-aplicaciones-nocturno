

package presentacion;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.persistence.EntityManager;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

import presentacion.*;
import publicadores.controladorAltaPublish;
import publicadores.controladorIniciarSesionPublish;
import publicadores.controladorModificarUsuarioPublish;

import utils.Conexion;

import javax.swing.JMenuItem;

import interfaces.Fabrica;
import interfaces.IControladorAlta;
import interfaces.IControladorModificarUsuario;

import javax.swing.JInternalFrame;

public class Principal {

	public static JFrame frame;
	private AltaInstituto altaInstitutoInternalFrame;
	private AltaUsuario altaUsuarioInternalFrame;
	private AltaCurso altaCursoInternalFrame;
	private AltaCategoria altaCategoriaInternalFrame;
	private AltaEdicionCurso altaEdicionCursoInternalFrame;
	private ModificarUsuario modificarUsuarioInternalFrame;
	private CrearPF crearPfInternalFrame;
	private InscripcionEdicionCurso inscripcionEdicionCursoInternalFrame;
	private AgregarCursoProgForm agregarCursoProgFormInternalFrame;
	protected ConsultaEdicionCurso consultaEdicionCursoInternalFrame;
	private ConsultaCurso consultaCursoInternalFrame;
	protected ConsultaProgFormacion consultaProgFormacionInternalFrame;
	private ConsultaUsuario consultaUsuarioInternalFrame;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		
		controladorAltaPublish cAP = new controladorAltaPublish();
      cAP.publicar();
		
       controladorIniciarSesionPublish cIS = new controladorIniciarSesionPublish();
        cIS.publicar();
        /* 
        controladorModificarUsuarioPublish cMU = new controladorModificarUsuarioPublish();
        cMU.publicar();
        
        controladorSeguirPublish cS = new controladorSeguirPublish();
        cS.publicar();
        */
        
		initialize();
		Conexion conexion=Conexion.getInstancia();

		Fabrica fabrica = Fabrica.getInstancia();
		IControladorAlta iCAlta = fabrica.getIControladorAlta();
		IControladorModificarUsuario iCModUser = fabrica.getIControladorModificarUsuario();
		
		
		
		Dimension desktopSize = frame.getSize();
		Dimension jInternalFrameSize;
		
		altaInstitutoInternalFrame = new AltaInstituto(iCAlta);
		jInternalFrameSize = altaInstitutoInternalFrame.getSize();
		altaInstitutoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		altaInstitutoInternalFrame.setVisible(false);
		frame.getContentPane().add(altaInstitutoInternalFrame);
		
		altaEdicionCursoInternalFrame = new AltaEdicionCurso(iCAlta);
		jInternalFrameSize = altaEdicionCursoInternalFrame.getSize();
		altaEdicionCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		altaEdicionCursoInternalFrame.setVisible(false);
		frame.getContentPane().add(altaEdicionCursoInternalFrame);
		
		altaUsuarioInternalFrame = new AltaUsuario(iCAlta);
		altaUsuarioInternalFrame.setBounds(43, 0, 679, 408);
		frame.getContentPane().add(altaUsuarioInternalFrame);
		altaUsuarioInternalFrame.setVisible(false);
		
	    altaCursoInternalFrame = new AltaCurso(iCAlta);
		jInternalFrameSize = altaCursoInternalFrame.getSize();
		altaCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		altaCursoInternalFrame.setVisible(false);
		frame.getContentPane().add(altaCursoInternalFrame);
		
		modificarUsuarioInternalFrame = new ModificarUsuario(iCModUser);
		modificarUsuarioInternalFrame.setBounds(0, 0, 576, 396);
		frame.getContentPane().add(modificarUsuarioInternalFrame);
		modificarUsuarioInternalFrame.setVisible(false);
		
		crearPfInternalFrame = new CrearPF(iCAlta);
		crearPfInternalFrame.setBounds(43, 0, 679, 408);
		frame.getContentPane().add(crearPfInternalFrame);
		crearPfInternalFrame.setVisible(false);
		
		inscripcionEdicionCursoInternalFrame = new InscripcionEdicionCurso(iCAlta);
		inscripcionEdicionCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
			    (desktopSize.height- jInternalFrameSize.height)/2);
		frame.getContentPane().add(inscripcionEdicionCursoInternalFrame);
		inscripcionEdicionCursoInternalFrame.setVisible(false);
		
		agregarCursoProgFormInternalFrame = new AgregarCursoProgForm(iCModUser);
		agregarCursoProgFormInternalFrame.setBounds(43, 0, 679, 408);
		frame.getContentPane().add(agregarCursoProgFormInternalFrame);
		agregarCursoProgFormInternalFrame.setVisible(false);
		
		consultaEdicionCursoInternalFrame = new ConsultaEdicionCurso(iCAlta);
		consultaEdicionCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
			    (desktopSize.height- jInternalFrameSize.height)/2);
		frame.getContentPane().add(consultaEdicionCursoInternalFrame);
		consultaEdicionCursoInternalFrame.setVisible(false);
		
		consultaCursoInternalFrame = new ConsultaCurso(iCAlta, iCModUser);
		jInternalFrameSize = consultaCursoInternalFrame.getSize();
		consultaCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		consultaCursoInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2, 0);
		consultaCursoInternalFrame.setVisible(false);
		frame.getContentPane().add(consultaCursoInternalFrame);
		
		consultaProgFormacionInternalFrame = new ConsultaProgFormacion(iCModUser,iCAlta);
		jInternalFrameSize = consultaProgFormacionInternalFrame.getSize();
		consultaProgFormacionInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		consultaProgFormacionInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2, 0);
		consultaProgFormacionInternalFrame.setVisible(false);
		frame.getContentPane().add(consultaProgFormacionInternalFrame);
		
		consultaUsuarioInternalFrame = new ConsultaUsuario(iCAlta);
		jInternalFrameSize = consultaUsuarioInternalFrame.getSize();
		consultaUsuarioInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		consultaUsuarioInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2, 0);
		consultaUsuarioInternalFrame.setVisible(false);
		frame.getContentPane().add(consultaUsuarioInternalFrame);
		
		altaCategoriaInternalFrame = new AltaCategoria(iCAlta);
		jInternalFrameSize = altaCategoriaInternalFrame.getSize();
		altaCategoriaInternalFrame.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
		    (desktopSize.height- jInternalFrameSize.height)/2);
		altaCategoriaInternalFrame.setVisible(false);
		frame.getContentPane().add(altaCategoriaInternalFrame);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 792, 625);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 101, 22);
		frame.setJMenuBar(menuBar);
		
		JMenu mnAlta = new JMenu("Alta");
		menuBar.add(mnAlta);
		
		JMenuItem mntmAltaInstituto = new JMenuItem("Instituto");
		mnAlta.add(mntmAltaInstituto);
		
		JMenuItem mntmAltaEdicion = new JMenuItem("Edicion de Curso");
		mnAlta.add(mntmAltaEdicion);
		mntmAltaEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				altaEdicionCursoInternalFrame.refreshInstituto();
				}catch(Exception e1) {
					
				}
				altaEdicionCursoInternalFrame.setVisible(true);
			}
		});
		
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Usuario");
		mntmAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					altaUsuarioInternalFrame.refresh();
					}catch(Exception e1) {
						
					}
				
				altaUsuarioInternalFrame.setVisible(true);
			}
		});
		mnAlta.add(mntmAltaUsuario);
		mntmAltaInstituto.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				altaInstitutoInternalFrame.setVisible(true);
			}
		});;
		
		
		JMenuItem mntmAltaCurso = new JMenuItem("Curso");
		mnAlta.add(mntmAltaCurso);
		
		JMenu mnModificar = new JMenu("Modificar");
		menuBar.add(mnModificar);
		
		JMenuItem mntmModificarUsuario = new JMenuItem("Usuario");
		mntmModificarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					modificarUsuarioInternalFrame.refresh();
					}catch(Exception e1) {
						
					}
				
				modificarUsuarioInternalFrame.setVisible(true);
			}
		});
		mnModificar.add(mntmModificarUsuario);
		
		JMenuItem mntmAgregarCursoProgForm = new JMenuItem("Agregar Curso a Prog. Formacion");
		mntmAgregarCursoProgForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					agregarCursoProgFormInternalFrame.refresh();
					}catch(Exception e1) {
						
					}
				
				agregarCursoProgFormInternalFrame.setVisible(true);
			}
		});
		mnModificar.add(mntmAgregarCursoProgForm);
		
		mntmAltaCurso.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				altaCursoInternalFrame.refresh();
				altaCursoInternalFrame.setVisible(true);
			}
		});;
		
		JMenuItem mntmCrearPF = new JMenuItem("Programa de Formaci\u00F3n");
		mntmCrearPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				crearPfInternalFrame.setVisible(true);
			}
			
		});
	
		mnAlta.add(mntmCrearPF);
		
		JMenuItem mntmAltaCategoria = new JMenuItem("Categoria");
		mntmAltaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaCategoriaInternalFrame.setVisible(true);
			}
		});
		mnAlta.add(mntmAltaCategoria);
		
		JMenu mnNewMenu = new JMenu("Inscripcion");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmInscripcionEdicionCurso = new JMenuItem("Edicion de Curso");
		mntmInscripcionEdicionCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					inscripcionEdicionCursoInternalFrame.refreshInstituto();
					}catch(Exception e1) {
						
					}
				
				inscripcionEdicionCursoInternalFrame.setVisible(true);
			}
			
		});
		mnNewMenu.add(mntmInscripcionEdicionCurso);
		
		JMenu mnNewMenu_1 = new JMenu("Consulta");
		menuBar.add(mnNewMenu_1);
		
	
		JMenuItem mntmCurso = new JMenuItem("Curso");
		mntmCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					consultaCursoInternalFrame.inicializarComboBox();
					}catch(Exception e1) {
						
					}
				consultaCursoInternalFrame.clearAllData();
				consultaCursoInternalFrame.setVisible(true);
				
			}
		});
		mnNewMenu_1.add(mntmCurso);
		
		JMenuItem mntmNewMenuItem_CEdCurso = new JMenuItem("Edici\u00F3n de Curso");
		mntmNewMenuItem_CEdCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					consultaEdicionCursoInternalFrame.refreshInstituto();
					}catch(Exception e1) {
						
					}
				consultaEdicionCursoInternalFrame.limpiar();
				consultaEdicionCursoInternalFrame.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_CEdCurso);
		
		JMenuItem mntmConsultausuario = new JMenuItem("Usuario");
		mnNewMenu_1.add(mntmConsultausuario);
		mnNewMenu_1.add(mntmCurso);
		mntmConsultausuario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					consultaUsuarioInternalFrame.refresh();
					}catch(Exception e1) {
						
					}
				
				consultaUsuarioInternalFrame.setVisible(true);
			}
		});
		
		JMenuItem mntmConsultaPF = new JMenuItem("Programa de Formacion");
		mntmConsultaPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					consultaProgFormacionInternalFrame.refreshPF();
					}catch(Exception e1) {
						
					}
				consultaProgFormacionInternalFrame.setVisible(true);
			}
		});
		
		mnNewMenu_1.add(mntmConsultaPF);
		
		
		
	}
}
