package interfaces;

import controladores.ControladorAlta;
import controladores.ControladorIniciarSesion;
import controladores.ControladorModificarUsuario;
import interfaces.IControladorAlta;

public class Fabrica {
	private static Fabrica instancia = null;
	
	private Fabrica(){}
	
	public static Fabrica getInstancia() {
		if (instancia == null)
			instancia = new Fabrica();
		return instancia;
	}
	public IControladorAlta getIControladorAlta() {
		return new ControladorAlta();
	}
	public IControladorModificarUsuario getIControladorModificarUsuario() {
		return new ControladorModificarUsuario();
	}
	
	public IControladorIniciarSesion getControladorIniciarSesion() {
		return new ControladorIniciarSesion();
	}
}

