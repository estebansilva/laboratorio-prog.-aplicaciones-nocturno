package interfaces;

import java.util.ArrayList;
import java.util.List;

import datatypes.DtProgFormacion;
import datatypes.DtUsuario;
import excepciones.CursoEnProgFormacionRepetidoException;
import excepciones.inscripcionEdicionRepetidaException;
import logica.Curso;
import logica.Edicion;
import logica.Instituto;
import manejadores.ManejadorCurso;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorInstituto;

public interface IControladorModificarUsuario {
	public void modificarUsuario(DtUsuario dtusuario);
	
	public DtUsuario obtenerDt(String nick);
	
	public void setCursoProgFormacion(String nombreC, String nombrePF) throws CursoEnProgFormacionRepetidoException;
	
	public List<String> setInstitutos();
	
	@SuppressWarnings("null")
	public List<String> setCursos(String s);

	public List<String> setEstudiantes();


	public List<String> setEdiciones(String s);

	List<String> setDocente();
	public List<String> getPf();
	
	public List<String> getCursoPorPF(String pf);
	
	public DtProgFormacion obtenerDtPF(String pf);
}
