package interfaces;

import excepciones.IniciarSesionException;
import excepciones.UsuarioRepetidoException;

public interface IControladorIniciarSesion {
	public boolean existeUsuario(String nick);
	public boolean iniciarSesionbool(String nick, String pass);
	public void iniciarSesion(String nick, String pass)throws IniciarSesionException;
	public String convertirEmalNick(String s);
}
