package interfaces;

import java.util.Date;
import java.util.List;

import datatypes.DtEdicion;
import datatypes.DtEstudiante;
import datatypes.DtCurso;
import datatypes.DtDocente;
import datatypes.DtInstituto;
import datatypes.DtPF;
import datatypes.DtUsuario;
import excepciones.CursoRepetidaException;
import excepciones.CursoVacioException;
//import excepciones.CursoRepetidaException;
import excepciones.InstitutoRepetidaException;
import excepciones.InstitutoVacioException;
import excepciones.ProgFormacionRepetidoException;
import excepciones.UsuarioRepetidoException;
import excepciones.categoriaRepetidaException;
import excepciones.inscripcionEdicionRepetidaException;
import excepciones.inscripcionPFormacionRepetidaException;
import excepciones.EdicionRepetidaException;

public interface IControladorAlta {
	
	public void ingresarInstituto(DtInstituto dtin) throws InstitutoRepetidaException; 
	
	public void ingresarDocente(DtUsuario dtusuario, DtInstituto dtinstituto)throws UsuarioRepetidoException; 
	
	public void ingresarEstudiante(DtUsuario dtusuario)throws UsuarioRepetidoException; 

	public void ingresarCurso(DtCurso dtc, DtInstituto dti, List<String> ls, List<String> categoria) throws CursoRepetidaException;

	public void IngresarProgramaF(DtPF dtpf) throws ProgFormacionRepetidoException;
	
	public void ingresarEdicion(DtEdicion dtEdi,List<String> docente, String curso) throws EdicionRepetidaException;
	
	public void inscripcionEdicionCurso(String ins, String cur, String edi, String est) throws inscripcionEdicionRepetidaException;
	
	public List<String> setInstitutos();
	
	public List<String> setEstudiantes();
	
	public List<String> setCursos(String s);
	
	public List<String> setEdiciones(String s);
	
	public DtEdicion darInfoEdicion(String s);
	
	public List<String> getDocentes(String s);

	public String[] getInfoCurso(String s);
	
	public List<String> getPFromacion(String s);
	
	public boolean esDocnete(String s);
	
	public List<String> getUsuarios();
	
	public String getDocneteInstituto(String s);
	
	public DtDocente getInfoDocente(String s);
	
	public List<String> getEdicionDcocente(String s);
	
	public DtEstudiante getInfoEstudiante(String s);
	
	public List<String> getEdicionEstudiante(String s);
	
	public List<String> getPreviaCurso(String s);
	
	public List<String> getEstudianteEnEdicion(String s);
	
	public List<String> getDocenteEdicion(String s);
	
	public void ingresarCategoria(String nombre) throws categoriaRepetidaException;
	
	public List<String> listaDeCategorias();
	
	public boolean existenickemail(String n, String e);
	
	public boolean existeCategoria(String S);
	
	public List<String>cursosporCategoria(String c);
	
	public boolean edicionEsVigente(String s);
	
	public String[] getInfoEdicion(String s);
	
	public List<String> setTodosLosCursos();
	
	public boolean esposibleinscribirse(String edi, String est);
	
	public DtEdicion edicionVigente(String c);

	public String getEstadoInscripcionEdicion(String estudiante, String edicion);
	
	public void changeEstadoInscripcionEdicion(String estudiante, String edicion, boolean aceptado);
	
	public List<String[]> getEstudianteEnEdicionAceptado(String s);
	
	public boolean existeCurso(String S);
	
	public List<String> obtenerCategoriadeCurso(String c);
	
	public List<String> buscaNombre(String c);
	
	public List<String> buscaFecha(String c);
}
