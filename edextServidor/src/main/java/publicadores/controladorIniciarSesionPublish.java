package publicadores;

import excepciones.IniciarSesionException;
import excepciones.UsuarioRepetidoException;
import interfaces.Fabrica;
import interfaces.IControladorAlta;
import interfaces.IControladorIniciarSesion;
import logica.Docente;
import logica.Estudiante;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;
import configuraciones.WebServiceConfiguracion;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class controladorIniciarSesionPublish {
	
	private Fabrica fabrica;
	private IControladorIniciarSesion icon;
	private WebServiceConfiguracion configuracion;
	private Endpoint endpoint;
	
	public controladorIniciarSesionPublish() {
		// TODO Auto-generated constructor stub
		fabrica = Fabrica.getInstancia();
		icon = fabrica.getControladorIniciarSesion();
	}


	@WebMethod(exclude = true)
	public void publicar() {
	String address = "http://127.0.0.1:1942/controladorIniciarSesion" ;
	endpoint = Endpoint.publish(address, this);
	}


	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
	    return endpoint;
	}

	@WebMethod
	public boolean existeUsuario(String nick) {
		return icon.existeUsuario(nick);
	}

	@WebMethod
	public boolean iniciarSesionbool(String nick, String pass) {
		return icon.iniciarSesionbool(nick, pass);
		
	}

	@WebMethod
	public void iniciarSesion(String nick, String pass) {
		try {
			icon.iniciarSesion(nick, pass);
		} catch (IniciarSesionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@WebMethod
	public String convertirEmalNick(String s) {
		return icon.convertirEmalNick(s);
	}
	
@WebMethod
public boolean iniciarSesionboolEstudiante(String nick, String pass) {
	ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
	if(mE.existeEstudiante(nick)) {
		Estudiante e=mE.buscarEstudiante(nick);
		if(e.getPass().equals(pass))
			return true;
	}else if(mE.existeCorreo(nick)) {
		Estudiante e=mE.buscarEstudiante(mE.obtenerEstudianteEmail(nick));
		if(e.getPass().equals(pass))
			return true;
	}
	return false;
	
}
	
	
}
