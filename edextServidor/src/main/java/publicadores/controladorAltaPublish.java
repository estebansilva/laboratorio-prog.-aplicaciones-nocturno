package publicadores;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;
import configuraciones.WebServiceConfiguracion;
import controladores.ControladorAlta;
import datatypes.DtCurso;
import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtEstudiante;
import datatypes.DtInstituto;
import datatypes.DtPF;
import datatypes.DtUsuario;
import datatypes.Estado;
import excepciones.CursoRepetidaException;
import excepciones.EdicionRepetidaException;
import excepciones.InstitutoRepetidaException;
import excepciones.ProgFormacionRepetidoException;
import excepciones.UsuarioRepetidoException;
import excepciones.categoriaRepetidaException;
import excepciones.inscripcionEdicionRepetidaException;
import excepciones.inscripcionPFormacionRepetidaException;
import interfaces.Fabrica;
import interfaces.IControladorAlta;
import logica.Categoria;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.InscripcionEd;
import logica.Instituto;
import logica.ProgFormacion;
import manejadores.ManejadorCategoria;
import manejadores.ManejadorCurso;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEdicion;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorInscripcionEd;
import manejadores.ManejadorInstituto;
import manejadores.ManejadorProgFormacion;
import utils.Conexion;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class controladorAltaPublish {
	private Fabrica fabrica;
	private WebServiceConfiguracion configuracion;
	private IControladorAlta icon;
	private Endpoint endpoint;

	public controladorAltaPublish() {
		fabrica = Fabrica.getInstancia();
		icon = fabrica.getIControladorAlta();
	}


@WebMethod(exclude = true)
public void publicar() {
    String address = "http://127.0.0.1:1942/controladorAlta" ;
	endpoint = Endpoint.publish(address, this);
}

@WebMethod(exclude = true)
public Endpoint getEndpoint() {
    return endpoint;
}


@WebMethod
public String[] setInstitutos(){
	ManejadorInstituto mi = ManejadorInstituto.getInstancia();
	 List<String> lista = mi.obtenerInstitutos();
	 String[] listaReturn = new String[lista.size()];
	 listaReturn = lista.toArray(listaReturn);
	 return listaReturn;
}

@WebMethod
public String[] listaDeCategorias(){
	ManejadorCategoria mc= ManejadorCategoria.getInstancia();
	 List<String> lista = mc.obtenerCategorias();
	 String[] listaReturn = new String[lista.size()];
	 return listaReturn = lista.toArray(listaReturn);
}


@WebMethod
public String[] setTodosLosCursos(){
	ManejadorCurso mC = ManejadorCurso.getInstancia();
	List<String> lista = mC.obtenerCurso();
	String[] listaReturn = new String[lista.size()];
	return listaReturn = lista.toArray(listaReturn);
}

@WebMethod
public String[] setCursos(String s){
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	List<String> lista = mc.obtenerCursoInstituto(s);
	String[] listaReturn = new String[lista.size()];
	return listaReturn = lista.toArray(listaReturn);
}


@WebMethod
public boolean existeCurso(String S) {
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	return !mc.existeCurso(S);
}

@WebMethod
public String[] getDocentes(String s){
	ManejadorDocente md = ManejadorDocente.getInstancia();
	List<String> resultd = md.obtenerDocentesInstituto(s);
	String[] listaReturn = new String[resultd.size()];
	return listaReturn = resultd.toArray(listaReturn);
}

@WebMethod
public void ingresarEdicion(String[] docente, String nomEdiCurso, Calendar f1, Calendar f2, int cupoInt, Calendar f3, String curso) throws Exception {
	
	List <String> docentesList = new ArrayList<String>();
	docentesList = Arrays.asList(docente);
	
	
	DtEdicion dte = new DtEdicion(nomEdiCurso, f1.getTime(), f2.getTime(), cupoInt, f3.getTime());
	try {
		icon.ingresarEdicion(dte, docentesList, curso);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

@WebMethod
public void ingresarDocente(java.lang.String nick, java.lang.String nombre, java.lang.String apellido, java.lang.String correo, java.util.Calendar fechaNac, java.lang.String pass, String instituto) throws Exception {
	DtInstituto dti = new DtInstituto(instituto);
	DtUsuario dtu = new DtUsuario(nick,nombre,apellido,correo,fechaNac.getTime(),pass);
	
	try {
		icon.ingresarDocente(dtu, dti);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

@WebMethod
public void ingresarEstudiante(java.lang.String nick, java.lang.String nombre, java.lang.String apellido, java.lang.String correo, java.util.Calendar fechaNac, java.lang.String pass) throws Exception {
	
	DtUsuario dtu = new DtUsuario(nick,nombre,apellido,correo,fechaNac.getTime(),pass);
	try {
		icon.ingresarEstudiante(dtu);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
} 

@WebMethod
public void ingresarCurso(String[] previas, String[] categoria, String nombre, String descrip, String duracion, int horas, 
		int creditos, Calendar fechaRC, String url, String instituto ) throws Exception {
	// TODO Auto-generated method stub
	System.out.println("estoy en el servidor");
	
	DtCurso dtc = new DtCurso(nombre,descrip,duracion,horas,creditos,fechaRC.getTime(),url);
    DtInstituto dti = new DtInstituto(instituto);
	
	List <String> listaPrevias = new ArrayList<String>();
	if (previas != null) {
		listaPrevias = Arrays.asList(previas);
	}
	
	List <String> listaCategoria = new ArrayList<String>();
	if (categoria != null) {
		listaCategoria = Arrays.asList(categoria);
	}
	
	try {
		icon.ingresarCurso(dtc, dti, listaPrevias, listaCategoria);
		System.out.println("entro al try con: " + nombre + descrip + duracion + horas + creditos + fechaRC.getTime());
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

@WebMethod
public boolean esDocnete(String s) {
	// TODO Auto-generated method stub
	ManejadorDocente md = ManejadorDocente.getInstancia();
	if(md.existeDocente(s))
		return true;
	return false;
}


@WebMethod
public String[] getDocenteEdicion(String s) {
	  ManejadorDocente md = ManejadorDocente.getInstancia();
        ManejadorEdicion me = ManejadorEdicion.getInstancia();
        Edicion e = me.buscarEdicion(s);
        List<String> d = md.obtenerDocentes();
        List<String> result = new ArrayList();
        for(String doc:d) {
            Docente docente = md.buscarDocente(doc);
            if(docente.existeEdicion(e))
                result.add(doc);
        } 
    	
        String[] listaReturn = new String[result.size()];
    	return listaReturn = result.toArray(listaReturn);
}

@WebMethod
public String[] getEdicionEstudiante(String s) {
	// TODO Auto-generated method stub
	ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
	Estudiante e = me.buscarEstudiante(s);
	List<String> result = new ArrayList<String>();
	List<InscripcionEd> ie = e.getInscripcionEd();
	for(InscripcionEd i:ie) {
		result.add(i.getEdicion().getNombre());
	} 
    String[] listaReturn = new String[result.size()];
	return listaReturn = result.toArray(listaReturn);
}

@WebMethod
public boolean existenickemail(String n, String e) {
	ManejadorDocente mD = ManejadorDocente.getInstancia();
	ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
	if((mD.buscarDocente(n) != null) ||(mE.buscarEstudiante2(n) != null) ||(mD.existeCorreo(e) || (mE.existeCorreo(e))))
		return true;
	return false;
}


@WebMethod
public boolean existeCategoria(String S) {
	ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
	if(!mCat.existeCategoria(S))
		return true;
	return false;
}

@WebMethod
public String[] cursosporCategoria(String c) {

	ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
	Categoria cat = mCat.buscarCategorias(c);
	List<String> resultado = new ArrayList<String>(); 
	List<Curso> cursos= cat.getCurso();
	for(Curso cur:cursos) {
		resultado.add(cur.getNombre());
	}

    String[] listaReturn = new String[resultado.size()];
	return listaReturn = resultado.toArray(listaReturn);
}

@WebMethod
public String[]  getPreviaCurso(String s) {
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	Curso c = mc.buscarCurso(s);
	List<Curso> cursos = c.getPrevias();
	List<String> result2 = new ArrayList<String>();;
	for(Curso cu:cursos) {
		result2.add(cu.getNombre());
	} 

    String[] listaReturn = new String[result2.size()];
	return listaReturn = result2.toArray(listaReturn);
}

@WebMethod
public String[] obtenerCategoriadeCurso(String c) {
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	Curso curso = mc.buscarCurso(c);
	List<Categoria> listacat = curso.getCategoria();
	List<String> resultado = new ArrayList<String>();
	for(Categoria cat:listacat) {
		resultado.add(cat.getNombre());
		}

    String[] listaReturn = new String[resultado.size()];
	return listaReturn = resultado.toArray(listaReturn);
}

@WebMethod
public  String[] setEdiciones(String s) {
	// TODO Auto-generated method stub
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	Curso c = mc.buscarCurso(s);
	List<Edicion> ed = c.getEdicion();
	List<String> result2 = new ArrayList<String>();
	for(Edicion e:ed) {
		result2.add(e.getNombre());
	} 
    String[] listaReturn = new String[result2.size()];
	return listaReturn = result2.toArray(listaReturn);
}

public String[] getPFromacion(String cursoElegido){
	ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
	ManejadorCurso mcur = ManejadorCurso.getInstancia();
	List<String> progFormacionExiste = new ArrayList<String>();
	List<String> listapf = mPF.obtenerProgFormacion();
	Curso cursoElegidoInstancia = mcur.buscarCurso(cursoElegido);
	 
	 for(String pf:listapf) {
		ProgFormacion instanciaPF =  mPF.buscarProgFormacion(pf);
		if ( instanciaPF.existeCurso(cursoElegidoInstancia) )
			progFormacionExiste.add((String)instanciaPF.getNombre());
	 }

    String[] listaReturn = new String[progFormacionExiste.size()];
	return listaReturn = progFormacionExiste.toArray(listaReturn);
}

@WebMethod
public String[] getInfoCurso(String s) {
	//String s es el nombre de la clase que quiero buscar
	ManejadorCurso mcur = ManejadorCurso.getInstancia();		
	return mcur.infoCurso(s);
}

@WebMethod
public String[] getEstudianteEnEdicion(String s) {
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion ed = mE.buscarEdicion(s);
	List<String> result = new ArrayList<String>();
	List<InscripcionEd> ie = ed.getInscripcionEd();
	for(InscripcionEd i:ie) {
		result.add(i.getEstudiante().getNick());
	} 

    String[] listaReturn = new String[result.size()];
	return listaReturn = result.toArray(listaReturn);
}


@WebMethod
public DtEdicion darInfoEdicion(String s) {
	// TODO Auto-generated method stub
	
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion edic = mE.buscarEdicion(s);
	DtEdicion dtedic = new DtEdicion(edic.getNombre(), edic.getFechaI(), edic.getFechaF(),edic.getCupo(), edic.getFechaPub());
	return dtedic;
}


@WebMethod
public void changeEstadoInscripcionEdicion(String estudiante, String edicion, boolean aceptado) {
	ManejadorInscripcionEd mIE = ManejadorInscripcionEd.getInstancia();
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion ed = mE.buscarEdicion(edicion);
	List<InscripcionEd> ie = ed.getInscripcionEd();//traigo lista con todas las inscripciones
	Conexion conexion=Conexion.getInstancia();
	EntityManager em =conexion.getEntityManager();
    
	for(InscripcionEd i:ie) {
		em.getTransaction().begin();
		em.persist(i);
		if (i.getEstudiante().getNick().equals(estudiante)) {
			if (aceptado) {
				i.setEstado(Estado.Aceptado);
			}else {
				i.setEstado(Estado.Rechazado);
			}
		}
		em.getTransaction().commit();
	}
}

@WebMethod
public  String[] getEdicionDcocente(String s) {
	// TODO Auto-generated method stub
	ManejadorDocente md = ManejadorDocente.getInstancia();
	Docente d = md.buscarDocente(s);
	List<String> result = new ArrayList<>();
	List<Edicion> ed = d.getEdicion();
	for(Edicion e:ed) {
		result.add(e.getNombre());
	} 

    String[] listaReturn = new String[result.size()];
	return listaReturn = result.toArray(listaReturn);
}

@WebMethod
public String[][] getEstudianteEnEdicionAceptado(String s) {
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion ed = mE.buscarEdicion(s);
	List<String[]> result = new ArrayList();
	List<InscripcionEd> ie = ed.getInscripcionEd();
	String[][] resultadoLista = null;
	int contador = 0;
	for(InscripcionEd i:ie) {
		if (i.getEstado().equals(Estado.Aceptado))	{

			String[] edicionRet = {
					i.getEstudiante().getNick(),
					i.getEstado().toString()
					};
			result.add(edicionRet);
			resultadoLista[contador] = edicionRet;
			contador = contador + 1;
		}
	}  
	
	return resultadoLista;
}


@WebMethod
public String[] getInfoEdicion(String s) {
	//String s es el nombre de la clase que quiero buscar
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion edic = mE.buscarEdicion(s);
	return mE.infoEdicion(s);
}
@WebMethod
public DtEdicion edicionVigente(String c) {
	ManejadorCurso mC = ManejadorCurso.getInstancia();
	Curso curso = mC.buscarCurso(c);
	List<Edicion> ediciones = curso.getEdicion();
	Date fecha = new Date();
	for(Edicion ed:ediciones) {
		if(ed.getFechaI().after(fecha)){
			DtEdicion dte = new DtEdicion(ed.getNombre(), ed.getFechaI(), ed.getFechaF(), ed.getCupo(), ed.getFechaPub());
			return dte;
		}
	}
	return null;
}


@WebMethod
public void inscripcionEdicionCurso(String ins, String cur, String edi, String est) throws Exception {	
	try {
		icon.inscripcionEdicionCurso(ins, cur, edi, est);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

@WebMethod
public boolean esposibleinscribirse(String edi, String est) {
	if(icon.esposibleinscribirse(edi, est))
		return false;
	else return true;
}	

@WebMethod
public String getEstadoInscripcionEdicion(String estudiante, String edicion) {
	String estado = "";
	ManejadorInscripcionEd mIE = ManejadorInscripcionEd.getInstancia();
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion ed = mE.buscarEdicion(edicion);
	List<InscripcionEd> ie = ed.getInscripcionEd();//traigo lista con todas las inscripciones
	for(InscripcionEd i:ie) {
		if (i.getEstudiante().getNick().equals(estudiante)) {
			estado = i.getEstado().toString();
		}
	}	
	return estado;
}


@WebMethod
public String[] buscaNombre(String c) {
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	List<String> result = mc.obtenerCursoPorBusqueda(c);
    String[] listaReturn = new String[result.size()];
	return listaReturn = result.toArray(listaReturn);
}

@WebMethod
public String[] buscaFecha(String c) {
	ManejadorCurso mc = ManejadorCurso.getInstancia();
	List<String> result =  mc.obtenerCursoPorFecha(c);
    String[] listaReturn = new String[result.size()];
	return listaReturn = result.toArray(listaReturn);
}

/*
@WebMethod
public void ingresarInstituto(DtInstituto dtin) throws InstitutoRepetidaException {
	ManejadorInstituto mI = ManejadorInstituto.getInstancia();
	Instituto ins = mI.buscarInstituto(dtin.getNombre());
	if (ins != null) 
		throw new InstitutoRepetidaException("El Instituto " + dtin.getNombre() + " ya existe");
	ins = new Instituto(dtin.getNombre());
	mI.agregarInstituto(ins);
}


@WebMethod
public void IngresarProgramaF(DtPF dtpf) throws ProgFormacionRepetidoException {
	ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
	if (mPF.buscarProgFormacion(dtpf.getNombre()) != null)
		throw new ProgFormacionRepetidoException("El Programa de formacin " + dtpf.getNombre() + " ya existe");	
	ProgFormacion progformacion = new ProgFormacion(dtpf.getNombre(), dtpf.getDescripcion(), dtpf.getFechaI(), dtpf.getFechaF());
	mPF.agregarProgFormacion(progformacion);
	
}


@WebMethod
public ArrayList<String> setEstudiantes() {
	// TODO Auto-generated method stub
	ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
	return (ArrayList<String>) me.obtenerEstudiantes();
}


@WebMethod
public void inscripcionEdicionCurso(String ins, String cur, String edi, String est)
		throws inscripcionEdicionRepetidaException {
	// TODO Auto-generated method stub
	ManejadorEdicion medi = ManejadorEdicion.getInstancia();
	ManejadorEstudiante mest = ManejadorEstudiante.getInstancia();
	ManejadorInscripcionEd mi = ManejadorInscripcionEd.getInstancia();
	Estudiante estu = mest.buscarEstudiante2(est);
	Edicion edicion = medi.buscarEdicion(edi);
	InscripcionEd i = new InscripcionEd(estu, edicion);
	Date comp = new Date();
	if(mi.existeInscripcionEd(edi, est)|| (edicion.getFechaI().before(comp)) || (edicion.getCupo() == 0))
		throw new inscripcionEdicionRepetidaException("La Inscripcion a la Edicion " + edi + " y estudiante "+ est + " ya existe,  la fecha expiro o no hay cupo");
	
	if(edicion.getCupo() > 0) {
		medi.restarCupo(edi);
		Conexion conexion= Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    em.getTransaction().begin();
	    em.persist(edicion);
	    edicion.agregarInscripcionEd(i);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
	    em.persist(estu);
	    estu.agregarInscripcionEd(i);
		em.getTransaction().commit();

		mi.agregarInscripcionEd(i);
	}
}

@WebMethod
public ArrayList<String> getUsuarios() {
	// TODO Auto-generated method stub
	ManejadorDocente md = ManejadorDocente.getInstancia();
	List<String> d = md.obtenerDocentes();
	List<String> result = new ArrayList();
	result.addAll(setEstudiantes());
	result.addAll(d);
	return (ArrayList<String>) result;
}

@WebMethod
public String getDocneteInstituto(String s) {
	// TODO Auto-generated method stub
	ManejadorDocente md = ManejadorDocente.getInstancia();
	Docente d = md.buscarDocente(s);
	return d.getInstituto().getNombre();
}

@WebMethod
public DtDocente getInfoDocente(String s) {
	// TODO Auto-generated method stub
	ManejadorDocente md = ManejadorDocente.getInstancia();
	Docente d = md.buscarDocente(s);
	DtDocente dt = new DtDocente(d.getNick(), d.getNombre(), d.getApellido(), d.getCorreo(), d.getFechaNac(),d.getPass());
	return dt;
}

@WebMethod
public DtEstudiante getInfoEstudiante(String s) {
	// TODO Auto-generated method stub
	ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
	Estudiante e = me.buscarEstudiante(s);
	DtEstudiante dt = new DtEstudiante(e.getNick(), e.getNombre(), e.getApellido(), e.getCorreo(), e.getFechaNac(),e.getPass());
	return dt;
}


@WebMethod
public void inscripcionPFormacion(String pForm, String est)
		throws inscripcionPFormacionRepetidaException {
	// TODO Auto-generated method stub
	ManejadorProgFormacion mpf = ManejadorProgFormacion.getInstancia();
	ManejadorEstudiante mest = ManejadorEstudiante.getInstancia();
	ManejadorInscripcionPf mipf= ManejadorInscripcionPf.getInstancia();
	ProgFormacion pf = mpf.buscarProgFormacion(pForm);
	Estudiante estu = mest.buscarEstudiante2(est);
	Date comp = new Date();
	if(mipf.existeInscripcionPf(pForm, est))
		throw new inscripcionPFormacionRepetidaException("La Inscripcion al Programa de Formación " + pForm + " y estudiante "+ est + " ya existe o la fecha expiro");
	
		Conexion conexion= Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	  
	    InscripcionPf inscripcionPf = new InscripcionPf(estu, pf); 
	    mipf.agregarInscripcionPf(inscripcionPf);
	    em.getTransaction().begin();
	    em.persist(pf);
	    pf.setInscripcionPf(inscripcionPf);
		em.getTransaction().commit();
		
		em.getTransaction().begin();
	    em.persist(estu);
	    estu.setInscripcionPf(inscripcionPf);
		em.getTransaction().commit();
	
}

@WebMethod
public ArrayList<String> getPFEstudiante(String s) {
	// TODO Auto-generated method stub
	ManejadorProgFormacion mpf= ManejadorProgFormacion.getInstancia();
	ProgFormacion pf= mpf.buscarProgFormacion(s);
	List<String> result = new ArrayList();
	List<InscripcionPf> insPf = pf.getInscripcionPf();
	
	for(InscripcionPf i:insPf) {
		result.add(i.getEstudiante().getNick());
	} 
	return (ArrayList<String>) result;
}

@WebMethod
public ArrayList<String> getEstudianteProgFormacion(String s) {
	// TODO Auto-generated method stub
	ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
	Estudiante e = me.buscarEstudiante(s);
	List<String> result = new ArrayList();
	List<InscripcionPf> ie = e.getInscripcionPf();
	for(InscripcionPf i:ie) {
		result.add(i.getProgFormacion().getNombre());
	} 
	return (ArrayList<String>) result;
}


@WebMethod
public boolean edicionEsVigente(String s) {		
	ManejadorEdicion mE = ManejadorEdicion.getInstancia();
	Edicion edic = mE.buscarEdicion(s);
	Date actual = new Date();
	return (edic.getFechaI().before(actual)) && (actual.before(edic.getFechaF()));		
}






*/
}

