package publicadores;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import datatypes.DtProgFormacion;
import datatypes.DtUsuario;
import excepciones.CursoEnProgFormacionRepetidoException;
import excepciones.EdicionRepetidaException;
import excepciones.InstitutoRepetidaException;
import interfaces.Fabrica;
import interfaces.IControladorModificarUsuario;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.Instituto;
import logica.ProgFormacion;
import manejadores.ManejadorCurso;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorInstituto;
import manejadores.ManejadorProgFormacion;
import manejadores.ManejadorProgFormacion;
import utils.Conexion;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;
import configuraciones.WebServiceConfiguracion;


@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class controladorModificarUsuarioPublish {

	private Fabrica fabrica;
	private IControladorModificarUsuario icon;
	private WebServiceConfiguracion configuracion;
	private Endpoint endpoint;
	
	public controladorModificarUsuarioPublish() {
		// TODO Auto-generated constructor stub
		fabrica = Fabrica.getInstancia();
		icon = fabrica.getIControladorModificarUsuario();
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
	    //endpoint = Endpoint.publish("http://" + configuracion.getConfigOf("#WS_IP") + ":" + configuracion.getConfigOf("#WS_PORT") + "/controlador", this);
		endpoint = Endpoint.publish("http://192.168.1.7:8080/controladorModificarUsuario", this);
		System.out.println("http://192.168.1.7:8080/controladorModificarUsuario");
	}

	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
	    return endpoint;
	}

	/*

	@WebMethod
	public void modificarUsuario(DtUsuario dtusuario) {
		icon.modificarUsuario(dtusuario);
		}
	
	@WebMethod
	public DtUsuario obtenerDt(String nick) {
		return icon.obtenerDt(nick);
	}

	@WebMethod
	public void setCursoProgFormacion( String nombrePF, String nombreCurso) throws CursoEnProgFormacionRepetidoException {
		icon.setCursoProgFormacion(nombrePF, nombreCurso);
	}
	
	@WebMethod
	public List<String> setInstitutos(){
		return icon.setInstitutos();
	}
	
	@WebMethod
	public List<String> setCursos(String s){
		
		return icon.setCursos(s);
	}

	@WebMethod
	public List<String> setEstudiantes() {
		// TODO Auto-generated method stub
		return icon.setEstudiantes();
	}
	@WebMethod
	public List<String> setDocente() {
		// TODO Auto-generated method stub
		
		return icon.setDocente();
	}

	@WebMethod
	public List<String> setEdiciones(String s) {
		// TODO Auto-generated method stub
		return icon.setEdiciones(s);
	}
	@WebMethod
	public List<String> getPf() {
		// TODO Auto-generated method stub
		return icon.getPf();
	}

	@WebMethod
	public List<String> getCursoPorPF(String pf) {
		
		return icon.getCursoPorPF(pf);
	}
	@WebMethod
	public DtProgFormacion obtenerDtPF(String pf) {
		
		return icon.obtenerDtPF(pf);
	}
	*/
}
