package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
@Entity
public class Estudiante extends Usuario {
	
	@OneToMany(mappedBy="estudiante",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEd> inscripcionEd = new ArrayList<>();
	
	

	public Estudiante(String nick, String nombre, String apellido, String correo, Date fechaNac,String pass) {
		super(nick, nombre, apellido, correo, fechaNac,pass);
		// TODO Auto-generated constructor stub
	}

	public List<InscripcionEd> getInscripcionEd() {
		return inscripcionEd;
	}
	
	public void agregarInscripcionEd(InscripcionEd inscripcionEd) {
		this.inscripcionEd.add(inscripcionEd);
	}
	
	
}
