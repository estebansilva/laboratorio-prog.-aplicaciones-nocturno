package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Usuario {
	@Id		//Etiqueta de la primary-key
	private String nick;
	private String nombre;
	private String apellido;
	private String correo;
	private Date fechaNac;
	private String pass;
	
	
	public Usuario(String nick, String nombre, String apellido, String correo, Date fechaNac, String pass) {
		super();
		this.nick = nick;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fechaNac = fechaNac;
		this.pass= pass;
	}
	public String getNick() {
		return nick;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public String getPass() {
		return pass;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}


}
