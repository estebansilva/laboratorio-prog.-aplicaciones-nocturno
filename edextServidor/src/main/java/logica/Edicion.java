package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "Edicion")
public class Edicion {
	@Id     //Etiqueta de la primary-key de la tabla Edicion en este caso
	private String nombre;
	private Date fechaI;
	private Date fechaF;
	private int cupo;
	private Date fechaPub;
	@OneToMany(mappedBy="edicion",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEd> inscripcionEd = new ArrayList<>();
	

	public Edicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub) {
		super();
		this.nombre = nombre;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
		this.cupo = cupo;
		this.fechaPub = fechaPub;
	}

	public String getNombre() {
		return nombre;
	}



	public Date getFechaI() {
		return fechaI;
	}


	public Date getFechaF() {
		return fechaF;
	}


	public int getCupo() {
		return cupo;
	}

	public void setCupo(int cupo) {
		this.cupo = cupo;
	}

	public Date getFechaPub() {
		return fechaPub;
	}

	
	public List<InscripcionEd> getInscripcionEd() {
		return inscripcionEd;
	}
	
	public void agregarInscripcionEd(InscripcionEd inscripcionEd) {
				this.inscripcionEd.add(inscripcionEd);
	}
	
	public boolean existeInscripcionEd(InscripcionEd inscripcionEd) {
		if (this.inscripcionEd.indexOf(inscripcionEd) == -1)
			return true;
		return false;
	}

	
}
