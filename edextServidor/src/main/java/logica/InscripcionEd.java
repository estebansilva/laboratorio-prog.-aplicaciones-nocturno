package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import datatypes.Estado;
@Entity
@Table(name = "InscripcionEd") 
@IdClass(InscripcionEdId.class)
public class InscripcionEd {
	@Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private Estudiante estudiante;

    @Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private Edicion edicion;
    
    private Estado estado;
	private Date fecha;
	
	
	
	public InscripcionEd() {
		super();
		// TODO Auto-generated constructor stub
	}
	public InscripcionEd(Estudiante estudiante, Edicion edicion) {
		super();
		this.estudiante = estudiante;
		this.edicion = edicion;
		this.fecha = new Date();
		this.estado = Estado.Inscripto;
	}

	public Date getFecha() {
		return fecha;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public Edicion getEdicion() {
		return edicion;
	}

	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	
}
