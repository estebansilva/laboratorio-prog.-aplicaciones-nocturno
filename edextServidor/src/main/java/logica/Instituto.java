package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "Instituto")
public class Instituto {
	@Id  //Etiqueta de la primary-key de la tabla Instituto en este caso
	private String nombre;
	@OneToMany(mappedBy="instituto",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Curso> cursos = new ArrayList<>();
	@OneToMany(mappedBy="instituto",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Docente> docente = new ArrayList<>();
	
	
	
	public Instituto(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}


	public List<Curso> getCursos() {
		return cursos;
	}

	//public void setCursos(List<Curso> cursos) {
		//this.cursos = cursos;
	//}
	
	public void agregarCurso(Curso curso) {
		this.cursos.add(curso);
	}
	
	public void agregarDocente(Docente docente) {
				this.docente.add(docente);
	}
	
	//public List<Docente> getDocente() {
		//return docente;
	//}

}
