package logica;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Curso")
public class Curso {
	@Id     //Etiqueta de la primary-key de la tabla Curso en este caso
	private String nombre;
	private String descrip;
	private String duracion;
	private int cantHoras;
	private int creditos;
	private Date fechaR;
	private String url;
    @ManyToOne
	private Instituto instituto;
    @OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
    private List<Edicion> edicion = new ArrayList<>();
    @ManyToMany
    private List<Curso> previas = new ArrayList<>();
    @ManyToMany
    private List<Categoria> categoria = new ArrayList<>();
	
	public Curso() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Curso(String nombre, String desc, String duracion, int cantHoras,  int creditos, Date fechaR,
			String url, Instituto instituto, List<Categoria> categoria) {
		super();
		this.nombre = nombre;
		this.descrip = desc;
		this.duracion = duracion;
		this.cantHoras = cantHoras;
		this.creditos = creditos;
		this.fechaR = fechaR;
		this.url = url;
		this.instituto = instituto;
		this.categoria= categoria;
	}

	public String getNombre() {
		return nombre;
	}



	public String getDescrip() {
		return descrip;
	}



	public String getDuracion() {
		return duracion;
	}



	public int getCantHoras() {
		return cantHoras;
	}



	public int getCreditos() {
		return creditos;
	}

	

	public Date getFechaR() {
		return fechaR;
	}


	public String getUrl() {
		return url;
	}


	public Instituto getInstituto() {
		return instituto;
	}

	
	public List<Edicion> getEdicion() {
		return edicion;
	}


	public void agregarEdicion(Edicion edicion) {
		this.edicion.add(edicion);
	}

	public List<Curso> getPrevias() {
		return previas;
	}
	
	public void agregarPrevia(Curso curso) {
		this.previas.add(curso);
	}
	
	public List<Categoria> getCategoria() {
		return categoria;
	}

	
	
}
