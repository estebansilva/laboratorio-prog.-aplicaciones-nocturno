package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "ProgFormacion")
public class ProgFormacion {
	@Id		//Etiqueta de la primary-key de la tabla ProFormacion en este caso
	private String nombre;
	private String descrip;
	private Date fechaI;
	private Date fechaF;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Curso> curso = new ArrayList<>();
	
	

	public ProgFormacion(String nombre, String desc, Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descrip = desc;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}

	public String getNombre() {
		return nombre;
	}


	public String getDescrip() {
		return descrip;
	}


	public Date getFechaI() {
		return fechaI;
	}


	public Date getFechaF() {
		return fechaF;
	}

	public List<Curso> getCurso() {
		return curso;
	}
	
	public void agregarCurso(Curso curso) {
				this.curso.add(curso);
	}
	
	public boolean existeCurso(Curso curso) {
		if (this.curso.indexOf(curso) == -1) {
			return false;
		} else {
			return true;
		}
	}


	
}
