package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
@Entity
public class Docente extends Usuario{
	@ManyToOne
	private Instituto instituto;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Edicion> edicion = new ArrayList<>();
	
	
	public Docente(String nick, String nombre, String apellido, String correo, Date fechaNac, Instituto instituto,String pass) {
		super(nick, nombre, apellido, correo, fechaNac,pass);
		// TODO Auto-generated constructor stub
	}

	public Instituto getInstituto() {
		return instituto;
	}

	public void setInstituto(Instituto instituto) {
		this.instituto = instituto;
	}

	public List<Edicion> getEdicion() {
		return edicion;
	}
	
	public void agregarEdicion(Edicion edicion) {
			this.edicion.add(edicion);
	}
	public boolean existeEdicion(Edicion edicion) {
        if (this.edicion.indexOf(edicion) == -1) 
            return false;
        return true;
    }
}
