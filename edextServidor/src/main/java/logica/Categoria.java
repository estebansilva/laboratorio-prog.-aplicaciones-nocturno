package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Categoria")
public class Categoria {
	@Id
	private String nombre;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Curso> curso = new ArrayList<>();


	public Categoria(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public List<Curso> getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso.add(curso);
	}

}
