package excepciones;

public class CursoRepetidaException extends Exception {

	private static final long serialVersionUID = 1L;
	public CursoRepetidaException(String string) {
		super(string);
	}
}
