package excepciones;

public class InstitutoVacioException extends Exception {
	private static final long serialVersionUID = 1L;
	public InstitutoVacioException(String e) {
		super(e);
	}
}
