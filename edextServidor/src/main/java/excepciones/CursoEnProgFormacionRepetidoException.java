package excepciones;

public class CursoEnProgFormacionRepetidoException extends Exception {

	private static final long serialVersionUID = 1L;
	public CursoEnProgFormacionRepetidoException(String string) {
		super(string);
	}

}
