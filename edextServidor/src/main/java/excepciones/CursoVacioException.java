package excepciones;

public class CursoVacioException extends Exception {
	private static final long serialVersionUID = 1L;
	public CursoVacioException(String e) {
		super(e);
	}
}