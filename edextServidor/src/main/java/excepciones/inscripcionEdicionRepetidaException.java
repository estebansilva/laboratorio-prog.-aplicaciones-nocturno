package excepciones;

public class inscripcionEdicionRepetidaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public inscripcionEdicionRepetidaException(String e) {
		super(e);
	}

}
