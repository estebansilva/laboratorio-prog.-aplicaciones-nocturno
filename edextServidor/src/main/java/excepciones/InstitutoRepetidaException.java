package excepciones;

public class InstitutoRepetidaException extends Exception{
	private static final long serialVersionUID = 1L;
	public InstitutoRepetidaException(String e) {
		super(e);
	}
}
