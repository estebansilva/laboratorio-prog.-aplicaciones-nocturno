package excepciones;

public class SeguirRepetidoException extends Exception{

	private static final long serialVersionUID = 1L;
	public SeguirRepetidoException(String string) {
		super(string);
	}
	
}
