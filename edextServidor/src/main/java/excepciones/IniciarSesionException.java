package excepciones;

public class IniciarSesionException extends Exception {
	
		private static final long serialVersionUID = 1L;
		public IniciarSesionException(String string) {
			super(string);
		}
	
}
