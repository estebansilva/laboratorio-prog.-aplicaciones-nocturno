package excepciones;

public class ProgFormacionRepetidoException extends Exception {

	private static final long serialVersionUID = 1L;
	public ProgFormacionRepetidoException(String string) {
		super(string);
	}
}
