package test;

import org.junit.Test;

import interfaces.Fabrica;
import interfaces.IControladorAlta;
import interfaces.IControladorIniciarSesion;
import interfaces.IControladorModificarUsuario;
import utils.Conexion;
import datatypes.DtCurso;
import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtEstudiante;
import datatypes.DtInstituto;
import datatypes.DtPF;
import datatypes.DtProgFormacion;
import datatypes.DtUsuario;
import excepciones.CursoEnProgFormacionRepetidoException;
import excepciones.CursoRepetidaException;
import excepciones.EdicionRepetidaException;
import excepciones.IniciarSesionException;
import excepciones.InstitutoRepetidaException;
import excepciones.ProgFormacionRepetidoException;
import excepciones.UsuarioRepetidoException;
import excepciones.categoriaRepetidaException;
import excepciones.inscripcionEdicionRepetidaException;
import excepciones.inscripcionPFormacionRepetidaException;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssertAlta {
	private Fabrica fab;
	private IControladorAlta iCAlta;
	private IControladorIniciarSesion icis;
	private IControladorModificarUsuario iCMod;

    @Before
    public void iniciar() {
        fab = Fabrica.getInstancia();
        iCAlta = fab.getIControladorAlta();
        icis = fab.getControladorIniciarSesion();
        iCMod = fab.getIControladorModificarUsuario();
    }
	
    @BeforeClass
	public static void cleanup() {
		Conexion conexion=Conexion.getInstancia();

	    EntityManager em = conexion.getEntityManager();
	    if(!em.getTransaction().isActive()) em.getTransaction().begin();

	    em.createNativeQuery("TRUNCATE table categoria CASCADE").executeUpdate();
	    em.createNativeQuery("TRUNCATE table instituto CASCADE").executeUpdate();
	    em.createNativeQuery("TRUNCATE table curso CASCADE").executeUpdate();
	    em.getTransaction().commit();
	}
    
    
    DtInstituto nuevoInsti = new DtInstituto("Instituto test1");
    String nuevaCat = "Cat test1";
	int numero = 10;
	Date fechaActual = new Date();
    DtCurso nuevoCurso = new DtCurso("Curso Test1","Desc","Duracion",numero,numero,fechaActual,"www.fing.edu.uy");
    List<String> listaCat = new ArrayList<String>();
    List<String> listaPrev = new ArrayList<String>();
    List<String> listaDoc = new ArrayList<String>();
    DtUsuario nuevoEst = new DtUsuario("EstudianteTest","Random","TTT","CorreoEst",fechaActual,"pass");
    DtUsuario nuevoDoc = new DtUsuario("DocTest","Random","TTT","CorreoDoc",fechaActual,"pass");
    Date fechaFinal = new Date(fechaActual.getTime() + (1000 * 60 * 60 * 48));
    Date tomorrow = new Date(fechaActual.getTime() + (1000 * 60 * 60 * 24));
    DtEdicion nuevaEdi = new DtEdicion("EdicionTest",tomorrow,fechaFinal,numero,fechaActual);
    DtPF nuevoPF = new DtPF("PFTest","desc",fechaActual,fechaFinal);
    
	@Test
	public void a_altaInstituto() throws InstitutoRepetidaException {
		iCAlta.ingresarInstituto(nuevoInsti);
		boolean existe=false;
		for(String nameInst : iCAlta.setInstitutos()) {
			if (nameInst.equals(nuevoInsti.getNombre()))
				System.out.println(nameInst);
				existe = true;
		}
		assertTrue(existe);
	}
	
	
	@Test
	public void a_altaCat() throws categoriaRepetidaException {
	      iCAlta.ingresarCategoria(nuevaCat);
	      boolean existe=false;
	      for(String nameCat: iCAlta.listaDeCategorias()) {
	    	  if(nameCat.equals(nuevaCat));
	    	  	existe = true;
	      }
	      assertTrue(existe);
	}
	

	@Test
    public void b_altaCurso() throws InstitutoRepetidaException {
       listaCat.add(nuevaCat);
       boolean existe=false;
       try {
       iCAlta.ingresarCurso(nuevoCurso, nuevoInsti, listaPrev, listaCat);
       } catch (CursoRepetidaException e2) {
    	   e2.printStackTrace();
   	   }
       for(String nameCur: iCAlta.setTodosLosCursos()){
    	   if(nameCur.equals(nuevoCurso.getNombre()));
    	   	existe = true;
       }
       assertTrue(existe);
       
    }
	@Test
	public void c_altaEstudiante() throws UsuarioRepetidoException{
		boolean existe=false;
		iCAlta.ingresarEstudiante(nuevoEst);
		for(String nameEst : iCAlta.setEstudiantes()){
	    	   if(nameEst.equals(nuevoEst.getNombre()));
	    	   	   existe = true;
	       }
		assertTrue(existe);
		
	} 
	
    @Test
    public void c_altaDocente() throws UsuarioRepetidoException{
    	boolean existe=false;
    	iCAlta.ingresarDocente(nuevoDoc, nuevoInsti);
    	for(String nameDoc : iCAlta.getDocentes(nuevoInsti.getNombre())){
	    	   if(nameDoc.equals(nuevoDoc.getNombre()));
	    	   	   existe = true;
	       }
		assertTrue(existe);
    }
    @Test
    public void d_altaEdicion() throws EdicionRepetidaException{
    	boolean existe=false;
    	listaDoc.add(nuevoDoc.getNick());
    	iCAlta.ingresarEdicion(nuevaEdi, listaDoc, nuevoCurso.getNombre());
    	for(String nameEdi : iCAlta.setEdiciones(nuevoCurso.getNombre())){
	    	   if(nameEdi.equals(nuevaEdi.getNombre()));
	    	   	   existe = true;
	       }
		assertTrue(existe);
    }
    
    @Test
    public void f_altaPF()throws ProgFormacionRepetidoException{
    	boolean existe=false;
    	iCAlta.IngresarProgramaF(nuevoPF);
    	for(String namePF : iCAlta.getPFromacion(nuevoCurso.getNombre())){
	    	   if(namePF.equals(nuevoPF.getNombre()));
	    	   	   existe = true;
	       }
		assertTrue(existe);
    	
    }
  
    @Test
    public void f_setInsti(){
    	List<String> a = iCAlta.setInstitutos();
		assertTrue(a!=null);
    }
    
    @Test
    public void f_setEst(){
    	List<String> a = iCAlta.setEstudiantes();
		assertTrue(a!=null);
    }
    
    @Test
    public void f_setCur(){
    	List<String> a = iCAlta.setCursos(nuevoInsti.getNombre());
		assertTrue(a!=null);
    }
    
    @Test
    public void f_setEdi(){
    	List<String> a = iCAlta.setEdiciones(nuevoCurso.getNombre());
		assertTrue(a!=null);
    }
    
    @Test
    public void f_setdoc(){
    	List<String> a = iCAlta.getDocentes(nuevoInsti.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setUsu(){
    	List<String> a = iCAlta.getUsuarios();
		assertTrue(a!=null);
    }
    @Test
    public void f_setesdoc(){
		assertTrue(iCAlta.esDocnete(nuevoDoc.getNick()));
    }
    @Test
    public void f_setPFCur(){
    	List<String> a = iCAlta.getPFromacion(nuevoCurso.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setInfcur(){
    	String[] a = iCAlta.getInfoCurso(nuevoCurso.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setdocins(){
    	String a = iCAlta.getDocneteInstituto(nuevoDoc.getNick());
		assertTrue(a==nuevoInsti.getNombre());
    }
    @Test
    public void f_setinfodoc(){
    	DtDocente a = iCAlta.getInfoDocente(nuevoDoc.getNick());
		assertTrue(a!=null);
    }
    @Test
    public void f_setinfoest(){
    	DtEstudiante a = iCAlta.getInfoEstudiante(nuevoEst.getNick());
		assertTrue(a!=null);
    }
    @Test
    public void f_setedidoc(){
    	List<String> a = iCAlta.getEdicionDcocente(nuevoDoc.getNick());
		assertTrue(a!=null);
    }
    @Test
    public void f_setediest(){
    	List<String> a = iCAlta.getEdicionEstudiante(nuevoEst.getNick());
		assertTrue(a!=null);
    }
    @Test
    public void f_setpreviacur(){
    	List<String> a = iCAlta.getPreviaCurso(nuevoCurso.getNombre());
		assertTrue(a!=null);
    }
    
    @Test
    public void f_incipcionEdicion() throws inscripcionEdicionRepetidaException{
    	boolean existe=false;
    	iCAlta.inscripcionEdicionCurso(nuevoInsti.getNombre(), nuevoCurso.getNombre(), nuevaEdi.getNombre(), nuevoEst.getNick());
    	for(String in : iCAlta.getEstudianteEnEdicion(nuevaEdi.getNombre())){
	    	   if(in.equals(nuevoEst.getNick()));
	    	   	   existe = true;
	       }
		assertTrue(existe);
    }
    
    @Test
    public void f_setEstenedi(){
    	List<String> a = iCAlta.getEstudianteEnEdicion(nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setdocEdi(){
    	List<String> a = iCAlta.getDocenteEdicion(nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setescitenickmail(){
		assertTrue(!iCAlta.existenickemail("asasdash", "asdfasaafas@asdadsasd"));
    }
    @Test
    public void f_setexitcat(){
		assertTrue(!iCAlta.existeCategoria("asadfadfs"));
    }
    @Test
    public void f_setcurcat(){
    	List<String> a = iCAlta.cursosporCategoria(nuevaCat);
		assertTrue(a!=null);
    }
    @Test
    public void f_setesvigente(){
		assertTrue(!iCAlta.edicionEsVigente(nuevaEdi.getNombre()));
    }
    @Test
    public void f_setestadoinscest(){
    	String a = iCAlta.getEstadoInscripcionEdicion(nuevoEst.getNick(), nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_xchangeedtinscedi(){
    	iCAlta.changeEstadoInscripcionEdicion(nuevoEst.getNick(), nuevaEdi.getNombre(), true);
    	List<String[]> a = iCAlta.getEstudianteEnEdicionAceptado(nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_xsetaceptadoinscedi(){
    	List<String[]> a = iCAlta.getEstudianteEnEdicionAceptado(nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setexitecur(){
		assertTrue(!iCAlta.existeCurso(nuevoCurso.getNombre()));
    }
    @Test
    public void f_setcatcur(){
    	List<String> a = iCAlta.obtenerCategoriadeCurso(nuevoCurso.getNombre());
		assertTrue(a!=null);
    }
   
    @Test
    public void f_setesposibleinscibirse(){
		assertFalse(iCAlta.esposibleinscribirse(nuevaEdi.getNombre(), nuevoEst.getNick()));
    }
    @Test
    public void f_setdivigente(){
    	DtEdicion a = iCAlta.edicionVigente(nuevoCurso.getNombre());
		assertTrue(a==null);
    }
    @Test
    public void f_setinfoedi(){
    	String[] a = iCAlta.getInfoEdicion(nuevaEdi.getNombre());
		assertTrue(a!=null);
    }
    @Test
    public void f_setexisteus(){
    	assertTrue(icis.existeUsuario(nuevoEst.getNick()));
    }
    @Test
    public void f_setexisteus2(){
    	assertTrue(icis.existeUsuario(nuevoDoc.getNick()));
    }
    @Test
    public void f_setiniciarsesb(){
    	assertTrue(icis.iniciarSesionbool(nuevoEst.getNick(), nuevoEst.getPass()));
    }
    @Test
    public void f_setiniciarsesb2(){
    	assertTrue(icis.iniciarSesionbool(nuevoDoc.getNick(), nuevoDoc.getPass()));
    }
    @Test
    public void f_setiniciarses() throws IniciarSesionException{
    	icis.iniciarSesion(nuevoEst.getNick(), nuevoEst.getPass());
    	assertTrue(icis.iniciarSesionbool(nuevoEst.getNick(), nuevoEst.getPass()));
    }
    @Test
    public void f_setiniciarses2() throws IniciarSesionException{
    	icis.iniciarSesion(nuevoDoc.getNick(), nuevoDoc.getPass());
    	assertTrue(icis.iniciarSesionbool(nuevoDoc.getNick(), nuevoDoc.getPass()));
    }
    @Test
    public void f_setiniciarsesbc(){
    	assertTrue(icis.iniciarSesionbool(nuevoEst.getCorreo(), nuevoEst.getPass()));
    }
    @Test
    public void f_setiniciarsesb2c(){
    	assertTrue(icis.iniciarSesionbool(nuevoDoc.getCorreo(), nuevoDoc.getPass()));
    }
    @Test
    public void f_setiniciarsesc() throws IniciarSesionException{
    	icis.iniciarSesion(nuevoEst.getCorreo(), nuevoEst.getPass());
    	assertTrue(icis.iniciarSesionbool(nuevoEst.getNick(), nuevoEst.getPass()));
    }
    @Test
    public void f_setiniciarses2c() throws IniciarSesionException{
    	icis.iniciarSesion(nuevoDoc.getCorreo(), nuevoDoc.getPass());
    	assertTrue(icis.iniciarSesionbool(nuevoDoc.getNick(), nuevoDoc.getPass()));
    }
    @Test
    public void f_setconv(){
    	String a = icis.convertirEmalNick(nuevoEst.getCorreo());
    	assertTrue(a!=null);
    }
    @Test
    public void f_setconv2(){
    	String a = icis.convertirEmalNick(nuevoDoc.getCorreo());
		assertTrue(a!=null);
    }
    
    @Test
   	public void x_modUsuario() throws UsuarioRepetidoException  {
       	DtUsuario docenteEdit = new DtUsuario("DocTest","nombre","apellido","CorreoDoc",fechaActual,"pass2");
       	iCMod.modificarUsuario(docenteEdit);
       	DtUsuario docedited = iCMod.obtenerDt("DocTest");
       	assertTrue(
       	docenteEdit.getNombre().equals(docedited.getNombre()) &&
       	docenteEdit.getApellido().equals(docedited.getApellido())&&
       	docenteEdit.getCorreo().equals(docedited.getCorreo())&&
       	docenteEdit.getPass().equals(docedited.getPass())
       	);
       	
       	
   	}
       @Test
   	public void x_modUsuario2() throws UsuarioRepetidoException  {
       	DtEstudiante estEdit = new DtEstudiante("EstudianteTest","Random","TTT","CorreoEst",fechaActual,"pass2");
       	iCMod.modificarUsuario(estEdit);
       	DtUsuario docedited = iCMod.obtenerDt("EstudianteTest");
       	assertTrue(
       			estEdit.getNombre().equals(docedited.getNombre()) &&
       			estEdit.getApellido().equals(docedited.getApellido())&&
       			estEdit.getCorreo().equals(docedited.getCorreo())&&
       			estEdit.getPass().equals(docedited.getPass())
       	);
       	
       	
   	}
       @Test
      	public void x_cursoPF() throws CursoRepetidaException, CursoEnProgFormacionRepetidoException    {   	
       	iCMod.setCursoProgFormacion(nuevoCurso.getNombre(), nuevoPF.getNombre());
       	boolean existe=false;
   		for(String curso : iCMod.getCursoPorPF(nuevoPF.getNombre())) {
   			if (curso.equals(nuevoCurso.getNombre()))
   				existe = true;
   		}
   		assertTrue(existe);
       	
       }
       @Test
      	public void x_setins() {   	
       	List<String> a = iCMod.setInstitutos();
   		assertTrue(a!=null);
       }
       
       @Test
      	public void x_setcur() {   	
       	List<String> a = iCMod.setCursos(nuevoInsti.getNombre());
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setest() {   	
       	List<String> a = iCMod.setEstudiantes();
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setedi() {   	
       	List<String> a = iCMod.setEdiciones(nuevoCurso.getNombre());
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setcurpf() {   	
       	List<String> a = iCMod.getCursoPorPF(nuevoPF.getNombre());
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setpf() {   	
       	List<String> a = iCMod.getPf();
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setdtusu() {   	
       	DtUsuario a = iCMod.obtenerDt(nuevoDoc.getNick());
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setdtusu2() {   	
       	DtUsuario a = iCMod.obtenerDt(nuevoEst.getNick());
   		assertTrue(a!=null);
       }
       @Test
      	public void x_setdtpf() {   	
       	DtProgFormacion a = iCMod.obtenerDtPF(nuevoPF.getNombre());
   		assertTrue(a!=null);
       }
   
}
