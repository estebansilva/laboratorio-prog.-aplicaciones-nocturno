package datatypes;

public class DtInstituto {
	private String nombre;

	public DtInstituto() {
		super();
	}
	public DtInstituto(final String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
}