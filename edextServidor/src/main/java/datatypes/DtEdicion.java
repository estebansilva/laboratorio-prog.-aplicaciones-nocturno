package datatypes;

import java.util.Date;

public class DtEdicion {
	final private String nombre;
	final private Date fechaI;
	final private Date fechaF;
	final private int cupo;
	final private Date fechaPub;
	
	public DtEdicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub) {
		super();
		this.nombre = nombre;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
		this.cupo = cupo;
		this.fechaPub = fechaPub;
	}

	public String getNombre() {
		return nombre;
	}

	public Date getFechaI() {
		return fechaI;
	}

	public Date getFechaF() {
		return fechaF;
	}

	public int getCupo() {
		return cupo;
	}

	public Date getFechaPub() {
		return fechaPub;
	}
	
	
}
