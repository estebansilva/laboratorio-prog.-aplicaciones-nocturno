package datatypes;

import java.util.Date;

public class DtPF {
	final private String nombre;
	final private String descripcion;
	final private Date fechaI;
	final private Date fechaF;

	public DtPF(String nombre, String descripcion,  Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFechaI() {
		return fechaI;
	}

	public Date getFechaF() {
		return fechaF;
	}



}
