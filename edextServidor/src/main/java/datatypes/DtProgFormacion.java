package datatypes;

import java.util.Date;

public class DtProgFormacion {
	private String nombre;
	private String descrip;
	private Date fechaI;
	private Date fechaF;
	public DtProgFormacion(String nombre, String descrip, Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descrip = descrip;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}
	public String getNombre() {
		return nombre;
	}
	public String getDescrip() {
		return descrip;
	}
	public Date getFechaI() {
		return fechaI;
	}
	public Date getFechaF() {
		return fechaF;
	}
	
	
}
