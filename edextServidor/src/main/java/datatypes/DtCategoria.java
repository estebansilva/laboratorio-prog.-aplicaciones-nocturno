package datatypes;

public class DtCategoria {
	
		final private String nombre;

		public DtCategoria(String nombre) {
			super();
			this.nombre = nombre;
		}

		public String getNombre() {
			return nombre;
		}
	
}
