package datatypes;

import java.util.Date;

public class DtUsuario {
	final private String nick;
	final private String nombre;
	final private String apellido;
	final private String correo;
	final private Date fechaNac;
	final private String pass;
	
	public DtUsuario(String nick, String nombre, String apellido, String correo, Date fechaNac, String pass) {
		super();
		this.nick = nick;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fechaNac = fechaNac;
		this.pass= pass;
	}

	public String getNick() {
		return nick;
	}
	
	public String getPass() {
		return pass;
	}


	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public Date getFechaNac() {
		return fechaNac;
	}
	
	
}
