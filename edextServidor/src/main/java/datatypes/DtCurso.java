package datatypes;

import java.util.Calendar;
import java.util.Date;

public class DtCurso {
	private String nombre;
	private String descrip;
	private String duracion;
	private int cantHoras;
	private int creditos;
	private Date fechaR;
	private String url;
	
	public DtCurso() {
		super();
	}
	public DtCurso(final String nombre, final String descrip,final String duracion, final int cantHoras, final int creditos, final Date fechaR,
			final String url) {
		super();
		this.nombre = nombre;
		this.descrip = descrip;
		this.duracion = duracion;
		this.cantHoras = cantHoras;
		this.creditos = creditos;
		this.fechaR = fechaR;
		this.url = url;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescrip() {
		return descrip;
	}

	public String getDuracion() {
		return duracion;
	}

	public int getCantHoras() {
		return cantHoras;
	}

	public int getCreditos() {
		return creditos;
	}

	public Date getFechaR() {
		return fechaR;
	}

	public String getUrl() {
		return url;
	}
	
}
