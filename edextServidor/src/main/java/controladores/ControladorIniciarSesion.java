package controladores;

import excepciones.IniciarSesionException;
import excepciones.UsuarioRepetidoException;
import interfaces.IControladorIniciarSesion;
import logica.Docente;
import logica.Estudiante;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;

public class ControladorIniciarSesion implements IControladorIniciarSesion {

	@Override
	public boolean existeUsuario(String nick) {
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if(mD.existeDocente(nick) || mD.existeCorreo(nick)) {
			return true;
		}
		if(mE.existeEstudiante(nick) || mE.existeCorreo(nick)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean iniciarSesionbool(String nick, String pass) {
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if(mD.existeDocente(nick)) {
			Docente d=mD.buscarDocente(nick);
			if(d.getPass().equals(pass))
				return true;
		}else if (mD.existeCorreo(nick)) {
			Docente d=mD.buscarDocente(mD.obtenerDocenteEmail(nick));
			if(d.getPass().equals(pass))
				return true;
		}
		if(mE.existeEstudiante(nick)) {
			Estudiante e=mE.buscarEstudiante(nick);
			if(e.getPass().equals(pass))
				return true;
		}else if(mE.existeCorreo(nick)) {
			Estudiante e=mE.buscarEstudiante(mE.obtenerEstudianteEmail(nick));
			if(e.getPass().equals(pass))
				return true;
		}
		return false;
		
	}

	@Override
	public void iniciarSesion(String nick, String pass) throws IniciarSesionException {
		// TODO Auto-generated method stub
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if(mD.existeDocente(nick)) {
			Docente d=mD.buscarDocente(nick);
			if(!d.getPass().equals(pass))
				throw new IniciarSesionException("La contraseņa no es correcta");
		}else if (mD.existeCorreo(nick)) {
			Docente d=mD.buscarDocente(mD.obtenerDocenteEmail(nick));
			if(!d.getPass().equals(pass))
				throw new IniciarSesionException("La contraseņa no es correcta");
		}
		if(mE.existeEstudiante(nick)) {
			Estudiante e=mE.buscarEstudiante(nick);
			if(!e.getPass().equals(pass))
				throw new IniciarSesionException("La contraseņa no es correcta");
		}else if(mE.existeCorreo(nick)) {
			Estudiante e=mE.buscarEstudiante(mE.obtenerEstudianteEmail(nick));
			if(!e.getPass().equals(pass))
				throw new IniciarSesionException("La contraseņa no es correcta");
		}
		
	}

	@Override
	public String convertirEmalNick(String s) {
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if(mD.existeCorreo(s)) {
			return mD.obtenerDocenteEmail(s);
		}else {
			return mE.obtenerEstudianteEmail(s);
		}
	}

}
