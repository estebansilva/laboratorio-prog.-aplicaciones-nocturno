package controladores;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import datatypes.DtProgFormacion;
import datatypes.DtUsuario;
import excepciones.CursoEnProgFormacionRepetidoException;
import excepciones.EdicionRepetidaException;
import excepciones.InstitutoRepetidaException;
import interfaces.IControladorModificarUsuario;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.Instituto;
import logica.ProgFormacion;
import manejadores.ManejadorCurso;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorInstituto;
import manejadores.ManejadorProgFormacion;
import manejadores.ManejadorProgFormacion;
import utils.Conexion;

public class ControladorModificarUsuario implements IControladorModificarUsuario{

	public ControladorModificarUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void modificarUsuario(DtUsuario dtusuario) {
		ManejadorDocente mD= ManejadorDocente.getInstancia();
		ManejadorEstudiante mE= ManejadorEstudiante.getInstancia();
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
		if(mD.buscarDocente(dtusuario.getNick())!= null) {
			Docente docente = mD.buscarDocente(dtusuario.getNick());
			em.persist(docente);
			docente.setNombre(dtusuario.getNombre());
			docente.setApellido(dtusuario.getApellido());
			docente.setFechaNac(dtusuario.getFechaNac());
			docente.setPass(dtusuario.getPass());
			em.getTransaction().commit();
		}
		else {
			Estudiante estudiante = mE.buscarEstudiante2(dtusuario.getNick());
			em.persist(estudiante);
			estudiante.setNombre(dtusuario.getNombre());
			estudiante.setApellido(dtusuario.getApellido());
			estudiante.setFechaNac(dtusuario.getFechaNac());
			estudiante.setPass(dtusuario.getPass());
			em.getTransaction().commit();
		}
		}
	
	@Override
	public DtUsuario obtenerDt(String nick) {
		ManejadorDocente mD= ManejadorDocente.getInstancia();
		ManejadorEstudiante mE= ManejadorEstudiante.getInstancia();
		DtUsuario dtusuario;
		if(mD.existeDocente(nick)) {
			Docente docente = mD.buscarDocente(nick);
			dtusuario = new DtUsuario(docente.getNick(),docente.getNombre(),docente.getApellido(),docente.getCorreo(),docente.getFechaNac(),docente.getPass());
		}
		else {
			Estudiante estudiante = mE.buscarEstudiante2(nick);
			dtusuario = new DtUsuario(estudiante.getNick(),estudiante.getNombre(),estudiante.getApellido(),estudiante.getCorreo(),estudiante.getFechaNac(),estudiante.getPass());
		}
		return dtusuario;
	}

	public void setCursoProgFormacion( String nombrePF, String nombreCurso) throws CursoEnProgFormacionRepetidoException {
		boolean existe = false;
		ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
		ManejadorCurso 	mC = ManejadorCurso.getInstancia();
		ProgFormacion  pf = mPF.buscarProgFormacion(nombrePF);
		List<Curso> listaC = pf.getCurso();
		Curso  curso = mC.buscarCurso(nombreCurso);
	
		for(Curso c:listaC) {
			if ( c.equals(curso)) {
				existe = true;
				throw new CursoEnProgFormacionRepetidoException("El Curso " +  nombreCurso + " ya existe en el Programa de Formacion");
			}
		} 
		
		if (existe == false) {
			mPF.agregarCurso(pf, curso);
		}	
	}
	
	public List<String> setInstitutos(){
		ManejadorInstituto mi = ManejadorInstituto.getInstancia();
		return mi.obtenerInstitutos();
	}
	
	@SuppressWarnings("null")
	public List<String> setCursos(String s){
		ManejadorInstituto mI = ManejadorInstituto.getInstancia();
		Instituto i = mI.buscarInstituto(s);
		List<Curso> cs = i.getCursos();
		List<String> result = new ArrayList<String>();;
		for(Curso c:cs) {
			result.add(c.getNombre());
		} 
		return result;
	}

	@Override
	public List<String> setEstudiantes() {
		// TODO Auto-generated method stub
		ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
		return me.obtenerEstudiantes();
	}
	@Override
	public List<String> setDocente() {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		return md.obtenerDocentes();
	}

	@Override
	public List<String> setEdiciones(String s) {
		// TODO Auto-generated method stub
		ManejadorCurso mI = ManejadorCurso.getInstancia();
		Curso i = mI.buscarCurso(s);
		List<Edicion> ed = i.getEdicion();
		List<String> result = new ArrayList<String>();
		for(Edicion e:ed) {
			result.add(e.getNombre());
		} 
		return result;
	}
	@Override
	public List<String> getPf() {
		// TODO Auto-generated method stub
		ManejadorProgFormacion mpf = ManejadorProgFormacion.getInstancia();
		return mpf.obtenerProgFormacion();
	}

	@Override
	public List<String> getCursoPorPF(String pf) {
		ManejadorProgFormacion mpf = ManejadorProgFormacion.getInstancia();
		ProgFormacion progf = mpf.buscarProgFormacion(pf);
		List<Curso> cursos = progf.getCurso();
		List<String> result = new ArrayList<String>();
		for(Curso c:cursos) {
			result.add(c.getNombre());
		}
		return result;
	}
	@Override
	public DtProgFormacion obtenerDtPF(String pf) {
		ManejadorProgFormacion mpf = ManejadorProgFormacion.getInstancia();
		return mpf.obtenerDt(pf);
	}
	
}
