package controladores;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import datatypes.DtCurso;
import datatypes.DtDocente;
import datatypes.DtInstituto;
import datatypes.DtPF;
import datatypes.DtUsuario;
import datatypes.DtEdicion;
import datatypes.DtEstudiante;
import datatypes.Estado;
import excepciones.CursoRepetidaException;
import excepciones.CursoVacioException;
//import excepciones.CursoRepetidaException;
import excepciones.InstitutoRepetidaException;
import excepciones.InstitutoVacioException;
import excepciones.EdicionRepetidaException;
import excepciones.ProgFormacionRepetidoException;
import excepciones.UsuarioRepetidoException;
import excepciones.categoriaRepetidaException;
import excepciones.inscripcionEdicionRepetidaException;
import excepciones.inscripcionPFormacionRepetidaException;
import interfaces.IControladorAlta;
import logica.Categoria;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.InscripcionEd;
import logica.Instituto;
import logica.ProgFormacion;
import logica.Usuario;
import manejadores.ManejadorInstituto;
import manejadores.ManejadorEdicion;
import manejadores.ManejadorCategoria;
import manejadores.ManejadorCurso;
import manejadores.ManejadorDocente;
import manejadores.ManejadorEstudiante;
import manejadores.ManejadorInscripcionEd;
import manejadores.ManejadorProgFormacion;
import utils.Conexion;

public class ControladorAlta implements IControladorAlta{
	
	public ControladorAlta() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@Override
	public void ingresarInstituto(DtInstituto dtin) throws InstitutoRepetidaException {
		ManejadorInstituto mI = ManejadorInstituto.getInstancia();
		Instituto ins = mI.buscarInstituto(dtin.getNombre());
		if (ins != null) 
			throw new InstitutoRepetidaException("El Instituto " + dtin.getNombre() + " ya existe");
		ins = new Instituto(dtin.getNombre());
		mI.agregarInstituto(ins);
	}
	
	public void ingresarDocente(DtUsuario dtusuario, DtInstituto dtinstituto) throws UsuarioRepetidoException {
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if((mD.buscarDocente(dtusuario.getNick()) != null) ||(mE.buscarEstudiante2(dtusuario.getNick()) != null))
			throw new UsuarioRepetidoException("El usuario " + dtusuario.getNick() + " ya existe");
		if(mD.existeCorreo(dtusuario.getCorreo()) || (mE.existeCorreo(dtusuario.getCorreo())))
			throw new UsuarioRepetidoException("El correo " + dtusuario.getCorreo() + " ya existe");
	    ManejadorInstituto mI = ManejadorInstituto.getInstancia();	
	    Instituto ins = mI.buscarInstituto(dtinstituto.getNombre());
		Docente docente = new Docente(dtusuario.getNick(),dtusuario.getNombre(),dtusuario.getApellido(),dtusuario.getCorreo(),dtusuario.getFechaNac(), ins,dtusuario.getPass());
		docente.setInstituto(ins);
		mD.agregarDocente(docente);
		ins.agregarDocente(docente);
	}
	
	public void ingresarEstudiante(DtUsuario dtusuario)throws UsuarioRepetidoException{
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if((mD.buscarDocente(dtusuario.getNick()) != null)||(mE.buscarEstudiante2(dtusuario.getNick()) != null))
			throw new UsuarioRepetidoException("El usuario " + dtusuario.getNick() + " ya existe");
		if(mD.existeCorreo(dtusuario.getCorreo()) || (mE.existeCorreo(dtusuario.getCorreo())))
			throw new UsuarioRepetidoException("El correo " + dtusuario.getCorreo() + " ya existe");
		Estudiante estudiante = new Estudiante(dtusuario.getNick(),dtusuario.getNombre(),dtusuario.getApellido(),dtusuario.getCorreo(),dtusuario.getFechaNac(),dtusuario.getPass());
		mE.agregarEstudiante(estudiante);
	} 

	//@Override
	public void ingresarCurso(DtCurso dtc, DtInstituto dti, List<String> ls, List<String> categoria) throws CursoRepetidaException {
		// TODO Auto-generated method stub
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		ManejadorCategoria mcat= ManejadorCategoria.getInstancia();
		if(mc.buscarCurso(dtc.getNombre()) != null) {
			throw new CursoRepetidaException("El Curso " + dtc.getNombre() + " ya existe");
		}else {
			ManejadorInstituto mi = ManejadorInstituto.getInstancia();
			Instituto i = mi.buscarInstituto(dti.getNombre());
			
			List<Categoria> listacat= new ArrayList<Categoria>();
			if(!categoria.isEmpty()) {
				for(String s:categoria) {
					Categoria cat = mcat.buscarCategorias(s);
					if(cat!=null)
						listacat.add(cat);
				}
			}
			
			
			Curso c = new Curso(dtc.getNombre(), dtc.getDescrip() , dtc.getDuracion() , dtc.getCantHoras(), dtc.getCreditos(),dtc.getFechaR(),dtc.getUrl(), i, listacat);
			Conexion conexion= Conexion.getInstancia();
	        EntityManager em =conexion.getEntityManager();
			if(ls != null) {
				for(String s:ls) {
					c.agregarPrevia(mc.buscarCurso(s));
				}
			}
			
			
			mc.agregarCurso(c);
			
			em.getTransaction().begin();
	        em.persist(i);
	        i.agregarCurso(c);
	        em.getTransaction().commit();
		
	        if(categoria != null) {
				for(Categoria cate:listacat){
					em.getTransaction().begin();
			        em.persist(cate);
			        cate.setCurso(c);
			        em.getTransaction().commit();
				}
			}
		}
	}


	public void IngresarProgramaF(DtPF dtpf) throws ProgFormacionRepetidoException {
		ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
		if (mPF.buscarProgFormacion(dtpf.getNombre()) != null)
			throw new ProgFormacionRepetidoException("El Programa de formacin " + dtpf.getNombre() + " ya existe");	
		ProgFormacion progformacion = new ProgFormacion(dtpf.getNombre(), dtpf.getDescripcion(), dtpf.getFechaI(), dtpf.getFechaF());
		mPF.agregarProgFormacion(progformacion);
		
	}
	
	
	public void ingresarEdicion(DtEdicion dtEdi, List<String> docente, String curso) throws EdicionRepetidaException {
		ManejadorEdicion mEC = ManejadorEdicion.getInstancia();
		Edicion edic = mEC.buscarEdicion(dtEdi.getNombre());
		if (edic != null) 
			throw new EdicionRepetidaException("La edicion de curso " + dtEdi.getNombre() + " ya existe");
		edic = new Edicion(dtEdi.getNombre(), dtEdi.getFechaI(),dtEdi.getFechaF(),dtEdi.getCupo(),dtEdi.getFechaPub());
			mEC.agregarEdicion(edic);
			ManejadorCurso mC= ManejadorCurso.getInstancia();
			Curso c= mC.buscarCurso(curso);
			ManejadorDocente md = ManejadorDocente.getInstancia();
			for(String doc:docente) {
				Conexion conexion= Conexion.getInstancia();
		        EntityManager em =conexion.getEntityManager();
				Docente docenteauxiliar = md.buscarDocente(doc);
				em.getTransaction().begin();
		        em.persist(docenteauxiliar);
		        docenteauxiliar.agregarEdicion(edic);
		        em.getTransaction().commit();
		
			
		}
			
		Conexion conexion= Conexion.getInstancia();
	    EntityManager em =conexion.getEntityManager();
	    em.getTransaction().begin();
	    em.persist(c);
		c.agregarEdicion(edic);
		em.getTransaction().commit();
	}
	

	
	public List<String> setInstitutos(){
		ManejadorInstituto mi = ManejadorInstituto.getInstancia();
		return mi.obtenerInstitutos();
	}
	
	
	public List<String> setTodosLosCursos(){
		ManejadorCurso mC = ManejadorCurso.getInstancia();
		return mC.obtenerCurso();
	}
	
	@SuppressWarnings("null")
	public List<String> setCursos(String s){
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		return mc.obtenerCursoInstituto(s);
	}
	
	@SuppressWarnings("null")
	public List<String> getDocentes(String s){
		ManejadorDocente md = ManejadorDocente.getInstancia();
		List<String> resultd = md.obtenerDocentesInstituto(s);
		return resultd;
	}
	


	@Override
	public List<String> setEstudiantes() {
		// TODO Auto-generated method stub
		ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
		return me.obtenerEstudiantes();
	}

	@Override
	public List<String> setEdiciones(String s) {
		// TODO Auto-generated method stub
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		Curso c = mc.buscarCurso(s);
		List<Edicion> ed = c.getEdicion();
		List<String> result2 = new ArrayList<String>();
		for(Edicion e:ed) {
			result2.add(e.getNombre());
		} 
		return result2;
	}
	
	@Override
	public DtEdicion darInfoEdicion(String s) {
		// TODO Auto-generated method stub
		
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion edic = mE.buscarEdicion(s);
		DtEdicion dtedic = new DtEdicion(edic.getNombre(), edic.getFechaI(), edic.getFechaF(),edic.getCupo(), edic.getFechaPub());
		return dtedic;
	}

	@Override
	public void inscripcionEdicionCurso(String ins, String cur, String edi, String est)
			throws inscripcionEdicionRepetidaException {
		// TODO Auto-generated method stub
		ManejadorEdicion medi = ManejadorEdicion.getInstancia();
		ManejadorEstudiante mest = ManejadorEstudiante.getInstancia();
		ManejadorInscripcionEd mi = ManejadorInscripcionEd.getInstancia();
		Estudiante estu = mest.buscarEstudiante2(est);
		Edicion edicion = medi.buscarEdicion(edi);
		InscripcionEd i = new InscripcionEd(estu, edicion);
		Date comp = new Date();
		if(mi.existeInscripcionEd(edi, est)|| (edicion.getFechaI().before(comp)) || (edicion.getCupo() == 0))
			throw new inscripcionEdicionRepetidaException("La Inscripcion a la Edicion " + edi + " y estudiante "+ est + " ya existe,  la fecha expiro o no hay cupo");
		
		if(edicion.getCupo() > 0) {
			medi.restarCupo(edi);
			Conexion conexion= Conexion.getInstancia();
		    EntityManager em =conexion.getEntityManager();
		    em.getTransaction().begin();
		    em.persist(edicion);
		    edicion.agregarInscripcionEd(i);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
		    em.persist(estu);
		    estu.agregarInscripcionEd(i);
			em.getTransaction().commit();

			mi.agregarInscripcionEd(i);
		}
	}
	
	@Override
	public String[] getInfoCurso(String s) {
		//String s es el nombre de la clase que quiero buscar
		ManejadorCurso mcur = ManejadorCurso.getInstancia();		
		return mcur.infoCurso(s);
	}
	
	public List<String> getPFromacion(String cursoElegido){
		ManejadorProgFormacion mPF = ManejadorProgFormacion.getInstancia();
		ManejadorCurso mcur = ManejadorCurso.getInstancia();
		List<String> progFormacionExiste = new ArrayList<String>();
		List<String> listapf = mPF.obtenerProgFormacion();
		Curso cursoElegidoInstancia = mcur.buscarCurso(cursoElegido);
		 
		 for(String pf:listapf) {
			ProgFormacion instanciaPF =  mPF.buscarProgFormacion(pf);
			if ( instanciaPF.existeCurso(cursoElegidoInstancia) )
				progFormacionExiste.add((String)instanciaPF.getNombre());
		 }
		return progFormacionExiste; 
	}

	@Override
	public boolean esDocnete(String s) {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		if(md.existeDocente(s))
			return true;
		return false;
	}

	@Override
	public List<String> getUsuarios() {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		List<String> d = md.obtenerDocentes();
		List<String> result = new ArrayList();
		result.addAll(setEstudiantes());
		result.addAll(d);
		return result;
	}

	@Override
	public String getDocneteInstituto(String s) {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		Docente d = md.buscarDocente(s);
		return d.getInstituto().getNombre();
	}

	@Override
	public DtDocente getInfoDocente(String s) {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		Docente d = md.buscarDocente(s);
		DtDocente dt = new DtDocente(d.getNick(), d.getNombre(), d.getApellido(), d.getCorreo(), d.getFechaNac(),d.getPass());
		return dt;
	}

	@Override
	public DtEstudiante getInfoEstudiante(String s) {
		// TODO Auto-generated method stub
		ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
		Estudiante e = me.buscarEstudiante(s);
		DtEstudiante dt = new DtEstudiante(e.getNick(), e.getNombre(), e.getApellido(), e.getCorreo(), e.getFechaNac(),e.getPass());
		return dt;
	}

	@Override
	public List<String> getEdicionDcocente(String s) {
		// TODO Auto-generated method stub
		ManejadorDocente md = ManejadorDocente.getInstancia();
		Docente d = md.buscarDocente(s);
		List<String> result = new ArrayList();
		List<Edicion> ed = d.getEdicion();
		for(Edicion e:ed) {
			result.add(e.getNombre());
		} 
		return result;
	}

	@Override
	public List<String> getEdicionEstudiante(String s) {
		// TODO Auto-generated method stub
		ManejadorEstudiante me = ManejadorEstudiante.getInstancia();
		Estudiante e = me.buscarEstudiante(s);
		List<String> result = new ArrayList();
		List<InscripcionEd> ie = e.getInscripcionEd();
		for(InscripcionEd i:ie) {
			result.add(i.getEdicion().getNombre());
		} 
		return result;
	}

	@Override
	public List<String> getPreviaCurso(String s) {
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		Curso c = mc.buscarCurso(s);
		List<Curso> cursos = c.getPrevias();
		List<String> result2 = new ArrayList<String>();;
		for(Curso cu:cursos) {
			result2.add(cu.getNombre());
		} 
		return result2;
	}

	@Override
	public List<String> getEstudianteEnEdicion(String s) {
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion ed = mE.buscarEdicion(s);
		List<String> result = new ArrayList();
		List<InscripcionEd> ie = ed.getInscripcionEd();
		for(InscripcionEd i:ie) {
			result.add(i.getEstudiante().getNick());
		} 
		return result;
	}

	@Override
	public List<String> getDocenteEdicion(String s) {
		  ManejadorDocente md = ManejadorDocente.getInstancia();
	        ManejadorEdicion me = ManejadorEdicion.getInstancia();
	        Edicion e = me.buscarEdicion(s);
	        List<String> d = md.obtenerDocentes();
	        List<String> result = new ArrayList();
	        for(String doc:d) {
	            Docente docente = md.buscarDocente(doc);
	            if(docente.existeEdicion(e))
	                result.add(doc);
	        } 
	        return result;
	}

	@Override
	public void ingresarCategoria(String nombre) throws categoriaRepetidaException {
		ManejadorCategoria mC= ManejadorCategoria.getInstancia();
		if(!mC.existeCategoria(nombre))
			throw new categoriaRepetidaException("La Categor�a" + nombre + "solicitada ya existe.");
		Categoria c = new Categoria(nombre);
		mC.agregarCategoria(c);
		
	}
	
	@Override
	public List<String> listaDeCategorias(){
		ManejadorCategoria mc= ManejadorCategoria.getInstancia();
		return mc.obtenerCategorias();
	}
	
	@Override
	public boolean existenickemail(String n, String e) {
		ManejadorDocente mD = ManejadorDocente.getInstancia();
		ManejadorEstudiante mE = ManejadorEstudiante.getInstancia();
		if((mD.buscarDocente(n) != null) ||(mE.buscarEstudiante2(n) != null) ||(mD.existeCorreo(e) || (mE.existeCorreo(e))))
			return true;
		return false;
	}

	@Override
	public boolean existeCategoria(String S) {
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		if(!mCat.existeCategoria(S))
			return true;
		return false;
	}

	@Override
	public List<String> cursosporCategoria(String c) {
		
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		Categoria cat = mCat.buscarCategorias(c);
		List<String> resultado = new ArrayList<String>(); 
		List<Curso> cursos= cat.getCurso();
		for(Curso cur:cursos) {
			resultado.add(cur.getNombre());
		}
		return resultado;
	}
	
	public boolean edicionEsVigente(String s) {		
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion edic = mE.buscarEdicion(s);
		Date actual = new Date();
		return (edic.getFechaI().before(actual)) && (actual.before(edic.getFechaF()));		
	}
	
	
	public String[] getInfoEdicion(String s) {
		//String s es el nombre de la clase que quiero buscar
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion edic = mE.buscarEdicion(s);
		return mE.infoEdicion(s);
	}


	@Override
	public boolean esposibleinscribirse(String edi, String est) {
		ManejadorEdicion medi = ManejadorEdicion.getInstancia();
		ManejadorEstudiante mest = ManejadorEstudiante.getInstancia();
		ManejadorInscripcionEd mi = ManejadorInscripcionEd.getInstancia();
		Estudiante estu = mest.buscarEstudiante2(est);
		Edicion edicion = medi.buscarEdicion(edi);
		InscripcionEd i = new InscripcionEd(estu, edicion);
		Date comp = new Date();
		if(mi.existeInscripcionEd(edi, est)|| (edicion.getFechaI().before(comp)) || (edicion.getCupo() <= 0))
			return false;
		else return true;
	}

	@Override
	public DtEdicion edicionVigente(String c) {
		ManejadorCurso mC = ManejadorCurso.getInstancia();
		Curso curso = mC.buscarCurso(c);
		List<Edicion> ediciones = curso.getEdicion();
		Date fecha = new Date();
		for(Edicion ed:ediciones) {
			if(ed.getFechaI().after(fecha)){
				DtEdicion dte = new DtEdicion(ed.getNombre(), ed.getFechaI(), ed.getFechaF(), ed.getCupo(), ed.getFechaPub());
				return dte;
			}
		}
		return null;
	}	

	
	public String getEstadoInscripcionEdicion(String estudiante, String edicion) {
		String estado = "";
		ManejadorInscripcionEd mIE = ManejadorInscripcionEd.getInstancia();
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion ed = mE.buscarEdicion(edicion);
		List<InscripcionEd> ie = ed.getInscripcionEd();//traigo lista con todas las inscripciones
		for(InscripcionEd i:ie) {
			if (i.getEstudiante().getNick().equals(estudiante)) {
				estado = i.getEstado().toString();
			}
		}	
		return estado;
	}
	
	public void changeEstadoInscripcionEdicion(String estudiante, String edicion, boolean aceptado) {
		ManejadorInscripcionEd mIE = ManejadorInscripcionEd.getInstancia();
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion ed = mE.buscarEdicion(edicion);
		List<InscripcionEd> ie = ed.getInscripcionEd();//traigo lista con todas las inscripciones
		Conexion conexion=Conexion.getInstancia();
		EntityManager em =conexion.getEntityManager();
        
		for(InscripcionEd i:ie) {
			em.getTransaction().begin();
			em.persist(i);
			if (i.getEstudiante().getNick().equals(estudiante)) {
				if (aceptado) {
					i.setEstado(Estado.Aceptado);
				}else {
					i.setEstado(Estado.Rechazado);
				}
			}
			em.getTransaction().commit();
		}
	}
	
	public List<String[]> getEstudianteEnEdicionAceptado(String s) {
		ManejadorEdicion mE = ManejadorEdicion.getInstancia();
		Edicion ed = mE.buscarEdicion(s);
		List<String[]> result = new ArrayList();
		List<InscripcionEd> ie = ed.getInscripcionEd();
		for(InscripcionEd i:ie) {
			if (i.getEstado().equals(Estado.Aceptado))	{
				String[] edicionRet = {
						i.getEstudiante().getNick(),
						i.getEstado().toString()
						};
				result.add(edicionRet);
			}
		}  
		return result;
	}

	@Override
	public boolean existeCurso(String S) {
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		return !mc.existeCurso(S);
	}

	@Override
	public List<String> obtenerCategoriadeCurso(String c) {
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		Curso curso = mc.buscarCurso(c);
		List<Categoria> listacat = curso.getCategoria();
		List<String> resultado = new ArrayList<String>();
		for(Categoria cat:listacat) {
			resultado.add(cat.getNombre());
			}
		return resultado;
	}

	@Override
	public List<String> buscaNombre(String c) {
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		return mc.obtenerCursoPorBusqueda(c);
	}

	@Override
	public List<String> buscaFecha(String c) {
		ManejadorCurso mc = ManejadorCurso.getInstancia();
		return mc.obtenerCursoPorFecha(c);
	}

}

