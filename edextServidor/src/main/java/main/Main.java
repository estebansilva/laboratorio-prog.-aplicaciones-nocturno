package main;

import javax.persistence.EntityManager;

import logica.Instituto;
import utils.Conexion;

public class Main {

	public static void main(String[] args) {
		Instituto ins = new Instituto("Inco");
		Conexion conexion=Conexion.getInstancia();
        EntityManager em =conexion.getEntityManager();
        em.getTransaction().begin();
        em.persist(ins);
        em.getTransaction().commit();
	}

}
