package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "ProgFormacion")
public class ProgFormacion {
	@Id		//Etiqueta de la primary-key de la tabla ProFormacion en este caso
	private String nombre;
	private String descrip;
	private Date fechaI;
	private Date fechaF;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Curso> curso = new ArrayList<>();
	@OneToMany(mappedBy="progFormacion",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionPf> inscripcionPf = new ArrayList<>();
	
	
	public ProgFormacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProgFormacion(String nombre, String desc, Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descrip = desc;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public Date getFechaI() {
		return fechaI;
	}

	public void setFechaI(Date fechaI) {
		this.fechaI = fechaI;
	}

	public Date getFechaF() {
		return fechaF;
	}

	public void setFechaF(Date fechaF) {
		this.fechaF = fechaF;
	}

	public List<Curso> getCurso() {
		return curso;
	}

	public void setCurso(List<Curso> curso) {
		this.curso = curso;
	}
	
	public void agregarPrevia(Curso curso) {
		if (this.curso.indexOf(curso) == -1)
				this.curso.add(curso);
	}
	
}
