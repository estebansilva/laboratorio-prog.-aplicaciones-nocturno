package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "InscripcionPf") 
@IdClass(InscripcionPfId.class)
public class InscripcionPf {
	@Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private Estudiante estudiante;

    @Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private ProgFormacion progFormacion;
	private Date fecha;
	
	
	public InscripcionPf() {
		super();
		// TODO Auto-generated constructor stub
	}


	public InscripcionPf(Estudiante estudiante, ProgFormacion progFormacion, Date fecha) {
		super();
		this.estudiante = estudiante;
		this.progFormacion = progFormacion;
		this.fecha = fecha;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Estudiante getEstudiante() {
		return estudiante;
	}


	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}


	public ProgFormacion getProgFormacion() {
		return progFormacion;
	}


	public void setProgFormacion(ProgFormacion progFormacion) {
		this.progFormacion = progFormacion;
	}
	
	
	
}
