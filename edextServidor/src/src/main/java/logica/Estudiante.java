package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
@Entity
public class Estudiante extends Usuario {
	
	@OneToMany(mappedBy="estudiante",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEd> inscripcionEd = new ArrayList<>();
	
	@OneToMany(mappedBy="estudiante",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionPf> inscripcionPf = new ArrayList<>();
	public Estudiante() {	
		super();
		// TODO Auto-generated constructor stub
	}

	public Estudiante(String nick, String nombre, String apellido, String correo, Date fechaNac) {
		super(nick, nombre, apellido, correo, fechaNac);
		// TODO Auto-generated constructor stub
	}

	
}
