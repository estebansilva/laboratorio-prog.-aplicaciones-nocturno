package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "Instituto")
public class Instituto {
	@Id  //Etiqueta de la primary-key de la tabla Instituto en este caso
	private String nombre;
	@OneToMany(mappedBy="instituto",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Curso> cursos = new ArrayList<>();
	
	public Instituto() {
		super();
	}
	
	public Instituto(String nombre) {
		super();
		this.nombre = nombre;
		this.cursos = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public void agregarCurso(Curso curso) {
		if (this.cursos.indexOf(curso) == -1)
				this.cursos.add(curso);
	}
	
}
