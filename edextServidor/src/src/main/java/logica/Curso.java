package logica;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Curso")
public class Curso {
	@Id     //Etiqueta de la primary-key de la tabla Curso en este caso
	private String nombre;
	private String descrip;
	private String duracion;
	private LocalTime cantHoras;
	private int creditos;
	private Date fechaR;
	private String url;
    @ManyToOne
	private Instituto instituto;
    @OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
    private List<Edicion> edicion = new ArrayList<>();
    @ManyToMany
    private List<Curso> previas = new ArrayList<>();
	public Curso() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Curso(String nombre, String desc, String duracion, LocalTime cantHoras,  int creditos, Date fechaR,
			String url, Instituto instituto) {
		super();
		this.nombre = nombre;
		this.descrip = desc;
		this.duracion = duracion;
		this.cantHoras = cantHoras;
		this.creditos = creditos;
		this.fechaR = fechaR;
		this.url = url;
		this.instituto = instituto;
		this.edicion = null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public LocalTime getCantHoras() {
		return cantHoras;
	}

	public void setCantHoras(LocalTime cantHoras) {
		this.cantHoras = cantHoras;
	}

	public int getCreditos() {
		return creditos;
	}

	public void setCreditos(int creditos) {
		this.creditos = creditos;
	}

	public Date getFechaR() {
		return fechaR;
	}

	public void setFechaR(Date fechaR) {
		this.fechaR = fechaR;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Instituto getInstituto() {
		return instituto;
	}

	public void setInstituto(Instituto instituto) {
		this.instituto = instituto;
	}
	
	public List<Edicion> getEdicion() {
		return edicion;
	}

	public void setEdicion(List<Edicion> edicion) {
		this.edicion = edicion;
	}

	public void agregarEdicion(Edicion edicion) {
		if (this.edicion.indexOf(edicion) == -1)
				this.edicion.add(edicion);
	}

	public List<Curso> getPrevias() {
		return previas;
	}

	public void setPrevias(List<Curso> previas) {
		this.previas = previas;
	}
	
	public void agregarPrevia(Curso curso) {
		if (this.previas.indexOf(curso) == -1)
				this.previas.add(curso);
	}
	
	
}
