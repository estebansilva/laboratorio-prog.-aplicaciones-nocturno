package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name = "InscripcionEd") 
@IdClass(InscripcionEdId.class)
public class InscripcionEd {
	@Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private Estudiante estudiante;

    @Id
    @ManyToOne
    @JoinColumn(
            insertable=false,
            updatable=false
    )
    private Edicion edicion;
	private Date fecha;

	
	public InscripcionEd() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public InscripcionEd(Estudiante estudiante, Edicion edicion, Date fecha) {
		super();
		this.estudiante = estudiante;
		this.edicion = edicion;
		this.fecha = fecha;
	}
	public InscripcionEd(Estudiante estudiante, Edicion edicion) {
		super();
		this.estudiante = estudiante;
		this.edicion = edicion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public Edicion getEdicion() {
		return edicion;
	}

	public void setEdicion(Edicion edicion) {
		this.edicion = edicion;
	}
	
}
