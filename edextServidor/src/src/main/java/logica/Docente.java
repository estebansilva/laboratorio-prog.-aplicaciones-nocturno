package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
@Entity
public class Docente extends Usuario{
	@ManyToOne
	private Instituto instituto;
	@ManyToMany(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Edicion> edicion = new ArrayList<>();
	public Docente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Docente(String nick, String nombre, String apellido, String correo, Date fechaNac, Instituto instituto) {
		super(nick, nombre, apellido, correo, fechaNac);
		// TODO Auto-generated constructor stub
	}

	public Instituto getInstituto() {
		return instituto;
	}

	public void setInstituto(Instituto instituto) {
		this.instituto = instituto;
	}

	public List<Edicion> getEdicion() {
		return edicion;
	}

	public void setEdicion(List<Edicion> edicion) {
		this.edicion = edicion;
	}
	
	public void agregarEdicion(Edicion edicion) {
		if (this.edicion.indexOf(edicion) == -1)
			this.edicion.add(edicion);
	}
}
