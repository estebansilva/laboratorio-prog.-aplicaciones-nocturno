
<%@ page import= "java.util.ArrayList" %>
<%@ page import= "java.util.List" %>




<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/header.jsp" %>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <link type="text/css" rel="stylesheet" href='css\component-chosen.css'>
    <link type="text/css" rel="stylesheet" href='css\component-chosen.min.css'>
    <link type="text/css" rel="stylesheet" href='css\bootstrap.min.css'>
    <title>Inscripción a Edición de Curso</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">

  </head>
  <body>
  	

    <form action="Inscripción Edición de Curso" method="post">
    
    <script type = "text/javascript" src = 'js\curso.js' ></script> 
    <script type = "text/javascript" src = 'js\edicion.js' ></script>
    <script src="js\jquery-3.4.1.min.js" ></script>
    <script src="js\chosen.jquery.js" type="text/javascript"></script>
    <%
	IControladorAlta icon = fabrica.getIControladorAlta();
	List<String> instituto = icon.setInstitutos();%>
    
    
  <div class="form-group">
        <label for="Ins">Instituto</label>                                                 
            <select name="Ins" id="Ins"  data-placeholder="- Seleccione Instituto -"
             class="form-control chosenInstituto" onchange="changeCurso(this.id, 'curso')"
             value= "typeof Ins != 'undefined'? Ins : ''  %>">>
              <option value=""></option>
              <%if( instituto!= null){ %>
			<%for(String str:instituto){ %>
				<option value="<%=str%>"><%=str%></option>
			<% }%>
		<%}%>
              
            </select>
      </div>
 </div>
      <div class="form-group">
        <label for="Curso">Curso</label>
          <select id="curso" name="Curso" data-placeholder="- Seleccione Curso -"

           >
            <option value=""></option>
          </select>
      </div>
      
      <script>
        //chosen institución
        $('.chosenInstituto, .chosenCurso').chosen({no_results_text: "No hay resultados...",allow_single_deselect: true});
        $(".chosenInstituto").chosen().on("changeCurso", function(event) { 
                            document.getElementById('curso').value = "" ;
                            $(".chosenCurso").trigger('chosen:updated');                                                                                                       
                            });
        $(".chosenCurso").chosen().on("chosen:showing_dropdown", function(event) {  
                            $(".chosenCurso").trigger('chosen:updated');                                                                           
                            });
        </script>
        
        
        <div class="form-group">
        <label for="Edicion">Edicion</label>
          <select id="edicion" name="Edicion" data-placeholder="- Seleccione Edicion -"

            <option value=""></option>
          </select>
      </div>
      
      <script>
        //chosen institución
        $('.chosenCurso, .chosenEdicion').chosen({no_results_text: "No hay resultados...",allow_single_deselect: true});
        $(".chosenCurso").chosen().on("changeEdicion", function(event) { 
                            document.getElementById('edicion').value = "" ;
                            $(".chosenEdicion").trigger('chosen:updated');                                                                                                       
                            });
        $(".chosenEdicion").chosen().on("chosen:showing_dropdown", function(event) {  
                            $(".chosenEdicion").trigger('chosen:updated');                                                                           
                            });
        </script>

 <button type="submit" class="btn btn-primary">Ver información</button>

</form>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		


</body>
</html>