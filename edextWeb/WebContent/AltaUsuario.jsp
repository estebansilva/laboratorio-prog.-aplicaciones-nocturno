<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>     
<%@page import="java.util.List"%>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Alta Usuario</title>
</head>
<body>

  	
<form action="AltaUsuario" method="post">
	<%
	String nick="";
    String nombre="";
    String apellido="";
    String correo="";
    String password="";
    String cpassword="";
    String fecha="";
    String instituto="";
    
    nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
   // instituto=request.getParameter("instituto");
	%>
  
  	<%if(request.getAttribute("error") == "El campo Nickname esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Nickname esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Nombre esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Nombre esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Apellido esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Apellido esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Correo esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo correo esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Password esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Password esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Confirmar Password esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Confirmar Password esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "El campo Fecha de Nacimiento esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Fecha de Nacimiento esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "Las Password seleccionadas no coinciden"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> Las Password seleccionadas no coinciden.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	
 	<%if(request.getAttribute("error") == "No hay Instituto selecionado"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> No hay Instituto selecionado.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 		<%if(request.getAttribute("error") == "El nickname o el correo eleigo ya existen"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El nickname o el correo eleigo ya existen.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    nombre=request.getParameter("nombre");
    apellido=request.getParameter("apellido");
    correo=request.getParameter("correo");
    password=request.getParameter("password");
    cpassword=request.getParameter("cpassword");
    fecha=request.getParameter("fecha");
    instituto=request.getParameter("instituto");
    %>
 	<%} %>
 	<div class="form-group">
    <label for="exampleFormControlInput1">Nickname</label>
     <input type="text" name="nick" class="form-control" id="nicknameInput"
		placeholder="Ingrese un nickname para el usuario">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Nombre</label>
     <input type="text" name="nombre" class="form-control" id="nombreInput"
		placeholder="Ingresar nombre del usuario.">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Apellido</label>
     <input type="text" name="apellido" class="form-control" id="apellidoInput"
		placeholder="Ingresar apellido del usuario.">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Correo</label>
     <input type="text" name="correo" class="form-control" id="correoInput"
		placeholder="Ingresar correo.">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Fecha de Nacimiento</label>
     <input type="date" name="fecha" class="form-control" id="fechaInput"
		value="01/01/1990"min="01/01/1900" max="31/12/2019" >
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Password</label>
     <input type="password" name="password" class="form-control" id="passInput"
		placeholder="Ingresar contraseņa.">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Confirmar Password</label>
     <input type="password" name="cpassword" class="form-control" id="confirmarpassInput"
		placeholder="Ingresar contraseņa.">
  </div>
  <%
  ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
  ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
  String[] ins = port.setInstitutos();%>
  <div class="form-group">
  	<label for="exampleFormControlInput1">Si es docente elija instituto</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ %>
				<option value="Soy Estudiante">Soy Estudiante</option> 
			<%for(String auxins:ins){ %>
				<option value="<%=auxins%>"><%=auxins%></option>
				
			<%}%>
			<%}%>
			</select>
			
			
  </div>
  <script>

</script>
  
  <button type="submit" class="btn btn-primary">Confirmar</button>
 </form> 
 	


</body>
</html>
 
