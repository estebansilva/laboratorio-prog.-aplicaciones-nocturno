<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>    
<%@page import="java.util.List"%> 

<%@page import="java.util.Arrays"%> 
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Seleccionar estudiantes para una edición de curso</title>
</head>
<body>

<%  String docente = (String)request.getSession().getAttribute("usuario");
ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
String[] ins = port.setInstitutos();

String insti =(String)request.getParameter("Instituto");
String cosocon =(String)request.getParameter("Curso");
String edicon =(String)request.getParameter("ediciones");

%>
		
  <form action="SelectEstudEdicion" method="post" >
 
  <div class="form-group">
  	<label for="exampleFormControlInput1">Seleccione instituto o categoria:</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ 
				if(insti!= null){%>
					<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%for(String auxins:ins){ %>
					<%if(insti!= null){%>
						<%if(!insti.equals(auxins)){%>
						<option value="<%=auxins%>"><%=auxins%></option>
						<%}%>
					<%}else{%>
						<option value="<%=auxins%>"><%=auxins%></option>
					<%}%>
				<%}%>
			<%}else{%>
					<option value=""></option>
				<%} %>
			
			</select>
    </div>
    <%if(request.getAttribute("elegido") != null){
       	List<String> listaCursos;
    	request.setAttribute("Instituto",request.getParameter("Instituto"));
    	String elegido = (String)request.getAttribute("elegido");
    	if(port.existeCategoria(elegido)){
    		listaCursos = Arrays.asList(port.cursosporCategoria(elegido));
    	}else{
    		listaCursos = Arrays.asList(port.setCursos(elegido));
    	}%>
    	<div class="form-group">
  			<label for="exampleFormControlInput1">Seleccione curso:</label>
  				<select class="custom-select" id="Curso" name="Curso">
				<%if(ins != null){ 
				if(cosocon!= null){%>
					<option value="<%=cosocon%>"><%=cosocon%></option>
				<%}%>
				
				<%for(String auxcur:listaCursos){ %>
					<option value="<%=auxcur%>"><%=auxcur%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    </div>
	   
<% if(request.getAttribute("cursoelegido") != null){
	String cursoelegido = (String)request.getAttribute("cursoelegido");%>
	<% List<String> ediciones = Arrays.asList(port.setEdiciones(cursoelegido));
	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Ediciones:</label>
  				<select class="custom-select" id="ediciones" name="ediciones">
				<%if(ediciones != null){
				if(edicon!= null){%>
					<option value="<%=edicon%>"><%=edicon%></option>
				<%}%>
				<%for(String auxedi:ediciones){ %>
					<option value="<%=auxedi%>"><%=auxedi%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	
	    	<%if(request.getAttribute("edicionlegida") != null){
	    		String edicionlegida = (String)request.getAttribute("edicionlegida");
	    		publicadores.DtEdicion dte = port.darInfoEdicion(edicionlegida);
	    		List<String> est = Arrays.asList(port.getEstudianteEnEdicion(edicionlegida));
	    		List<String> doc = Arrays.asList(port.getDocenteEdicion(edicionlegida));
	    		%>
	
					 	<div>	
							<br>
				        	<label for="exampleFormControlInput1">Nombre: <%=dte.getNombre()%> </label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fecha Inicio: <%=dte.getFechaI()%></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fehca Fin: <%=dte.getFechaF()%></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Cupos: <%=dte.getCupo()%></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fecha Publicación: <%= dte.getFechaPub() %></label>
				        	<br>
				        </div>
			 
		<%//ahora muestro los estudiantes en un grid 
				List<String> estudiantes = Arrays.asList(port.getEstudianteEnEdicion(edicionlegida)); //devuelve lista de estudiantes inscriptos a un curso
				
				
			
				System.out.print(estudiantes.size());
				
				//CREAR NUEVA FUNCION QUE TAMBIEN DEVUELVA ESTADO 
				if(estudiantes != null){%>
					<div class="container">					
						<div class="row">
						    <div class="col-6">Nombre del Estudiante</div>
						    <div class="col-3">Estado Actual</div>
						    <div class="col-3">Aceptar</div>
						
							<%
							int cont = 0;
							for(String auxEst:estudiantes){
							cont++;
							%>
							<div class="w-100"></div> 
							<div class="col-6">
								<%= auxEst%>
							</div>
						    <div class="col-3">
						    	<%=port.getEstadoInscripcionEdicion(auxEst, edicionlegida)%>
						   	</div>
						    <div class="col-3">						
						    	<input type="checkbox" id="<%=auxEst%>" name="<%=auxEst%>" value="Aceptar">	    
								
						    </div>
						    <%
							}
							
						    %>
						    <button type="button" onclick="<%request.setAttribute("Alicar", "confirmar"); %>" >Aplicar</button>
						</div>
					</div>
	        		<%
 					}
 				} 
			}
	    }
 	%> 
 		
		
	  
	   <br>
	   <button type="submit" class="btn btn-primary" id="infoEdicionBtn" name="infoEdicionBtn" >Ver información</button> 


</form>
</body>
</html>