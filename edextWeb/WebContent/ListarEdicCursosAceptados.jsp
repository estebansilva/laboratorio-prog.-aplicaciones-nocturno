<%@page import="com.sun.tools.javac.code.Attribute.Array"%>
<%@ page import= "java.util.List" %>
<%@ page import= "java.util.ArrayList" %>
<%@ page import= "java.util.Arrays" %>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/header.jsp" %>
<script src="http://code.jquery.com/jquery-latest.js"> </script>
<meta charset="ISO-8859-1">
<title>Lista de aceptados a Edición de Curso</title>
</head>
<body>
	

<%  String docente = (String)request.getSession().getAttribute("usuario");

ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
String[] institutos = port.setInstitutos();
String[] cursos = null;	
String[] ediciones = null;	
	
if ( institutos != null) {
		cursos = port.setCursos(institutos[0]);
		if (cursos != null) {
			ediciones = port.setEdiciones(cursos[0]);	
		}
		
	}	
	
	String instituto="";
	instituto=request.getParameter("instituto");
	String curso = request.getParameter("curso");
%>
		
  <form action="ListarAceptadosEdicionCurso" method="post" >
 
  <div class="form-group">
    <label for="instituto">Instituto</label>
    <select name="instituto" class="form-control" id="instituto">     
       <%
    	if  (institutos != null) {	
       		for(String ins:institutos) {
  	    %>       	
   	        	<option>  <%= ins %>  </option>
       <%
        	}
    	}
       %>
    </select>
  </div>

<div class="form-group">
    <label for="curso">Curso</label>
    <select name="curso" class="form-control" id="curso" >           	
   	   <%
   		if  (institutos != null) {	
   	
       		for(String cur:cursos) {
       	%>     	
   	        	<option>  <%= cur %>  </option>
       <%
       		}
       	}
	   %>
    </select>
 </div>

	
	  <% 
	  curso = request.getParameter("curso");
 		if(curso != null ){
 			  String[] edicionesNomStr = port.getEdicionDcocente(docente);
 			  String[] edicionesCursos = port.setEdiciones(curso);
 			  List<String> edicionesNom = new ArrayList<>();	 
 			  edicionesNom = Arrays.asList(edicionesNomStr);
 			  
 			  if (edicionesCursos != null){
 			if (!(edicionesNom.isEmpty())){
 				for(String s:edicionesCursos) {
 					if (edicionesNom.indexOf(s) != -1) {
 					String[] infoEdicion = port.getInfoEdicion(s);
		%>	
					 	<div>	
							<br>
				        	<label for="exampleFormControlInput1">Nombre: <%=infoEdicion[0] %> </label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fecha Inicio: <%=infoEdicion[1]  %></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fehca Fin: <%=infoEdicion[2] %></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Cupos: <%=infoEdicion[3] %></label>
				        	<br>
				        	<label for="exampleFormControlInput1">Fecha Publicación: <%= infoEdicion[4] %></label>
				        	<br>
				        </div>
			 
		<% 
			 String[][] estudiantes = port.getEstudianteEnEdicionAceptado(s);
 				int estudiantesLenght = estudiantes.length;
 				System.out.println("numero de estudiantes: " + estudiantesLenght);
 				if (estudiantesLenght > 0){
 					for(int j=0; j<estudiantesLenght; j++) {
			        	if(estudiantes != null){ 
			        		String[] estudianteJ = estudiantes[j];
	    %>
				        <div>	
							<br>
				        	<label for="exampleFormControlInput1">Nombre: <%=estudianteJ[0] %> </label>
				        	<br>
				        	<label for="exampleFormControlInput1">Estado: <%=estudianteJ[1]%></label>
				        </div>
	   
	        
		<%					
			        	}
 					  }
 					}
 				}
 			} 
			 
 			}	
 			
 		}
 		}
	
 	%> 
 		
		
	  
	   <br>
	   <button type="submit" class="btn btn-primary" id="infoEdicionBtn" name="infoEdicionBtn" >Ver información</button> 

</form>

</body>

<script>
function mostrarOcultarTablas(id){
	mostrado=0;
	elem = document.getElementById(id);
	edicion  = document.getElementById(edicion);
	
	if(elem.style.display=='block')
		mostrado=1;
		elem.style.display='none';
	
	if(mostrado!=1)
		elem.style.display='block';
}
	
	
</script>

<script>

$(document).on("change", "#instituto"  , function() {
	 var institutoGet = $("#instituto").val();
	 var cursoGet = $("#curso").val();
	 var $selectCurso = $("#curso");  
	 var $selectEdicionBtn = $("#infoEdicionBtn"); 
	     
	 $.get("ListarAceptadosEdicionCurso", {institutoGet},
	      function(responseJson){
			var responseJsonCurso=responseJson[0]
			
			 if (responseJsonCurso !== null)
					$selectCurso.prop('disabled', false);
					$selectEdicionBtn.prop('disabled', false);
					$selectCurso.find("option").remove();
					$.each(responseJsonCurso , function(index, item) {               
						$selectCurso.append('<option value="' + item + '">'+item+'</option>');
				 	});	
			 if (responseJsonCurso == '')
				$selectCurso.prop('disabled', true);
	 });		$selectEdicionBtn.prop('disabled', true);
});


</script>


</html>