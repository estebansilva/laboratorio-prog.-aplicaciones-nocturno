<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>  
<%@page import="java.util.List"%>  
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Curso consultado</title>
</head>
<body>

		<%!
		String insti2;
		String curso2;
		%>
		<%
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
		
		insti2 = (String)request.getSession().getAttribute("Instituto2");
		curso2 = (String)request.getSession().getAttribute("Curso2");
		System.out.print(curso2);
		String[] infocurso = port.getInfoCurso(curso2);
		
        	%>
        	<br>
        	<label for="exampleFormControlInput1">Instituto: <%=insti2%></label>
        	<br>
        	<label for="exampleFormControlInput1">Nombre de Curso: <%=curso2%></label>
        	<br>
        	<label for="exampleFormControlInput1">Descripcion: <%=infocurso[0]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Duracion: <%=infocurso[1]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Cantidad de Horas: <%=infocurso[2]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Creditos: <%=infocurso[3]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Fecha: <%=infocurso[4]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Url: <%=infocurso[5]%></label>
        	<br>
        	<%
        	String[] previas =port.getPreviaCurso(curso2);
        	String[] catcur = port.obtenerCategoriadeCurso(curso2);
        	%>
        	<div class="form-group">
	    	<label for="exampleFormControlSelect2">Categorias:</label>
		   	<select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
		    	<%if(catcur != null){
		    		for(String auxcat:catcur){%>
		    			<option value="<%=auxcat%>"><%=auxcat%></option>
		    		<%}
	    		}%>
	    	</select>
			</div>
        	<div class="form-group">
  			<label for="exampleFormControlInput1">Previas:</label>
  				<select class="custom-select" id="previas" name="previas">
				<%if(previas != null){ %>
				<%for(String auxpre:previas){ %>
					<option value="<%=auxpre%>"><%=auxpre%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(previas != null){ %>
	    	<input type="button" value="Consulta Previa" onclick="window.location='index.jsp'" >
	    	<% } %>
	    	<% String[] ediciones = port.setEdiciones(curso2);
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Ediciones:</label>
  				<select class="custom-select" id="ediciones" name="ediciones">
				<%if(ediciones != null){ %>
				<%for(String auxedi:ediciones){ %>
					<option value="<%=auxedi%>"><%=auxedi%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(ediciones != null){ %>
	    	 <input type="button" value="Consultar Edicion" onclick="window.location='index.jsp'" >
	    	<% } %>
	    	<%String[] pf = port.getPFromacion(curso2);
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Programas de Formacion:</label>
  				<select class="custom-select" id="pf" name="pf">
				<%if(pf != null){ %>
				<%for(String auxpf:pf){ %>
					<option value="<%=auxpf%>"><%=auxpf%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(pf != null){ %>
	    	<input type="button" value="Consulta Progama de Fomacion">
	  <% } %>
	   
     
 
 
	<button type="submit" class="btn btn-primary">Confirmar</button>
</body>
</html>