<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>     
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>  
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Consulta Curso</title>
</head>
<body>
<form action="ConsultaCurso" method="post">


	<%
	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
	String[] ins = port.setInstitutos();
	String[] cat = port.listaDeCategorias();
	String insti =(String)request.getParameter("Instituto");
	String cosocon =(String)request.getParameter("Curso");
	String edicon =(String)request.getParameter("Edicion");
	%>
	<div class="form-group">
  	<label for="exampleFormControlInput1">Seleccione instituto o categoria:</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ 
				if(insti!= null){%>
					<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%for(String auxins:ins){ %>
					<%if(insti!= null){%>
						<%if(!insti.equals(auxins)){%>
						<option value="<%=auxins%>"><%=auxins%></option>
						<%}%>
					<%}else{%>
						<option value="<%=auxins%>"><%=auxins%></option>
					<%}%>
				<%}%>
			<%}else{%>
					<option value=""></option>
				<%} %>
			<%
			if(cat !=null){
				if(insti!= null){%>
				<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%%>
					<%if(insti!= null){
						for(String auxcat:cat){%>
						<%if(insti.equals(auxcat)){%>
						<%}else{%>
							<option value="<%=auxcat%>"><%=auxcat%></option>
						<%}%>
						<%}%>
					<%}else{%>
						<%for(String auxcat:cat){ %>
						<option value="<%=auxcat%>"><%=auxcat%></option>
						<%} %>
					<%}%>
				<%}%>
			</select>
    </div>
    <%if(request.getAttribute("elegido") != null){
    	List<String> listaCursos;
    	request.setAttribute("Instituto",request.getParameter("Instituto"));
    	String elegido = (String)request.getAttribute("elegido");
    	if(port.existeCategoria(elegido)){
    		listaCursos = Arrays.asList(port.cursosporCategoria(elegido));
    	}else{
    		listaCursos = Arrays.asList(port.setCursos(elegido));
    	}%>
    	<div class="form-group">
  			<label for="exampleFormControlInput1">Seleccione curso:</label>
  				<select class="custom-select" id="Curso" name="Curso">
				<%if(ins != null){ 
				if(cosocon!= null){%>
					<option value="<%=cosocon%>"><%=cosocon%></option>
				<%}%>
				
				<%for(String auxcur:listaCursos){ %>
					<option value="<%=auxcur%>"><%=auxcur%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    </div>
        <% if(request.getAttribute("cursoelegido") != null){
        	String cursoelegido = (String)request.getAttribute("cursoelegido");
        	String[] infocurso = port.getInfoCurso(cursoelegido);
        	%>
        	<br>
        	<label for="exampleFormControlInput1">Descripcion: <%=infocurso[0]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Duracion: <%=infocurso[1]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Cantidad de Horas: <%=infocurso[2]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Creditos: <%=infocurso[3]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Fecha: <%=infocurso[4]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Url: <%=infocurso[5]%></label>
        	<br>
        	<%
        	List<String> previas = Arrays.asList(port.getPreviaCurso(cursoelegido));
        	
        	List<String> catcur = Arrays.asList(port.obtenerCategoriadeCurso(cursoelegido));
        	%>
        	<div class="form-group">
	    	<label for="exampleFormControlSelect2">Categorias:</label>
		   	<select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
		    	<%if(catcur != null){
		    		for(String auxcat:catcur){%>
		    			<option value="<%=auxcat%>"><%=auxcat%></option>
		    		<%}
	    		}%>
	    	</select>
			</div>
        	<div class="form-group">
  			<label for="exampleFormControlInput1">Previas:</label>
  				<select class="custom-select" id="previas" name="previas">
				<%if(previas != null){ %>
				<%for(String auxpre:previas){ %>
					<option value="<%=auxpre%>"><%=auxpre%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(previas != null){ %>
	    	<input type="button" value="Consulta Previa" onclick="window.location='index.jsp'" >
	    	<% } %>
	    	<% List<String> ediciones = Arrays.asList(port.setEdiciones(cursoelegido));
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Ediciones:</label>
  				<select class="custom-select" id="ediciones" name="ediciones">
				<%if(ediciones != null){ %>
				<%for(String auxedi:ediciones){ %>
					<option value="<%=auxedi%>"><%=auxedi%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(ediciones != null){ %>
	    	
	    	<button type="submit" class="btn btn-primary">Consultar Edicion</button>
	    	
	    	<% } %>
	    	<%List<String> pf = Arrays.asList(port.getPFromacion(cursoelegido));
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Programas de Formacion:</label>
  				<select class="custom-select" id="pf" name="pf">
				<%if(pf != null){ %>
				<%for(String auxpf:pf){ %>
					<option value="<%=auxpf%>"><%=auxpf%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    	</div>
	    	<%if(pf != null){ %>
	    	<input type="button" value="Consulta Progama de Fomacion">
	  <% } %>
	   
       <% }%>
 
    <%} %>
	<%if(request.getAttribute("cursoelegido") == null)%>
	<button type="submit" class="btn btn-primary">Confirmar</button>
  
      
	

</form>
</body>
</html>