<%@ page import= "java.util.List" %>
<%@ page import= "java.util.ArrayList" %>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/header.jsp" %>
<script src="http://code.jquery.com/jquery-latest.js"> </script>
<meta charset="ISO-8859-1">
<title>Alta Edici�n Curso</title>
</head>
<body>

<tr>
<td>
 
 <form action="AltaEdicionCurso2" method="post">
 
 <% 	

ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();


String[] institutos = port.setInstitutos();
String[] cursos = null;
String[] docentes = null;
if (institutos != null) {
	cursos = port.setCursos(institutos[0]);
	docentes = port.getDocentes(institutos[0]);	
}

	
String fecha = "";
String fechaIni= "";
String fechaFin= "";
String nomEdiCurso="";
String cupo= "";
String instituto="";
String curso= "";
String[] docente = null;

fecha=request.getParameter("fecha");
fechaIni=request.getParameter("fechaIni");
fechaFin=request.getParameter("fechaFin");
nomEdiCurso=request.getParameter("nomEdiCurso");
cupo=request.getParameter("cupo");
instituto=request.getParameter("instituto");
curso=request.getParameter("curso");
docente=request.getParameterValues("docente[]");

%>
 
 <%if(request.getAttribute("error") ==  "No pueden haber campos vacios"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> No pueden haber campos vacios.
  	</div>
  	<%
  	fecha=request.getParameter("fecha");
  	fechaIni=request.getParameter("fechaIni");
  	fechaFin=request.getParameter("fechaFin");
  	nomEdiCurso=request.getParameter("nomEdiCurso");
  	cupo=request.getParameter("cupo");
  	//instituto=request.getParameter("instituto");
  	//curso=request.getParameter("curso");
  	//docente=request.getParameter("docente");
    %>
 	<%} %>
 
   <div class="form-group">
 		<label for="fecha">Fecha</label>
		<input type="date" name="fecha" class="form-control" id="fecha" >
	 </div>	
	 
  <div class="form-group">
 		<label for="fechaIni">Fecha Inicio</label>
		<input type="date" name="fechaIni" class="form-control" id="fechaIni" >
	 </div>	
 	 
 	<div class="form-group">
 		<label for="fechaFin">Fecha Fin</label>
		<input type="date" name="fechaFin" class="form-control" id="fechaFin">
	</div>	
  	
  	 <div class="form-group">
  		<label for="nomEdiCurso">Nombre de la Edicion del Curso</label>
   		<input type="text" name="nomEdiCurso" class="form-control" id="nomEdiCurso" >
  	 </div>
  
  	 <div class="form-group">
  		<label for="cupo">Cupo</label>
   		<input type="text" name="cupo" class="form-control" id="cupo" >
    </div>
  
  
  <div class="form-group">
    <label for="instituto">Instituto</label>
    <select name="instituto" class="form-control" id="instituto">     
       <%
    	if  (institutos != null) {	
       		for(String ins:institutos) {
  	    %>       	
   	        	<option>  <%= ins %>  </option>
       <%
        	}
    	}
       %>
    </select>
  </div>

<div class="form-group">
    <label for="curso">Curso</label>
    <select name="curso" class="form-control" id="curso">           	
   	   <%
       	if  (institutos != null) {	
       	for(String cur:cursos) {
  	    %>       	
   	        	<option>  <%= cur %>  </option>
       <%
        	}
       	}
	   %>
    </select>
 </div>


 <div class="form-group">
    <label for="lbldocente">Docente</label>
     <select id="docente[]" name="docente[]" class="form-control" multiple>
       <%
       	if  (institutos != null) {	
       	for(String doc:docentes) {
  	    %>       	
   	        	<option>  <%= doc %>  </option>
       <%
        	}
       	}
	   %>
    </select>
     <input type="hidden" name="DocenteValor" value="">
  </div> 
  

 <button id= "btnSubmit" type="submit" class="btn btn-primary">Confirmar</button>
</form>
</td>
</tr>

</body>
 

<script>
$(document).on("change", "#instituto"  , function() {
	 var institutoGet = $("#instituto").val();
	 var $selectCurso = $("#curso");  
	 var $selectDocente = $("#docente"); 
	     
	 $.get("AltaEdicionCurso2", {institutoGet},
	      function(responseJson){
			var responseJsonCurso=responseJson[0], responseJsonDocente=responseJson[1];
			
			 if (responseJsonCurso !== null)
					$selectCurso.prop('disabled', false);
					$selectCurso.find("option").remove();
					$.each(responseJsonCurso , function(index, item) {               
						$selectCurso.append('<option value="' + item + '">'+item+'</option>');
				        console.log(index + ":" + item)	
				 	});	
			 if (responseJsonCurso == '')
				$selectCurso.prop('disabled', true);
				
			 if (responseJsonDocente !== null)
					$selectDocente.prop('disabled', false);
					$selectDocente.find("option").remove();
					$.each(responseJsonDocente , function(index, item) {               
						$selectDocente.append('<option value="' + item + '">'+item+'</option>');
				        console.log(index + ":" + item)	
				 	});	
			 if (responseJsonDocente == '')
				$selectDocente.prop('disabled', true);
	 
	 });
});





</script> 



</html>