<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<%@ page import= "java.util.ArrayList" %>
<%@ page import= "java.util.List" %>
<%@ page language="java" session="true" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link type="text/css" rel="stylesheet" href='css\component-chosen.css'>
    <link type="text/css" rel="stylesheet" href='css\component-chosen.min.css'>
    <link type="text/css" rel="stylesheet" href='css\bootstrap.min.css'>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	
</head>
<link  rel="icon"   href="imagenes/favicon.png" type="image/png" />
<body>
<script src="js\jquery-3.4.1.min.js" ></script>
    <script src="js\chosen.jquery.js" type="text/javascript"></script>

  <img src="imagenes/logo.png" align="left" width="100"
     height=auto>
<nav class="navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="#"></a>
  


  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  
  
  
  
  
  
  <%
	ControladorAltaPublishService cAP2 = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port2 = cAP2.getcontroladorAltaPublishPort();
  	String[] cursosheader = port2.setTodosLosCursos();
  
	String usr = (String)request.getSession().getAttribute("usuario");
	if (usr == null || usr.isEmpty()){
		request.getSession().setAttribute("usuario", "SoyVisitante");
	}
	String v = "Visitante";
	%>
	
	
  <div class="form-group">
            <select name="Curs" id="Curs"  data-placeholder="Buscar Cursos..."
             class="form-control chosen"
            >
              <option value=""></option>
              <%if(cursosheader!= null){ %>
            <%for(String str:cursosheader){ %>
                <option value="<%=str%>"><%=str%></option>
            <% }%>
        <%}%>

            </select>
      </div>
<script>
        //chosen instituci�n
        $('.chosen').chosen({no_results_text: "No existe el curso solicitado",allow_single_deselect: true});
        $(document).ready(function(){
            $(".chosen").chosen();
       });

        </script>
       
       
	<%if (request.getSession().getAttribute("usuario").equals("SoyVisitante")){%>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="login.jsp">Iniciar Sesi�n</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="AltaUsuario.jsp"> Registrarme <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="consultaCurso.jsp"> Consulta de Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="consultaEdicion.jsp"> Consulta de edici�n de Curso <span class="sr-only"></span></a>
      </li>
      </ul>
     
  </div>
	<% } %>
	<% if(usr != null){
		if(port2.esDocnete(usr)){%>
	<%= usr%>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="AltaCurso.jsp"> Alta Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="consultaCurso.jsp"> Consulta de Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="AltaEdicionCurso.jsp"> Alta de edici�n de Curso <span class="sr-only"></span></a>
      <li class="nav-item active">
        <a class="nav-link" href="consultaEdicion.jsp"> Consulta de edici�n de Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="SelectEstudEdicionCurso.jsp"> Seleccionar estudiantes para una edici�n de curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="ListarEdicCursosAceptados.jsp"> Listar aceptados a una edici�n de curso <span class="sr-only"></span></a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesi�n <span class="sr-only"> </span></a>
      </li>
      </ul>
  </div>
		<% } 
		  }	%>
	
	<% if(usr != null){
		if(!port2.esDocnete(usr)){%>
	<%= usr%>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="consultaCurso.jsp"> Consulta de Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="consultaEdicion.jsp"> Consulta de edici�n de Curso <span class="sr-only"></span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="inscripcionAEdicion.jsp"> Inscripci�n a edici�n de Curso <span class="sr-only"></span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="cerrarSesion.jsp">Cerrar Sesi�n <span class="sr-only">  </span></a>
      </li>
      </ul>
  </div> 
  		<% } 
  		  } %>

  
  </nav>


<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>
</html>