<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<%@page import="java.util.List"%>
   
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<%@include file="/header.jsp" %>
<title>Alta Curso</title>
</head>
<body>


<form action="AltaCurso" method="post">
	<%
	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
	String[] ins = port.setInstitutos();
	String[] cat = port.listaDeCategorias();
	String[] prev = port.setTodosLosCursos();
	
	String instituto=request.getParameter("instituto");	
    String nombre=request.getParameter("nombre");
	String descripcion=request.getParameter("descripcion");
    String duracion=request.getParameter("duracion");
    String ScantHoras=request.getParameter("cantHoras");
    String Screditos=request.getParameter("creditos");
    String url=request.getParameter("url");
	%>
	
		<%if(request.getAttribute("error") == "El nombre ya existe"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> Ya existe un curso con ese nombre.
  	</div>
  	<%
  	instituto=request.getParameter("instituto");	
     nombre=request.getParameter("nombre");
	 descripcion=request.getParameter("descripcion");
     duracion=request.getParameter("duracion");
     ScantHoras=request.getParameter("cantHoras");
     Screditos=request.getParameter("creditos");
     url=request.getParameter("url");
    %>
 	<%} %>
	<h1>Complete los datos del nuevo curso:</h1>
	
	<div class="form-group">
		<label for="formGroupExampleInput">Nombre:</label>
		<input type="text" name="nombre" class="form-control" id="formGroupExampleInput" placeholder="Nombre del curso" required>
	</div>
	<div class="form-group">
		<label for="exampleFormControlTextarea1">Descripcion:</label>
		<textarea class="form-control" name="descripcion" id="exampleFormControlTextarea1" rows="3" required></textarea>
	</div>
	<div class="form-group">
		<label for="quantity">Creditos (Mayor que 0):</label>
		<input type="number" name="creditos" id="quantity" name="quantity" min="1" required>
	</div>
	<div class="form-group">
		<label for="quantity">Cantidad de horas (Mayor que 0):</label>
		<input type="number" name="cantHoras" id="quantity" name="quantity" min="1" required>
	</div>
	<div class="form-group">
		<label for="formGroupExampleInput">Duraci�n:</label>
		<input type="text" name="duracion" class="form-control" id="formGroupExampleInput" placeholder="Ej: 15 Semanas" required>
	</div>
	<div class="form-group">
		<label for="formGroupExampleInput">URL:</label>
		<input type="text" name="url" class="form-control" id="formGroupExampleInput" placeholder="www.cursoejemplo.com" required>
	</div>
	<div class="form-group">
    	<label for="exampleFormControlSelect1">Instituto:</label>
    	<select class="form-control" name="instituto" id="exampleFormControlSelect1" required>
    		<%if(ins != null){ %>
			<%//for(String auxins:ins.){
			for(String auxins:ins){%>
				<option value="<%=auxins%>"><%=auxins%></option>
			<%}%>
			<%}%>
    	</select>
	</div>
	
	<%if(request.getAttribute("elegido") != null){
    	String elegido = (String)request.getAttribute("elegido");
    	prev = port.setCursos(elegido);
    	}%>
	    
	<div class="form-group">
		<label for="exampleFormControlSelect2">Previas: (opcional)</label>
		<select multiple class="form-control" name="previasSelect" id="exampleFormControlSelect2">
			<%
			if(prev != null){
	    		for(String auxprev:prev){%>
	    			<option value="<%=auxprev%>"><%=auxprev%></option>
	    		<%}
	    	} %>
		</select>
	</div>
	<div class="form-group">
	    <label for="exampleFormControlSelect2">Categorias: (al menos 1)</label>
	    <select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2" required>
	    	<%if(cat != null){
	    		for(String auxcat:cat){%>
	    			<option value="<%=auxcat%>"><%=auxcat%></option>
	    		<%}
	    	}%>
	    </select>
	</div>
	
	<div class="form-group">
		<input type="submit" value="Crear Curso">
	</div>

</form>
	
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>	



</body>
</html>