<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.Arrays"%> 
<%@page import="java.util.List"%> 
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Inscripcion a edicion</title>
</head>
<body>
	<form action="InscripcionAEdicion" method="post">
	<%
	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
	
	String[] ins = port.setInstitutos();
	String[] cat = port.listaDeCategorias();
	String insti =(String)request.getParameter("Instituto");
	String cursoins =(String)request.getParameter("Curso");
	%>
	<%if(request.getAttribute("error") == "Usted ya esta inscripto"){%>
  				<div class="alert alert-info alert-dismissible">
   						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   						<strong></strong>Usted ya esta inscripto.
  				</div>
	        	<%} %>
	<div class="form-group">
  	<label for="exampleFormControlInput1">Seleccione instituto o categoria:</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ 
				if(insti!= null){%>
					<option value="<%=insti%>"><%=insti%></option>
				<%}else{%>
					<option value=""></option>
				<%} %>
				<%for(String auxins:ins){ %>
					<%if(insti!= null){%>
						<%if(!insti.equals(auxins)){%>
						<option value="<%=auxins%>"><%=auxins%></option>
						<%}%>
					<%}else{%>
						<option value="<%=auxins%>"><%=auxins%></option>
					<%}%>
				<%}%>
			<%}%>
			<%
			if(cat !=null){
				if(insti!= null){%>
				<option value="<%=insti%>"><%=insti%></option>
				<%}%>
				<%%>
					<%if(insti!= null){
						for(String auxcat:cat){%>
						<%if(insti.equals(auxcat)){%>
						<%}else{%>
							<option value="<%=auxcat%>"><%=auxcat%></option>
						<%}%>
						<%}%>
					<%}else{%>
						<%for(String auxcat:cat){ %>
						<option value="<%=auxcat%>"><%=auxcat%></option>
						<%} %>
					<%}%>
				<%}%>
			</select>
    </div>
    <%if(request.getAttribute("elegido") != null){
    	List<String> listaCursos;
    	request.setAttribute("Instituto",request.getParameter("Instituto"));
    	String elegido = (String)request.getAttribute("elegido");
    	if(port.existeCategoria(elegido)){
    		listaCursos = Arrays.asList(port.cursosporCategoria(elegido));
    	}else{
    		listaCursos = Arrays.asList(port.setCursos(elegido));
    	}%>
    	<div class="form-group">
  			<label for="exampleFormControlInput1">Seleccione curso:</label>
  				<select class="custom-select" id="Curso" name="Curso">
				<%if(listaCursos != null){ 
				if(cursoins!= null){%>
					<option value="<%=cursoins%>"><%=cursoins%></option>
				<%}%>
				<%for(String auxcur:listaCursos){ %>
					<option value="<%=auxcur%>"><%=auxcur%></option>
				<%}%>
				<%}else{%>
					<option value=""></option>
				<%} %>
				</select>
	    </div>
		    <% if(request.getAttribute("cursoelegido") != null){
	        	String cursoelegido = (String)request.getAttribute("cursoelegido");
	        	publicadores.DtEdicion dte = port.edicionVigente(cursoelegido);
	        	%>
	        	<input type="hidden" id="thisField" name="CursoFinal" value="<%=cursoelegido%>">
	        	<br>
	        	<%if(dte != null){
	        	%>
	        	<input type="hidden" id="thisField" name="edicionFinal" value="<%=dte.getNombre()%>">
	        	<label for="exampleFormControlInput1">Nombre de la Edicion: <%=dte.getNombre()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Cupo: <%=dte.getCupo()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Publicacion: <%=dte.getFechaPub()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Inicio: <%=dte.getFechaI()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Fin: <%=dte.getFechaF()%></label>
	        	
	        	<%}else{%>
	        			<label for="exampleFormControlInput1">NO HAY EDICIONES VIGENTES PARA ESTE CURSO</label>
	        	<%}%>
	       <% }%>
		<%}%>
		<br>
	<button type="submit" class="btn btn-primary">Confirmar</button>	
	</form>
</body>
</html>