<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>     
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%> 
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>  
<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Inscripcion a edicion</title>
</head>
<body>
	<form action="InscripcionAEdicion" method="post">
	<%
	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
	String[] ins = port.setInstitutos();
	String[] cat = port.listaDeCategorias();
	%>
	<div class="form-group">
  	<label for="exampleFormControlInput1">Seleccione instituto o categoria:</label>
  	<select class="custom-select" id="institutoInput" name="Instituto">
			<%if(ins != null){ %>
			<%for(String auxins:ins){ %>
				<option value="<%=auxins%>"><%=auxins%></option>
				
			<%}%>
			<%}%>
			<%
			if(cat !=null){
				for(String auxcat:cat){%>
					<option value="<%=auxcat%>"><%=auxcat%></option>
				<%}%>
			<%}%>
			</select>
    </div>
    <%if(request.getAttribute("elegido") != null){
      	List<String> listaCursos;
    	request.setAttribute("Instituto",request.getParameter("Instituto"));
    	String elegido = (String)request.getAttribute("elegido");
    	if(port.existeCategoria(elegido)){
    		listaCursos = Arrays.asList(port.cursosporCategoria(elegido));
    	}else{
    		listaCursos = Arrays.asList(port.setCursos(elegido));
    	}%>
    	<div class="form-group">
  			<label for="exampleFormControlInput1">Seleccione curso:</label>
  				<select class="custom-select" id="Curso" name="Curso">
				<%if(ins != null){ %>
				<%for(String auxcur:listaCursos){ %>
					<option value="<%=auxcur%>"><%=auxcur%></option>
				<%}%>
				<%}%>
				</select>
	    </div>
	    <% if(request.getAttribute("cursoelegido") != null){
        	String cursoelegido = (String)request.getAttribute("cursoelegido");
        	String[] infocurso = port.getInfoCurso(cursoelegido);
        	
        	
        	%>
        	<input type="hidden" id="thisField" name="CursoFinal" value="<%=cursoelegido%>">
        	<br>
        	<label for="exampleFormControlInput1">Nombre: <%=cursoelegido%></label>
        	<br>
        	<label for="exampleFormControlInput1">Descripcion: <%=infocurso[0]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Duracion: <%=infocurso[1]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Cantidad de Horas: <%=infocurso[2]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Creditos: <%=infocurso[3]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Fecha: <%=infocurso[4]%></label>
        	<br>
        	<label for="exampleFormControlInput1">Url: <%=infocurso[5]%></label>
        	<br>
        	<%
        	String[] previas = port.getPreviaCurso(cursoelegido);
        	%>
        	<div class="form-group">
  			<label for="exampleFormControlInput1">Previas:</label>
  				<select class="custom-select" id="previas" name="previas">
				<%if(previas != null){ %>
				<%for(String auxpre:previas){ %>
					<option value="<%=auxpre%>"><%=auxpre%></option>
				<%}%>
				<%}%>
				</select>
	    	</div>
	    	<% String[] ediciones = port.setEdiciones(cursoelegido);
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Ediciones:</label>
  				<select class="custom-select" id="ediciones" name="ediciones">
				<%if(ediciones != null){ %>
				<%for(String auxedi:ediciones){ %>
					<option value="<%=auxedi%>"><%=auxedi%></option>
				<%}%>
				<%}%>
				</select>
	    	</div>
	    	<%String[] pf = port.getPFromacion(cursoelegido);
	    	%>
	    	<div class="form-group">
	    	<label for="exampleFormControlInput1">Programas de Formacion:</label>
  				<select class="custom-select" id="pf" name="pf">
				<%if(pf != null){ %>
				<%for(String auxpf:pf){ %>
					<option value="<%=auxpf%>"><%=auxpf%></option>
				<%}%>
				<%}%>
				</select>
	    	</div>
       <% }%>
		<%}%>
		<br>
	<button type="submit" class="btn btn-primary">Confirmar</button>	
	</form>
</body>
</html>