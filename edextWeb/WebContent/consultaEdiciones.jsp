<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>     
<%@page import="java.util.List"%>  
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>

<html>
<head>
<meta charset="ISO-8859-1">
<%@include file="/header.jsp" %>
<title>Edicion consultado</title>
</head>
<body>
<%!
		String insti2;
		String curso2;
		String edi2;
		%>
		<%insti2 = (String)request.getSession().getAttribute("Instituto2");
		curso2 = (String)request.getSession().getAttribute("Curso2");
        edi2 = (String)request.getSession().getAttribute("ediciones2");
    	ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
    	ControladorAltaPublish port = cAP.getcontroladorAltaPublishPort();
		publicadores.DtEdicion dte = port.darInfoEdicion(edi2);
	    		String[] est = port.getEstudianteEnEdicion(edi2);
	    		String[] doc = port.getDocenteEdicion(edi2);
	    		%>
	    	<%if(dte != null){
	        	%>
	        	<label for="exampleFormControlInput1">Nombre de el Instituto: <%=insti2%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Nombre del curso: <%=curso2%></label>
	        	<br>

	        	<label for="exampleFormControlInput1">Nombre de la Edicion: <%=dte.getNombre()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Cupo: <%=dte.getCupo()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Publicacion: <%=dte.getFechaPub()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Inicio: <%=dte.getFechaI()%></label>
	        	<br>
	        	<label for="exampleFormControlInput1">Fecha Fin: <%=dte.getFechaF()%></label>
	        	
	        	<br>
	        	<label for="exampleFormControlInput1">Docentes:</label>
	        	<select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
	    	<%if(est != null){
	    		for(String auxdoc:doc){%>
	    			<option value="<%=auxdoc%>"><%=auxdoc%></option>
	    		<%}
	    	}else{ %>
    		<option value=""></option>
    	<%} %>
			
	    </select>
	    <br>
	        	<label for="exampleFormControlInput1">Estudiantess:</label>
	    <select multiple class="form-control" name="categoriasSelect" id="exampleFormControlSelect2">
	    	<%if(est != null){
	    		for(String auxest:est){%>
	    			<option value="<%=auxest%>"><%=auxest%></option>
	    		<%}
	    	}else{ %>
	    		<option value=""></option>
	    	<%} %>
	    </select>
	        	
	        	<%}else{%>
	        			<label for="exampleFormControlInput1">NO HAY EDICIONES VIGENTES PARA ESTE CURSO</label>
	        	<%}%>
	   
      
		<br>


</body>
</html>