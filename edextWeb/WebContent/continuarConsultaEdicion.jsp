
<%@ page import= "java.util.ArrayList" %>
<%@ page import= "java.util.List" %>
<%@page import="java.util.Arrays"%>
<%@page import="publicadores.ControladorAltaPublishService"%>
<%@page import="publicadores.ControladorAltaPublishServiceLocator"%>
<%@page import="publicadores.ControladorAltaPublish"%>




<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/header.jsp" %>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <link type="text/css" rel="stylesheet" href='css\component-chosen.css'>
    <link type="text/css" rel="stylesheet" href='css\component-chosen.min.css'>
    <link type="text/css" rel="stylesheet" href='css\bootstrap.min.css'>
    <title>Informaci�n de la Edici�n</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">

  </head>
  <body>
  	<%
  	String institutoElegido = request.getParameter("Ins");
	String cursoElegido = request.getParameter("curso");
	String edicionElegida = request.getParameter("edicion");
	%>

    <form action="Inscripci�n Edici�n de Curso" method="post">
    
    <script type = "text/javascript" src = 'js\curso.js' ></script> 
    <script type = "text/javascript" src = 'js\edicion.js' ></script>
    <script src="js\jquery-3.4.1.min.js" ></script>
    <script src="js\chosen.jquery.js" type="text/javascript"></script>
   
  <%
		String mostrar = (String) request.getAttribute("mostrar");
		if (mostrar.equals("edicion")) {
			ArrayList<publicadores.DtEdicion> edic = (ArrayList<DtEdicion>) request.getAttribute("edicion");
			ArrayList<publicadores.DtEstudiante> estudi = (ArrayList<DtEstudiante>) request.getAttribute("estudiante");
			ArrayList<publicadores.DtDocente> doc = (ArrayList<DtDocente>) request.getAttribute("docente");
	%>
	
	<h1 align="center"> Edicion </h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">NOMBRE</th>
				<th scope="col">FECHA INICIO</th>
				<th scope="col">FECHA FIN</th>
				<th scope="col">CUPOS</th>
				<th scope="col">FECHA PUBLICACION</th>
			</tr>
		</thead>
		<tbody>
			<%
				int i = 1;
				for (DtEdicion dte : edic ) {
			%>
					<tr>
					<th scope="row"><%=i%></th>
					<td><%=dte.getNombre()%></td>
					<td><%=dte.getFechaI()%></td>
					<td><%=dte.getFechaF()%></td>
					<td><%=dte.getCupo()%></td>
					<td><%=dte.getFechaPub()%></td>
						
					
					</tr>
			<%
				}
			%>
		</tbody>
	</table>
	<h1 align="center"> Inscriptos </h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">NOMBRE</th>
			</tr>
		</thead>
		<tbody>
			<%
				int j = 1;
				for (DtEstudiante dtes : estudi ) {
			%>
					<tr>
					<th scope="row"><%=j%></th>
					<td><%=dtes.getNombre()%></td>
						
					
					</tr>
			<%
				}
			%>
		</tbody>
	</table>
	</table>
	<h1 align="center"> Docentes </h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">NOMBRE</th>
			</tr>
		</thead>
		<tbody>
			<%
				int k = 1;
				for (DtDocente dtdoc : doc ) {
			%>
					<tr>
					<th scope="row"><%=j%></th>
					<td><%=dtdoc.getNombre()%></td>
						
					
					</tr>
			<%
				}
			%>
		</tbody>
	</table>
	<%
		} 
	%>

	

						
						

</form>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		


</body>
</html>