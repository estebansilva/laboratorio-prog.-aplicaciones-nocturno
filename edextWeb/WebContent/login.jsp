<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/header.jsp" %>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Inicio de Sesi�n</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
<link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">

  </head>
  <body>
    <form action="LoginUsuario" method="post">
    <%
	String nick = "";
	String pass = "";
	
	 nick=request.getParameter("nick");
	 pass=request.getParameter("pass");
	%>
	<%if(request.getAttribute("error") == "El campo Nick esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Usuario esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    pass=request.getParameter("pass");

    %>
    
 	<%} %>
    <%if(request.getAttribute("error") == "El campo Password esta vacio"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> El campo Password esta vacio.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    pass=request.getParameter("pass");

    %>
 	<%} %>
    
    
 	<%if(request.getAttribute("error") == "La Pass es Incorrecta"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> Password Incorrecta.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    pass=request.getParameter("pass");

    %>
    
 	<%} %>
 	<%if(request.getAttribute("error") == "El nickname es Incorrecto"){%>
  	<div class="alert alert-info alert-dismissible">
   		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   		<strong></strong> Usuario Incorrecto.
  	</div>
  	<%
  	nick=request.getParameter("nick");
    pass=request.getParameter("pass");

    %>
 	<%} %>
 	
    

  <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesi�n</h1>
   <div class="form-group">
                      <input type="text" name="nick" class="form-control form-control-user" id="exampleInputFirstName" placeholder="Usuario">
                    </div>
   <div class="form-group">
                      <input type="password" name="pass" class="form-control form-control-user" id="exampleInputFirstName2" placeholder="Password">
                    </div>
 
  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" value="remember-me"> Recordar Usuario
    </label>
  </div>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesi�n</button>
  <p class="mt-5 mb-3 text-muted">� 2020</p>
</form>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

</body>
</html>