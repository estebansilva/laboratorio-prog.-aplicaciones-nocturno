package publicadores;

public class ControladorAltaPublishProxy implements publicadores.ControladorAltaPublish {
  private String _endpoint = null;
  private publicadores.ControladorAltaPublish controladorAltaPublish = null;
  
  public ControladorAltaPublishProxy() {
    _initControladorAltaPublishProxy();
  }
  
  public ControladorAltaPublishProxy(String endpoint) {
    _endpoint = endpoint;
    _initControladorAltaPublishProxy();
  }
  
  private void _initControladorAltaPublishProxy() {
    try {
      controladorAltaPublish = (new publicadores.ControladorAltaPublishServiceLocator()).getcontroladorAltaPublishPort();
      if (controladorAltaPublish != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)controladorAltaPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)controladorAltaPublish)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (controladorAltaPublish != null)
      ((javax.xml.rpc.Stub)controladorAltaPublish)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public publicadores.ControladorAltaPublish getControladorAltaPublish() {
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish;
  }
  
  public java.lang.String[] setInstitutos() throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.setInstitutos();
  }
  
  public java.lang.String[] setCursos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.setCursos(arg0);
  }
  
  public java.lang.String[] getDocentes(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getDocentes(arg0);
  }
  
  public void ingresarEdicion(java.lang.String[] arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3, int arg4, java.util.Calendar arg5, java.lang.String arg6) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.ingresarEdicion(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
  }
  
  public void ingresarEstudiante(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.ingresarEstudiante(arg0, arg1, arg2, arg3, arg4, arg5);
  }
  
  public void inscripcionEdicionCurso(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.inscripcionEdicionCurso(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String[] listaDeCategorias() throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.listaDeCategorias();
  }
  
  public java.lang.String[] getEstudianteEnEdicion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getEstudianteEnEdicion(arg0);
  }
  
  public java.lang.String[] getDocenteEdicion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getDocenteEdicion(arg0);
  }
  
  public void ingresarCurso(java.lang.String[] arg0, java.lang.String[] arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, int arg5, int arg6, java.util.Calendar arg7, java.lang.String arg8, java.lang.String arg9) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.ingresarCurso(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
  }
  
  public void ingresarDocente(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.util.Calendar arg4, java.lang.String arg5, java.lang.String arg6) throws java.rmi.RemoteException, publicadores.Exception{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.ingresarDocente(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
  }
  
  public java.lang.String[] getEdicionEstudiante(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getEdicionEstudiante(arg0);
  }
  
  public java.lang.String[] obtenerCategoriadeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.obtenerCategoriadeCurso(arg0);
  }
  
  public java.lang.String[] getEdicionDcocente(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getEdicionDcocente(arg0);
  }
  
  public boolean esDocnete(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.esDocnete(arg0);
  }
  
  public publicadores.DtEdicion darInfoEdicion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.darInfoEdicion(arg0);
  }
  
  public java.lang.String[] setEdiciones(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.setEdiciones(arg0);
  }
  
  public java.lang.String[] getInfoCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getInfoCurso(arg0);
  }
  
  public java.lang.String[] getPFromacion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getPFromacion(arg0);
  }
  
  public java.lang.String[] getPreviaCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getPreviaCurso(arg0);
  }
  
  public java.lang.String[] setTodosLosCursos() throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.setTodosLosCursos();
  }
  
  public boolean existeCurso(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.existeCurso(arg0);
  }
  
  public boolean existenickemail(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.existenickemail(arg0, arg1);
  }
  
  public boolean existeCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.existeCategoria(arg0);
  }
  
  public publicadores.DtEdicion edicionVigente(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.edicionVigente(arg0);
  }
  
  public java.lang.String[] buscaNombre(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.buscaNombre(arg0);
  }
  
  public java.lang.String[] buscaFecha(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.buscaFecha(arg0);
  }
  
  public java.lang.String[] getInfoEdicion(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getInfoEdicion(arg0);
  }
  
  public boolean esposibleinscribirse(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.esposibleinscribirse(arg0, arg1);
  }
  
  public java.lang.String[] cursosporCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.cursosporCategoria(arg0);
  }
  
  public void changeEstadoInscripcionEdicion(java.lang.String arg0, java.lang.String arg1, boolean arg2) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    controladorAltaPublish.changeEstadoInscripcionEdicion(arg0, arg1, arg2);
  }
  
  public java.lang.String getEstadoInscripcionEdicion(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getEstadoInscripcionEdicion(arg0, arg1);
  }
  
  public java.lang.String[][] getEstudianteEnEdicionAceptado(java.lang.String arg0) throws java.rmi.RemoteException{
    if (controladorAltaPublish == null)
      _initControladorAltaPublishProxy();
    return controladorAltaPublish.getEstudianteEnEdicionAceptado(arg0);
  }
  
  
}