package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;


import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;

/**
 * Servlet implementation class InscripcionEdicion
 */
@WebServlet("/inscripcionAEdicion")
public class InscripcionEdicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscripcionEdicion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		RequestDispatcher rd;
			if(request.getParameter("ediciones")!=null) {

				String edi = request.getParameter("ediciones");
				String CursoFinal= request.getParameter("CursoFinal");
				String[] info = port.getInfoCurso(CursoFinal);
				String est = (String)request.getSession().getAttribute("usuario");
				System.out.print(edi);
				System.out.print(CursoFinal);
				System.out.print(info[6]);
				System.out.print(est);
				
				rd = request.getRequestDispatcher("/index.jsp");
				rd.forward(request, response);
			//	icon.inscripcionEdicionCurso(ins, cur, edi, est);
			}
			else if(request.getParameter("Curso")!=null){
				request.setAttribute("cursoelegido", request.getParameter("Curso"));
				rd = request.getRequestDispatcher("/inscripcionAEdicion.jsp");
				rd.forward(request, response);
			}else {
				request.setAttribute("elegido", request.getParameter("Instituto"));
				rd = request.getRequestDispatcher("/inscripcionAEdicion.jsp");
				rd.forward(request, response);
			}
	}

}
