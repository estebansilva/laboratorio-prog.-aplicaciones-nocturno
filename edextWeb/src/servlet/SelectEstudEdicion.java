package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;

/**
 * Servlet implementation class SelectEstudEdicion
 */
@WebServlet("/SelectEstudEdicion")
public class SelectEstudEdicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectEstudEdicion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String institutoGet = request.getParameter("institutoGet");
		String cursoGet = request.getParameter("cursoGet");
		
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		List<String> cursosList = new ArrayList<>();
		List<String> cursos = new ArrayList<>();
		
		if (institutoGet != null) {
			try {
				cursos = Arrays.asList(port.setCursos(institutoGet));
			}catch(Exception  e) {
				throw new ServletException(e.getMessage()); 
			}
		}
			
		Gson gson = new Gson();
		if  (!(institutoGet == null) ) {	
				for(String c:cursos) {           	
	       			cursosList.add(c.toString());
	        	}		
		}
		
		
		String responseJsonCurso = gson.toJson(cursosList);
 	    response.setContentType("application/json, charset=UTF-8");
		String ambosJson = "["+responseJsonCurso+"]"; 
		response.getWriter().write(ambosJson); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		RequestDispatcher rd;
		if(request.getParameter("Alicar") != null) {
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			request.setAttribute("Curso", request.getParameter("Curso"));
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			request.setAttribute("edicionlegida",request.getParameter("ediciones"));
			request.setAttribute("ediciones",request.getParameter("ediciones"));
			String edicionVigente = (String)request.getAttribute("String");
			List<String> estudiantesInscriptos = Arrays.asList(port.getEstudianteEnEdicion(edicionVigente));

			for (String auxEst : estudiantesInscriptos) {
				String check = request.getParameter(auxEst);
				boolean aceptado = (check!=null);
				port.changeEstadoInscripcionEdicion(auxEst, edicionVigente, aceptado);
			} 
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
			
		}else if(request.getParameter("ediciones") != null) {
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			request.setAttribute("Curso", request.getParameter("Curso"));
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			request.setAttribute("edicionlegida",request.getParameter("ediciones"));
			request.setAttribute("ediciones",request.getParameter("ediciones"));
			String edicionVigente = (String)request.getAttribute("edicionlegida");
			List<String> estudiantesInscriptos = Arrays.asList(port.getEstudianteEnEdicion(edicionVigente));

			for (String auxEst : estudiantesInscriptos) {
				String check = request.getParameter(auxEst);
				boolean aceptado = (check!=null);
				port.changeEstadoInscripcionEdicion(auxEst, edicionVigente, aceptado);
			} 
			rd = request.getRequestDispatcher("/SelectEstudEdicionCurso.jsp");
			rd.forward(request, response);

		}else if(request.getParameter("Curso")!=null) {
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			request.setAttribute("Curso", request.getParameter("Curso"));
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			rd = request.getRequestDispatcher("/SelectEstudEdicionCurso.jsp");
			rd.forward(request, response);
		}else if(request.getParameter("Instituto")!=null) {
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			rd = request.getRequestDispatcher("/SelectEstudEdicionCurso.jsp");
			rd.forward(request, response);
		}
	    
	    
	}

}
