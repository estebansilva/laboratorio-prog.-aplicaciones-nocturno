package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;

/**
 * Servlet implementation class InscripcionAEdicion
 */
@WebServlet("/InscripcionAEdicion")
public class InscripcionAEdicion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscripcionAEdicion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		if(request.getParameter("edicionFinal")!=null){
			ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
			ControladorAltaPublish port = null;
			try {
				port = cAP.getcontroladorAltaPublishPort();
			} catch (ServiceException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			request.setAttribute("Curso", request.getParameter("Curso"));
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("edicionFinal", request.getParameter("edicionFinal"));
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			String ins = (String)request.getParameter("Instituto");
			String edi = (String)request.getParameter("edicionFinal");
			String CursoFinal= (String)request.getParameter("CursoFinal");
			String est = (String)request.getSession().getAttribute("usuario");
			if(port.esposibleinscribirse(edi, est)) {
				try {
					port.inscripcionEdicionCurso(ins, CursoFinal, edi, est);
					rd = request.getRequestDispatcher("/index.jsp");
					rd.forward(request, response);
				}catch(Exception e){
					e.printStackTrace();
				}
			}else {
				request.setAttribute("error", "Usted ya esta inscripto");
				
				rd = request.getRequestDispatcher("/inscripcionAEdicion.jsp");
				rd.forward(request, response);
			}	
			
		}
		else if(request.getParameter("Curso")!=null){
			request.setAttribute("cursoelegido", request.getParameter("Curso"));
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			request.setAttribute("Curso", request.getParameter("Curso"));
			rd = request.getRequestDispatcher("/inscripcionAEdicion.jsp");
			rd.forward(request, response);
		}else {
			request.setAttribute("elegido", request.getParameter("Instituto"));
			request.setAttribute("Instituto", request.getParameter("Instituto"));
			rd = request.getRequestDispatcher("/inscripcionAEdicion.jsp");
			rd.forward(request, response);
		}
	}

}
