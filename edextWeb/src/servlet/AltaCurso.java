package servlet;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;


import publicadores.ControladorAltaPublish;
import publicadores.ControladorAltaPublishService;
import publicadores.ControladorAltaPublishServiceLocator;


/**
 * Servlet implementation class AltaCurso
 */
@WebServlet("/AltaCurso")
public class AltaCurso extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AltaCurso() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Fabrica fabrica = Fabrica.getInstancia();
		//IControladorAlta icon = fabrica.getIControladorAlta();
		ControladorAltaPublishService cAP = new ControladorAltaPublishServiceLocator();
		ControladorAltaPublish port = null;
		try {
			port = cAP.getcontroladorAltaPublishPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		RequestDispatcher rd;

	    String instituto=request.getParameter("instituto");	
	    String nombre=request.getParameter("nombre");
		String descripcion=request.getParameter("descripcion");
	    String duracion=request.getParameter("duracion");
	    String ScantHoras=request.getParameter("cantHoras");
	    String Screditos=request.getParameter("creditos");
	    String url=request.getParameter("url");    
	    
	    
	    List<String> categoria = new ArrayList<String>();
	    //obtengo array con values seleccionados y los paso a una lista
	    String[] valuesCat = (String[])request.getParameterValues("categoriasSelect");
	    String[] listaCatReturn = null;
	    if(valuesCat != null) {
	        	categoria = Arrays.asList(valuesCat);
	        	listaCatReturn = new String[categoria.size()];
	        	listaCatReturn = categoria.toArray(listaCatReturn);     
	      }
	   
	    List<String> previas = new ArrayList<String>();
        String[] valuesPrev = (String[])request.getParameterValues("previasSelect");
        String[] listaPreviasReturn = null;
        if(valuesPrev != null) {
        	previas = Arrays.asList(valuesPrev);
        	listaPreviasReturn = new String[previas.size()];
        	listaPreviasReturn = previas.toArray(listaPreviasReturn);
        }	         
	    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	    Date fechaR =  new Date();
	    String fecha = "01/01/2020";
	      
		//parseo fecha
		try {
			fechaR = format.parse(fecha);
			fechaR =  new Date();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar fechaRC = Calendar.getInstance();
		//cambiar cantHoras y creditos a int
		int cantHoras = 0;
		int creditos = 0;
		try {
			cantHoras = Integer.parseInt(ScantHoras);
			creditos = Integer.parseInt(Screditos);
			fechaRC.setTime(fechaR);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}	
		
		if(!port.existeCurso(nombre)) {
			try {
				port.ingresarCurso(listaPreviasReturn, listaCatReturn, nombre, descripcion, duracion, cantHoras,creditos, fechaRC ,url, instituto);
				request.setAttribute("mensaje", "Se ha ingresado correctamente el curso " + nombre + " en el sistema.");	    								
				rd = request.getRequestDispatcher("/index.jsp");
				rd.forward(request, response);
			} catch ( Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("catch en ingresarCurso" +  "  " + e.toString() + "res: ");
			}		    									    							
			
		}else {
			request.setAttribute("error", "El nombre ya existe");
			request.setAttribute("instituto",instituto);	
		    request.setAttribute("nombre",nombre);
			request.setAttribute("descripcion",descripcion);
		    request.setAttribute("duracion",duracion);
		    request.setAttribute("cantHoras",cantHoras);
		    request.setAttribute("creditos",creditos);
		    request.setAttribute("url",url);
		    rd = request.getRequestDispatcher("/AltaCurso.jsp");
			rd.forward(request, response);
		}
	
	}

}